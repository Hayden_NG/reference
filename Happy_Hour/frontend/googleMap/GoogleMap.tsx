import React, { useEffect, useRef, useState } from "react";
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { Text, View, StyleSheet, Dimensions} from "react-native"
import { fetchForm } from '../form/thunk'
import { RootState } from '../store'
import { useDispatch, useSelector } from "react-redux";
import { FormDetail } from "../form/reducer";
import Carousel, { CarouselStatic } from "react-native-snap-carousel";
import { Button,Icon } from "native-base";
import { Actions } from "react-native-router-flux";
import Geolocation, { GeolocationResponse } from '@react-native-community/geolocation';


function GoogleMap() {
    const dispatch = useDispatch();
    const formsinfo = useSelector((state: RootState) => state.form.forms)
    const mapRef = useRef<MapView>(null);
    const cardsss = useRef<Carousel<any>>(null);

    const onCarouselPress = (i: number) => {

        dispatch(fetchForm());
        let location = formsinfo;

        let map = mapRef.current;
        if (map == null) {
            return;
        }

        map.animateToRegion({
            latitude: parseFloat(location[i].lat),
            longitude: parseFloat(location[i].lng),
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
        })
    }

    const _Item = ({ item }) => {
        const id = formsinfo.findIndex(x => x.id === item.id)
        return (
            <Button style={styles.cardContainer} onPress={() => (onCarouselPress(id))}>
                <View>
                    <Text style={{fontSize:20,color:'white',marginBottom:10}}>主題:{item.title}</Text>
                </View>
                <View>
                    <Button rounded transparent style={{ backgroundColor: "white", justifyContent: "center", width:150 }} onPress={() => Actions.push('postdetail', { id: item.id })}><Text>點擊查看更多</Text></Button>
                </View>
            </Button>
        );
    }

    const [locations, setLocation] = useState<GeolocationResponse | null>(null);
    const [isCurrent, setIsCurrent] = useState(false);
    Geolocation.getCurrentPosition((position: GeolocationResponse) => setLocation(position), () => setLocation(null))    // by default permission null

    useEffect(() => {
       if (locations && isCurrent) {
        let map = mapRef.current;
        map?.animateToRegion({
            latitude: locations.coords.latitude,
            longitude: locations.coords.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
        })
        setIsCurrent(false)
       }
    }, [locations]);

    const currentLocation = () => {
        if (!locations) {
            Geolocation.getCurrentPosition((position: GeolocationResponse) => { 
                setLocation(position);
                setIsCurrent(true);
            }, () => setLocation(null))    // by default permission null
            
            return;
        }

        let map = mapRef.current;
        map?.animateToRegion({
            latitude: locations.coords.latitude,
            longitude: locations.coords.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
        })
    }

    const onCarouselItemChange = (i: number) => {
        let location = formsinfo;

        let map = mapRef.current;
        if (map == null) {
            return;
        }

        map.animateToRegion({
            latitude: parseFloat(location[i].lat),
            longitude: parseFloat(location[i].lng),
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
        })
    }

    const onMarkerPressed = (location: FormDetail[], index: number) => {

        dispatch(fetchForm());
        let map = mapRef.current;
        if (map == null) {
            return;
        }
        map.animateToRegion({
            latitude: parseFloat(location[index].lat),
            longitude: parseFloat(location[index].lng),
            latitudeDelta: 0.09,
            longitudeDelta: 0.035
        });
        cardsss.current.snapToItem(index);
    }
    useEffect(() => {
        dispatch(fetchForm());
    }, [formsinfo.length])
    return (
        <View >
            <MapView
                ref={mapRef}
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={{
                    width: "100%",
                    height: "98%",
                }}
                showsCompass={true}
                // showsUserLocation={true}
                followsUserLocation={true}
                moveOnMarkerPress={true}
                initialRegion={{
                    latitude: 22.3203648,
                    longitude: 114.169773,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                }}>
                {formsinfo == null ? null: formsinfo!.map((data, i) => (
                    formsinfo! && <Marker
                        coordinate={{ latitude:parseFloat(data.lat) , longitude:parseFloat(data.lng) }} title={`$${data.price}`}
                        onPress={() => {onMarkerPressed(formsinfo, i)}}
                    >
                    </Marker>
                ))}

            </MapView>
            <Carousel
                ref={cardsss}
                data={formsinfo}
                itemWidth={300}
                removeClippedSubviews={false}
                sliderWidth={Dimensions.get('window').width}
                renderItem={_Item}
                containerCustomStyle={styles.carousel}
                onSnapToItem={(index) => onCarouselItemChange(index)}
            />
            <Button transparent style={{ justifyContent: "center", alignItems: "center", position: "absolute", top: "2%", right: "2%" }} onPress={() => { currentLocation() }}>
                <Icon name="navigate" style={{ fontSize: 33, color: 'rgba(0, 0, 0, 0.6)' }} />
            </Button>
        </View>
    )
}

export default GoogleMap



const styles = StyleSheet.create({
    // container: {
    //     flex: 1,
    // },
    carousel: {
        position: 'absolute',
        bottom: "20%",
        // marginBottom: 48
    },
    cardContainer: {
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        height: 200,
        width: 300,
        padding: 24,
        borderRadius: 24,
        flexDirection:'column',
        justifyContent: "center",
    },
    cardImage: {
        height: 120,
        width: 300,
        position: 'absolute',
        bottom: 0,
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24,
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        alignSelf: 'center',
        margin:10,
        justifyContent:"center"
    },
    MyLocationButton: {
        height: 123
    }
});