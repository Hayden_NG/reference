import { AttendersActions } from './action'

export interface Attenders {
    post_id: number;
    attender_email: string;
}

export interface AttendersState {
    attenders: Attenders[],
}

const initalState: AttendersState = {
    attenders: [],
}

export const attenderReducer = (oldState: AttendersState = initalState, action: AttendersActions): AttendersState => {
    switch(action.type) {
        case "@@POSTS/ATTENDER":
            return {
                ...oldState,
                attenders: action.attenders,
            }
    }
    return oldState;
}