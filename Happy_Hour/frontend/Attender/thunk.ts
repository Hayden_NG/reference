import { mapAttender } from './action';
import { ThunkDispatch, RootState } from '../store';
import config from '../config';
import { ActionConst, Actions } from 'react-native-router-flux';
import { Alert } from 'react-native';

export function mapParticipant(post_id: number, attender_email: string | null) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const jwt_Token = getState().auth.token
        const res = await fetch(`https://hphour.xyz/attender/users`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${jwt_Token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ post_id, attender_email })
        })

        const result = await res.json()
        console.log(result)
        if (result.statusCode === 406) {
            Alert.alert('參加完又想參加？？ 影分身啊？')
        } else if (result.statusCode === 401) {
            Alert.alert('自己開POST自己JOIN？ 影分身啊？')
        } else if (result.success) {
            dispatch(mapAttender(result));
            Actions.push('secondPage')
        } else {
            Alert.alert('唔好意思，人數已滿！')
        }
    }
}