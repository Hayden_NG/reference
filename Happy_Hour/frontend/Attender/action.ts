import { Attenders } from './reducer'

export function mapAttender(attenders: Attenders[]) {
    return {
        type: "@@POSTS/ATTENDER" as "@@POSTS/ATTENDER",
        attenders
    }
}

export type AttendersActions = ReturnType<typeof mapAttender>;