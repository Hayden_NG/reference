import React, {Component} from "react";
import {View,Body,Icon,Button,Left,Right,Title,Header} from "native-base";
import FormSheet from "../form/Form"


export default class PostScreen extends Component {
   render() {
       return(
           <View>
               <Header>
                   <Left>
                       <Button 
                            transparent
                            //@ts-ignore
                            onPress={()=>this.props.navigation.openDrawer()}>
                           <Icon name="menu" />
                       </Button>
                   </Left>
                   <Body>
                       <Title>Post</Title>
                   </Body>
                   <Right/>
               </Header>
               <FormSheet/>
           </View>
       )
   }
}