import React ,{Component, useState, useEffect} from "react"
import {Image,ScrollView,SafeAreaView,Alert} from "react-native"
import {View,Button,Text} from "native-base"
import {createDrawerNavigator,DrawerItems} from "react-navigation-drawer"
import{createAppContainer} from "react-navigation"
import styles from "../styleFolder/NavigationBar.style"
import HomeScreen from "./HomeScreen"
import ProfileScreen from "./ProfileScreen"
import PostScreen from "./PostScreen"
import HistoryScreen from "./HistoryScreen"
import {Actions} from "react-native-router-flux"
import {RootState} from '../store'
import { useSelector } from "react-redux"
import {GoogleSignin} from '@react-native-community/google-signin';
import {LoginManager} from "react-native-fbsdk"
import AsyncStorage from "@react-native-community/async-storage"
import ReduxForm1 from '../form/ReduxForm1'
import OngoingEvent from "./OngoingEvent"
import {useDispatch} from 'react-redux'
import {clearJwtToken, setJwtToken} from "../login/authAction"

const profilePicture = (props:any)=> {
  useEffect(()=> {
    testing()
  },[])

  const [profile, setProfile] = useState({})
  async function testing() {

    const jwt_Token = await AsyncStorage.getItem('@jwt_Token')
    const res =await fetch ('https://hphour.xyz/getUserInfo',{
      headers:{
        'Authorization': `Bearer ${jwt_Token}`
      }
    })
    const loginPackage = await res.json()
  
    setProfile(loginPackage);
  }
  
  const dispatch = useDispatch()
  return(
  <SafeAreaView style={styles.safeareaview}>
    <View style={styles.view}>
      <Image source={{uri:`${profile.icon}`}} style={styles.image}/>
      <Text style={styles.text}>Hi {profile.username}</Text>
    </View>
    <ScrollView>
      <DrawerItems {...props}/>
      <View>
        <Button
          transparent 
          onPress={()=>Alert.alert('Are you sure to logout?','',[{text:'Cancel'},{text:'Ok', onPress:async () =>{
            try {
              // await GoogleSignin.revokeAccess();
              await AsyncStorage.removeItem('@jwt_Token')
              // await props.signOutGoogle();
              await LoginManager.logOut()
              dispatch(clearJwtToken())
              console.log('logout succeed')
              Actions.replace('firstPage')
            } catch (error) {
              console.error(error);
            }
          }}])}>

            <Text style={styles.logout}>登出</Text>
        </Button>
      </View>
    </ScrollView>
  </SafeAreaView>
  )
}

const AppDrawerNavigator = createDrawerNavigator({
  主頁:HomeScreen,                     // component

  個人資料:ProfileScreen,              // component

  出POST:ReduxForm1,                  

  歷史記錄:HistoryScreen,

  進行中的活動:OngoingEvent,

},{contentComponent:profilePicture})            // optional

export default class SecondPage extends Component {

  render() {
    const AppNavigator = createAppContainer(AppDrawerNavigator)
    return(
      <AppNavigator/>
    )
  }
}
