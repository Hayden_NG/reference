import React, { useState, useEffect } from "react"
import { View, Header, Left, Button, Icon, Body, Title, Right, Text, Content, Card, CardItem } from "native-base"
import { useSelector } from "react-redux"
import { RootState } from "../store"

function HistoryScreen(props: any) {

    const [message, setMessage] = useState('');

    const jwt_Token = useSelector((rootState: RootState) => rootState.auth.token)

    const getHistory = async () => {
        const fetchEvent = await fetch(`https://hphour.xyz/test/history`, {
            headers: {
                'Authorization': `Bearer ${jwt_Token}`
            },
        });
        const Events = await fetchEvent.json();
        
        return setMessage(Events);
    }
    
    console.log(message)
    useEffect(() => {
        getHistory();
    },[])
    return (
        <View>
            <Header>
                <Left>
                    <Button
                        transparent
                        //@ts-ignore
                        onPress={() => props.navigation.openDrawer()}>
                        <Icon name="menu" />
                    </Button>
                </Left>
                <Body>
                    <Title>歷史紀錄</Title>
                </Body>
                <Right />
            </Header>
            {/* <Text style={{flex:1 ,justifyContent:"center", position:"absolute", left:"35%", top:"400%", }}>{message.message}</Text> */}
            {message.address == undefined ? <Text>{message.message}</Text> : 
                <View>
                    <View style={{ marginBottom: 0 }}>
                        <Content padder>
                            <Card>
                                <CardItem header bordered>
                                    <Text>主題:{message.title}</Text>
                                </CardItem>
                                <CardItem bordered>
                                    <Body>
                                        <Text>發起人:{message.host}</Text>
                                        <Text>開始時間:{new Date(message.time).toLocaleTimeString()}</Text>
                                        <Text>完結時間:{new Date(message.endtime).toLocaleTimeString()}</Text>
                                        <Text>活動簡介:{message.info}</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </Content>
                    </View>
                </View>}
        </View>
    )
}

export default HistoryScreen;