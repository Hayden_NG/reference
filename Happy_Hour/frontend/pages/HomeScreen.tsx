import React ,{Component} from "react"
import {Header,Left, Right, Icon,Body,Title,View,Button} from "native-base"
import GoogleMap from "../googleMap/GoogleMap"

export default class HomeScreen extends Component {
    
  render() {
    return(
        <View>
            <Header>
                <Left>
                    <Button
                        transparent
                        // @ts-ignore
                        onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name="menu" />
                    </Button>
                    
                </Left>
                <Body>
                    <Title>按地圖查找</Title>
                </Body>
                <Right/>
            </Header>
            <GoogleMap/>
        </View>
    )
  }
}