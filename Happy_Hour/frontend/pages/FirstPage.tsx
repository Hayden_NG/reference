import React from "react"
import {View,ImageBackground,Text} from "react-native"
import styles from "../styleFolder/FirstPage.style"
import Googlelogin from "../login/GoogleLogin"
import FbLogin from "../login/FbLogin"

export default function FirstPageS() {

    return(
        <View style={styles.container}>
            <ImageBackground source={require('../image/background.jpg')} style={styles.background}>
                <View style={styles.appName}>
                    <Text style={styles.textTitle}>Happy Hour</Text>
                </View>

                <View style={styles.button} >
                    <FbLogin />
                    <Googlelogin />
                </View>
            </ImageBackground>
        </View>
    )
}