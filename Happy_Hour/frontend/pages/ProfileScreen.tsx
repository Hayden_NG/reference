import React, { useState, useEffect } from 'react'
import { StyleSheet, View,Image, AsyncStorage } from 'react-native'
import { Input, Left, Title, Body,Button,Icon } from 'native-base'
import LinearGradient from 'react-native-linear-gradient'
import {Text,Divider } from 'react-native-elements'
// import Icon from 'react-native-vector-icons/Ionicons'
import { Container, Header, Content, Card, CardItem,  Right } from 'native-base'
import { useDispatch } from 'react-redux'

const MemberProfile = (props) => {
  useEffect(() => {
    testing()
    getpost()
  }, [])

  const [profile, setProfile] = useState({})
  const [post, setpost] = useState({})
  async function testing() {

    const jwt_Token = await AsyncStorage.getItem('@jwt_Token')
    const res = await fetch('https://hphour.xyz/getUserInfo', {
      headers: {
        'Authorization': `Bearer ${jwt_Token}`
      }
    })
    const loginPackage = await res.json()

    setProfile(loginPackage);
  }
  async function getpost() {
    const jwt_Token = await AsyncStorage.getItem('@jwt_Token')
    const res = await fetch('https://hphour.xyz/ongoing', {
      headers: {
        'Authorization': `Bearer ${jwt_Token}`
      }
    })
    const postdetail = await res.json()

    setpost(postdetail);
  }

  const dispatch = useDispatch()


  return (
    <>
    <View>
      <Header>
        <Left>
          <Button
            transparent
            //@ts-ignore
            onPress={() => props.navigation.openDrawer()}>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>個人資料</Title>
        </Body>
        <Right />
      </Header>

      {/* <Text style={{ flex: 1, justifyContent: "center", position: "absolute", left: "35%", top: "400%", }}>{message.message}</Text> */}
    </View>

    <View>
      <View style={styles.button}>

      </View>
      <LinearGradient
        colors={["#0033ff", "#6bc1ff"]}
        style={{ height: "29%", }}
      >
      </LinearGradient>
      <View style={{ alignItems: "center" }}>
        <Image
          style={{ width: 100, height: 100, borderRadius: 140 / 2, marginTop: -50 }}
          source={{ uri: `${profile.icon}` }}
        />
      </View>
      <View style={{ alignItems: "center" }}>
        <Text style={styles.textTitle}>Hi {profile.username}</Text>
        {/* <Text style={{fontSize:20,color:'red',marginTop:20}}>Welcome</Text> */}

      </View>
    </View>
    </>

  )
}

const styles = StyleSheet.create({

  root: {
    flex: 1
  },
  linearGradient: {
    height: "20%",
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  buttonText: {

  },
  inputStyle: {
    alignItems: "flex-end"
  },
  button: {
    flexDirection: "row",
    alignSelf: "flex-end"
  },
  container: {
    height: "20%"
  },
  text: {
    margin: 20,
    alignItems: "flex-start"
  },
  textTitle: {
    fontSize:50,
    fontFamily:"Pacifico-Regular",
  }

})


export default MemberProfile