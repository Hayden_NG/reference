import React from 'react';
import { Router, Scene} from 'react-native-router-flux';
import FirstPage from "./pages/FirstPage";
import {Provider} from "react-redux";
import {store} from './store';
import SecondPage from "./pages/SecondPage"
import RemotePushController from "./Notification"
import PostDetail from './Detail/DetailPage';
import LoadingPage from "./pages/LoadingPage"
import FormSheet1 from './form/ReduxForm1';
import FormSheet2 from './form/ReduxForm2';

export default function App() {

  return(

    <Provider store={store}>
      <Router>
        <Scene key="root">
          <Scene key="loadingPage" component = {LoadingPage} initial={true} hideNavBar/>
          <Scene key="firstPage" component={FirstPage} hideNavBar/>
          <Scene key="secondPage" component={SecondPage} hideNavBar/>
          <Scene key="postdetail" component={PostDetail} title="活動詳細資料"/>
          <Scene key="ReduxForm1" component={FormSheet1} title="表格"/>
          <Scene key="ReduxForm2" component={FormSheet2} title="填寫活動資料" />
        </Scene>
      </Router>
      <RemotePushController/>
    </Provider>
  )
};

