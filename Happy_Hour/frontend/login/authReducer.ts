import {AuthActions} from './authAction'

export interface CheckAuth {
    token : string | null,
}

export interface AuthState {
    token: string | null
}

const initialState = {
    token : ''
}

export const authReducer = (oldState:AuthState = initialState,action:AuthActions):CheckAuth => {
    switch(action.type) {
        case '@@SET_JWT_TOKEN':
            return {
                ...oldState,
                token:action.token
            }

        case '@@CLEAR_JWT_TOKEN':
            return {
                ...oldState,
                token:null,
            }
    }
    return oldState
}