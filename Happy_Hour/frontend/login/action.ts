export function checkLoginFB(first_name:string, profile_url:string,email:string) {
    return {
        type: '@@FB_LOGIN_SUCCEED' as '@@FB_LOGIN_SUCCEED',
        first_name:first_name,
        profile_url:profile_url,
        email:email,
    }
}

export function checkLoginGG(givenName:string,photo:string,email:string) {
    return{
        type: '@@GG_LOGIN_SUCCEED' as '@@GG_LOGIN_SUCCEED',
        givenName:givenName,
        photo:photo,
        email:email,
    }
}


export type LoginActions = ReturnType<typeof checkLoginFB> | ReturnType<typeof checkLoginGG>
