import {checkLoginFB} from '../login/action'
import { ThunkDispatch, RootState } from '../store';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { setJwtToken } from './authAction';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';


export function FbInfo() {
    return async (dispatch:ThunkDispatch)=> {
        await LoginManager.logInWithPermissions(['public_profile', 'email']).then(

           async function (result) {
                if (result.isCancelled) {
                    console.log('Login cancelled')
                } else {
                    // console.log('Login success with permissions: ' + result.grantedPermissions?.toString())
                   const data = await AccessToken.getCurrentAccessToken()

                    const fbToken = data?.accessToken.toString()

                    const res = await fetch(`https://graph.facebook.com/me?fields=id,name,first_name,email&access_token=${fbToken}`)
                    const result = await res.json()
                    const email = result.email
                    const first_name = result.first_name

                    const fb_id = result.id
                    const profile_pic = await fetch(`https://graph.facebook.com/${fb_id}/picture`)
                    const profile_url = profile_pic.url
                    const detail = ({
                        first_name: first_name,
                        icon: profile_url,
                        email: email,
                    })

                    if (fbToken != null) {
                        
                        dispatch(checkLoginFB(first_name, profile_url, email))
                        const res = await fetch('https://hphour.xyz/loginSucceed', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(detail)
                        })

                        const jwt_Token = await res.json()
                        await AsyncStorage.setItem('@jwt_Token', jwt_Token.token)

                        dispatch(setJwtToken(jwt_Token.token))
                        Actions.replace('secondPage')
                    }

                }
            },
            function (error) {
                console.log('Login fail with error: ' + error)
            }
        )
    }
}