import {LoginActions} from "./action"

export interface CheckLogin {
    // token: string | null;
    first_name: string | null;
    profile_url: string | null;
    email:string | null; 
}

export interface LoginState {
    // token: string | null;
    first_name: string | null;
    profile_url: string | null;
    email:string | null;
}


const initialState = {
    // token:null,
    first_name: null,
    profile_url: null,
    email:null,
}

export const loginReducer = (oldState:LoginState=initialState,action:LoginActions):CheckLogin=>{
    switch(action.type) {
        case "@@FB_LOGIN_SUCCEED":
            return {
                ...oldState,
                first_name : action.first_name,
                profile_url : action.profile_url,
                email : action.email,
            }
            
        case "@@GG_LOGIN_SUCCEED":
            return {
                ...oldState,
                first_name : action.givenName,
                profile_url : action.photo,
                email : action.email,
            }
    }
    
    return oldState
}