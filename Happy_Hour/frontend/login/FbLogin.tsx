import React, {useEffect} from 'react';
import {LoginManager,AccessToken} from "react-native-fbsdk";
import styles from '../styleFolder/FbLogin.style';
import {Button,Icon} from "native-base";
import {checkLoginFB} from "./action";
import {Actions} from "react-native-router-flux"
import { useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'
import {FbInfo} from "./FbThunk"
import {setJwtToken} from "./authAction"


export interface Data {
    data:string | null
}

function FbLogin() {

    const dispatch = useDispatch()

    const login = function handleFacebookLogin () {

        dispatch(FbInfo())
    }
    
    
    return (
        <Button dark style={styles.container} onPress={login}>
            <Icon name="logo-facebook" />
        </Button>
    )
}

export default FbLogin