import React, { useState, useCallback } from 'react';
import { Container, Content, Form, Item, Textarea, Icon, Picker, Button, Text, Input} from 'native-base';
import { Actions } from 'react-native-router-flux'
import { Alert, View } from 'react-native';
import DateTimePickerTester from './DatetimePicker';
import EndTimePicker from './EndtimePicker';
import Upload from './Upload';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { fetchForm, createForm } from './thunk'
import styles from "../styleFolder/Datetime.style"

const FormSheet2: React.FC = (props) => {
    const [image, setImage] = useState();
    const [endtime, setEndTime] = useState();
    const [time, setTime] = useState('');
    const [location, setLocation] = useState();
    const [lat, setLat] = useState('');
    const [lng, setLng] = useState('');
    const [price, setPrice] = useState();
    const [info, setInfo] = useState('');
    const [numofPpl, setPeople] = useState();
    const FBinfo = useSelector((state: RootState) => state.login.first_name)
    const useremail = useSelector((state: RootState) => state.login.email)
    const submitting = useSelector((state: RootState) => state.form.sumitting)
    const [check, setCheck] = useState('')

    const getImage = useCallback((image) => {
        setTime(image);
    }, [setImage])

    const getTime = useCallback((time) => {
        setTime(time);
    }, [setTime])

    const getEndTime = useCallback((time) => {
        setEndTime(time);
    }, [setEndTime])

    const host = FBinfo?.toString() as string

    const allData = {
        title: props.item.title,
        host: host,
        district: props.item.district,
        category: props.item.category,
        address: props.item.address,
        ppl: numofPpl,
        time: time,
        endtime: endtime,
        lat: props.item.lat,
        lng: props.item.lng,
        price: parseInt(price),
        info: info,
        image: image,
        useremail: useremail,
    }

    console.log(new Date())
    
    const checkSubmit = function() {
        if (props.item.title == "") {
            Alert.alert("你未打主題喎!")
        } else if (props.item.district == "") {
            Alert.alert("請選擇地區!")
        } else if (props.item.category == "") {
            Alert.alert("請選擇類別!")
        } else if (props.item.address == "") {
            Alert.alert("請輸入地址!")
        } else if (allData.ppl == '') {
            Alert.alert("請選擇人數!")
        } else if (allData.endtime <= new Date()) {
            Alert.alert("請選擇正確時間!")
        } else if (allData.price == null) {
            Alert.alert("請輸入費用!")
        } else if (allData.info == '') {
            Alert.alert("請輸入簡介!")
        } else if (allData.image == 'No Photo') {
            Alert.alert("請上載一張圖片!")
        } else{
            return setCheck('Pass');
        }
    }
    
    const dispatch = useDispatch();

    return (
        <View style={{width:"100%",
        height:"100%"}}>
            <Container>
                <Content>
                    <Form >

                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            placeholder="人數"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            style={{ width: undefined }}
                            selectedValue={numofPpl}
                            onValueChange={(itemValue, pos) => setPeople(itemValue)}
                        >
                            <Picker.Item label="人數" value={null} />
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4" />
                            <Picker.Item label="5" value="5" />
                        </Picker>
                        <DateTimePickerTester getMyTime={setTime}/>
                        <EndTimePicker getEndTime={setEndTime}/>
                        {time!='' && time >= endtime && endtime != ''? Alert.alert("玩嘢啊？開始得早過結束？"): null}

                        {/* <Item> */}
                        <Input keyboardType='numeric' maxLength={5} placeholder="活動費用" value={price} onChangeText={(price) => setPrice(price)} />
                        {/* </Item> */}
                        <Textarea style={{marginTop:10, marginLeft:0}} rowSpan={5} bordered placeholder="不少於20字簡介" value={info} onChangeText={(info) => setInfo(info)} />
                        <Upload getMyImage={setImage}/>
                        <Button primary style={styles.container} onPress={()=>{checkSubmit();check == 'Pass'?dispatch(createForm(allData)) && Actions.push('secondPage'):null;dispatch(fetchForm())}}>
                            <Text>提交</Text>
                        </Button>
                    </Form>
                </Content>
            </Container >
            {/* <View style={{height:"30%"}}></View> */}
        </View>
    );
}

export default FormSheet2;
