import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import styles from "../styleFolder/Datetime.style";

interface IDateTimePickerVisibleState{
  isDateTimePickerVisible:boolean,
  selectedDate: string
}

export default class DateTimePickerTester extends Component<{},IDateTimePickerVisibleState> {
    constructor(props:any) {
        super(props);
        this.state = {
          isDateTimePickerVisible: false,
          selectedDate: "",
        }
    }
  
    

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });



  _handleDatePicked = (date :Date)=> {
    this.setState({ selectedDate: date.toString() });
    this._hideDateTimePicker();
    console.log(this.state.selectedDate)
  };

  render() {
    const { isDateTimePickerVisible, selectedDate } = this.state;
    
    return (
        
      <View style={styles.container}>
        <TouchableOpacity onPress={this._showDateTimePicker} style={styles.button}>
          <View>
            <Text style={{color:'white'}}>活動開始時間</Text>
          </View>
        </TouchableOpacity>

        {/* <Text style={styles.text}>{this.state.selectedDate}</Text> */}
        {selectedDate==''? null: <Text>{new Date(selectedDate).toLocaleTimeString()}</Text>}
        <DateTimePicker
          isVisible={isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          mode="time"
        />
        {this.props.getMyTime(selectedDate)}
      </View>
    );
  }
}