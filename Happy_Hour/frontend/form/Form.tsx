//@ts-nocheck
import React, { useState, useCallback} from 'react';
import { Container, Header, Content, Form, Item, Textarea, Icon, Picker, Button, Text, Input } from 'native-base';
import { Actions } from 'react-native-router-flux'
import { Alert, SafeAreaView, Platform, View, StyleSheet,ScrollView } from 'react-native';
import config from '../config';
import DateTimePickerTester from './DatetimePicker';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import styles from "../styleFolder/Datetime.style"
import EndTimePicker from "./EndtimePicker"
import {useSelector,useDispatch} from 'react-redux'
import {RootState} from '../store'
import { createForm, fetchForm } from './thunk';
import Upload from "./Upload"

// navigator.geolocation = require('@react-native-community/geolocation');
// navigator.geolocation = require('react-native-geolocation-service');

const FormSheet: React.FC = () => {

    const dispatch = useDispatch()

    const [image, setImage] = useState();
    const [endtime, setEndTime] = useState();

    const [time, setTime] = useState('');
    const [titles, setTitle] = useState('');
    const [selecting, setSelecting] = useState('');
    const [category, setCategory] = useState('');
    const [numofPpl, setPeople] = useState('');
    const [location, setLocation] = useState();
    const [Idate, setIdate] = useState(new Date());
    const [lat, setLat] = useState('');
    const [lng, setLng] = useState('');
    const [price, setPrice] = useState('');
    const [info, setInfo] = useState('');

    const FBinfo = useSelector((state: RootState) => state.login.first_name)
    const useremail = useSelector((state: RootState) => state.login.email)
    const submitting = useSelector((state: RootState) => state.form.sumitting)
    
    const getImage = useCallback((image) => {
        setTime(image);
    }, [setImage])

    const getTime = useCallback((time) => {
        setTime(time);
    }, [setTime])

    const getEndTime = useCallback((time) => {
        setEndTime(time);
    }, [setEndTime])

    const host = FBinfo?.toString() as string

    const data = {
        title: titles,
        host: host,
        district: selecting,
        category: category,
        address: location,
        ppl: parseInt(numofPpl),
        time: time,
        endtime: endtime,
        lat: lat,
        lng: lng,
        price: parseInt(price),
        info: info,
        image: image,
        useremail: useremail,
    }

    return (
        <View style={{width:"100%",
        height:"100%"}}>
            <Container>
                <Content>
                    <Form >
                        <Item picker>
                            <Input placeholder="主題" onChangeText={setTitle} />
                            {titles == '' ? null : titles.length < 4 ? <Icon name='close-circle' /> : titles.length >= 5 && <Icon name='checkmark-circle' />}
                        </Item>
                        <Item picker>
                            <Input placeholder="發起人" value={FBinfo?.toString()} disabled/>
                        </Item>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            // placeholder="請選擇地區"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            style={{ width: undefined }}
                            selectedValue={selecting}
                            onValueChange={(itemValue, pos) => setSelecting(itemValue)}
                        >
                            <Picker.Item label="請選擇地區" value={null} />
                            <Picker.Item label="中西區" value="中西區" />
                            <Picker.Item label="灣仔區" value="灣仔區" />
                            <Picker.Item label="東區" value="東區" />
                            <Picker.Item label="南區" value="南區" />
                            <Picker.Item label="深水埗區" value="深水埗區" />
                            <Picker.Item label="油尖旺區" value="油尖旺區" />
                            <Picker.Item label="九龍城區" value="九龍城區" />
                            <Picker.Item label="黃大仙區" value="黃大仙區" />
                            <Picker.Item label="觀塘區" value="觀塘區" />
                            <Picker.Item label="葵青區" value="葵青區" />
                            <Picker.Item label="荃灣區" value="荃灣區" />
                            <Picker.Item label="屯門區" value="屯門區" />
                            <Picker.Item label="元朗區" value="元朗區" />
                            <Picker.Item label="北區" value="北區" />
                            <Picker.Item label="大埔區" value="大埔區" />
                            <Picker.Item label="沙田區" value="沙田區" />
                            <Picker.Item label="西貢區" value="西貢區" />
                            <Picker.Item label="離島區" value="離島區" />
                        </Picker>

                        <GooglePlacesAutocomplete
                            fetchDetails={true}
                            placeholder='詳細地址'
                            onPress={(data, details = null) => {
                                // 'details' is provided when fetchDetails = true
                                const address = details?.formatted_address
                                setLocation(address)
                                const lat = details?.geometry.location.lat
                                setLat(lat)
                                const lng = details?.geometry.location.lng
                                setLng(lng)
                                console.log(details?.geometry.location.lat, details?.geometry.location.lng);
                            }}
                            query={{
                                key: 'AIzaSyD2wjcPYpma6hSxErOcAdMgookZPyUDRLo',
                                language: 'zh-TW',
                                components: 'country:hk',
                            }}
                            currentLocation={true}
                            currentLocationLabel='Current location'
                        />
            
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            // placeholder="請選擇類別"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            style={{ width: undefined }}
                            selectedValue={category}
                            onValueChange={(Value, pos) => setCategory(Value)}
                        >
                            <Picker.Item label="請選擇類別" value={null} />
                            <Picker.Item label="運動" value="運動" />
                            <Picker.Item label="商業" value="商業" />
                            <Picker.Item label="遊戲" value="遊戲" />
                            <Picker.Item label="其他" value="其他" />
                        </Picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            placeholder="人數"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            style={{ width: undefined }}
                            selectedValue={numofPpl}
                            onValueChange={(itemValue, pos) => setPeople(itemValue)}
                        >
                            <Picker.Item label="人數" value={null} />
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4" />
                            <Picker.Item label="5" value="5" />
                        </Picker>
                        <DateTimePickerTester getMyTime={setTime}/>
                        <EndTimePicker getEndTime={setEndTime}/>

                        <Item picker>
                            <Input keyboardType='numeric' maxLength={5} placeholder="活動費用" value={price} onChangeText={(price) => setPrice(price)} />
                        </Item>
                        <Textarea style={{marginTop:0}} rowSpan={5} bordered underline placeholder="不少於20字簡介" value={info} onChangeText={(info) => setInfo(info)} />
                        <Upload getMyImage={setImage}/>
                        <Button primary style={styles.container} onPress={()=>{console.log(data);dispatch(createForm(data)); Actions.push('secondPage'); dispatch(fetchForm());}}>
                            <Text>提交</Text>
                        </Button>
                    </Form>
                </Content>
            </Container >
            <View style={{height:'15%'}}></View>
        </View>
    );
}

export default FormSheet;
