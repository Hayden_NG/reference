import React, { useState, useCallback } from 'react';
import { Container, Content, Form, Item, Icon, Picker, Button, Text, Input, Header, Left, Title, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux'
import { Alert, View } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import { fetchForm, createForm } from './thunk'
import styles from "../styleFolder/Datetime.style"


const FormSheet1 = (props) => {
    const [image, setImage] = useState();
    const [endtime, setEndTime] = useState();
    const [time, setTime] = useState('');
    const [titles, setTitle] = useState('');
    const [selecting, setSelecting] = useState('');
    const [category, setCategory] = useState('');
    const [numofPpl, setPeople] = useState();
    const [location, setLocation] = useState('');
    const [lat, setLat] = useState('');
    const [lng, setLng] = useState('');
    const [price, setPrice] = useState();
    const [info, setInfo] = useState('');
    const FBinfo = useSelector((state: RootState) => state.login.first_name)
    const useremail = useSelector((state: RootState) => state.login.email)
    const submitting = useSelector((state: RootState) => state.form.sumitting)
    const getImage = useCallback((image) => {
        setTime(image);
    }, [setImage])

    const getTime = useCallback((time) => {
        setTime(time);
    }, [setTime])

    const getEndTime = useCallback((time) => {
        setEndTime(time);
    }, [setEndTime])

    const host = FBinfo?.toString() as string
    console.log(location)
    const data = {
        title: titles,
        host: host,
        district: selecting,
        category: category,
        address: location,
        lat: lat,
        lng: lng,
    }

    return (
        <View>
            <Header>
                <Left>
                    <Button
                        transparent
                        //@ts-ignore
                        onPress={() => props.navigation.openDrawer()}>
                        <Icon name="menu" />
                    </Button>
                </Left>
                <Body>
                    <Title>填寫活動資料</Title>
                </Body>
                <Right />
            </Header>

            {/* <Text style={{ flex: 1, justifyContent: "center", position: "absolute", left: "35%", top: "400%", }}>{message.message}</Text> */}

            <View style={{ width: "100%", height: '100%' }}>
                <Container>
                    <Content>
                        <Form >
                            <Item style={{marginLeft:0}}>
                                <Input placeholder="主題" onChangeText={setTitle} />
                                {titles == '' ? null : titles.length <= 4 ? <Icon style={{ color: "red" }} name='close-circle' /> : titles.length >= 5 && <Icon style={{ color: "green" }} name='checkmark-circle' />}
                            </Item>
                            <Item style={{marginLeft:0}}>
                                <Input style={{ color: "hsl(0, 0%, 67%)" }} placeholder="發起人" value={FBinfo?.toString()} disabled />
                            </Item>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                // placeholder="請選擇地區"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                style={{ width: undefined }}
                                selectedValue={selecting}
                                onValueChange={(itemValue, pos) => setSelecting(itemValue)}
                            >
                                <Picker.Item label="請選擇地區" value={null} />
                                <Picker.Item label="中西區" value="中西區" />
                                <Picker.Item label="灣仔區" value="灣仔區" />
                                <Picker.Item label="東區" value="東區" />
                                <Picker.Item label="南區" value="南區" />
                                <Picker.Item label="深水埗區" value="深水埗區" />
                                <Picker.Item label="油尖旺區" value="油尖旺區" />
                                <Picker.Item label="九龍城區" value="九龍城區" />
                                <Picker.Item label="黃大仙區" value="黃大仙區" />
                                <Picker.Item label="觀塘區" value="觀塘區" />
                                <Picker.Item label="葵青區" value="葵青區" />
                                <Picker.Item label="荃灣區" value="荃灣區" />
                                <Picker.Item label="屯門區" value="屯門區" />
                                <Picker.Item label="元朗區" value="元朗區" />
                                <Picker.Item label="北區" value="北區" />
                                <Picker.Item label="大埔區" value="大埔區" />
                                <Picker.Item label="沙田區" value="沙田區" />
                                <Picker.Item label="西貢區" value="西貢區" />
                                <Picker.Item label="離島區" value="離島區" />
                            </Picker>

                            <GooglePlacesAutocomplete
                                fetchDetails={true}
                                placeholder='詳細地址'
                                onPress={(data, details = null) => {
                                    // 'details' is provided when fetchDetails = true
                                    const address = details?.formatted_address
                                    console.log(address)
                                    setLocation(address)
                                    const lat = details?.geometry.location.lat
                                    setLat(lat)
                                    const lng = details?.geometry.location.lng
                                    setLng(lng)
                                    console.log(details?.geometry.location.lat, details?.geometry.location.lng);
                                    console.log(location)
                                }}
                                query={{
                                    key: 'AIzaSyD2wjcPYpma6hSxErOcAdMgookZPyUDRLo',
                                    language: 'zh-TW',
                                    components: 'country:hk',
                                }}
                                currentLocation={true}
                                currentLocationLabel='Current location'
                            />

                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                // placeholder="請選擇類別"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                style={{ width: undefined }}
                                selectedValue={category}
                                onValueChange={(Value, pos) => setCategory(Value)}
                            >
                                <Picker.Item label="請選擇類別" value={null} />
                                <Picker.Item label="運動" value="運動" />
                                <Picker.Item label="商業" value="商業" />
                                <Picker.Item label="遊戲" value="遊戲" />
                                <Picker.Item label="其他" value="其他" />
                            </Picker>

                            <Button primary style={{ marginTop: "40%", justifyContent: "center", borderRadius: 50 }} onPress={() => { Actions.push('ReduxForm2', { item: data }) }}>
                                {/* { location != null ? Alert.alert('你未揀地區喎！'): null} */}
                                <Text>下一頁</Text>
                            </Button>
                        </Form>
                    </Content>
                </Container >
                {/* <View style={{height:"30%"}}></View> */}
            </View>
        </View>
    );
}

export default FormSheet1;
