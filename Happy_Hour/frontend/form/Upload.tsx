import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Modal, SafeAreaView, ImagePropTypes, Image } from 'react-native'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import ImagePicker from 'react-native-image-picker';
import { check, PERMISSIONS, request, RESULTS } from 'react-native-permissions'
import { color } from 'react-native-reanimated';
const upload: React.FC = (props) => {


	const [Modalcon, setModalcon] = useState(false)
	const [cameraGranted, setCameraGranted] = useState(false)
	const [photoGranted, setPhotoGranted] = useState(false)
	const [averaSource, setAveraSource] = useState({})
	const [pic, setPic] = useState('')

	const options = {
		title: 'my pic app',
		takePhotoButtion: 'Take Photo with your camera',
		chooseFromLibaryButtpntite: 'choose photo from libarary'
	}

	const photoOptions = {
		title: 'your photo',
		quality: 0.8,
		cancelButtonTitle: 'cancel',
		takePhotoButtonTitle: 'take photo',
		chooseFromLibraryButtonTitle: 'choose photo from libarary',
		allowsEditing: true,
		noData: false,
		storageOptions: {
			skipBackup: true,
			path: 'images'
		}
	};
	const myfunction = () => {
		ImagePicker.showImagePicker(photoOptions, (response) => {
			// console.log("Response =", response);
			if (response.didCancel) {
				console.log('User cancelled image picker')
			}
			else if (response.error) {
				console.log('ImagePicker Error:', response.error)
			}
			else {
				let source = { uri: response.uri, type: response.type, name: response.fileName }

				setAveraSource(source)
				setPic(response.data)
				console.log(source);
				{props.getMyImage(source)}
				setModalcon(false)

			}
		})
	}

	const handleCameraPermission = async () => {
		const res = await check(PERMISSIONS.ANDROID.CAMERA);
		// console.log("AppPermission checkPermission:", res)
		if (res === RESULTS.GRANTED) {
			setCameraGranted(true)

		} else if (res === RESULTS.DENIED) {
			const res2 = await request(PERMISSIONS.ANDROID.CAMERA)

			res2 === RESULTS.DENIED ? setCameraGranted(true) : setCameraGranted(false)

		}
	};
	const handleGerallyPermission = async () => {
		const res = await check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
		console.log("AppPermission checkPermission:", res)
		if (res === RESULTS.GRANTED) {
			setPhotoGranted(true)



		} else if (res === RESULTS.DENIED) {
			const res2 = await request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE)

			res2 === RESULTS.DENIED ? setPhotoGranted(true) : setPhotoGranted(false)

		}
	};



	useEffect(() => {
		handleCameraPermission()
		// 
	}, [])

	useEffect(() => {
		handleGerallyPermission()
	}, [])


	return (

		<SafeAreaView style={{ backgroundColor: 'white' }}>


			<View >

				{/* <Item rounded floatingLabel>
					<Label>Name</Label>
					<Input
						value={name}
						onChangeText={text => setName(text)}
					/>
				</Item>

				<Item rounded floatingLabel>
					<Label>Email</Label>
					<Input
						value={email}
						onChangeText={text => setEmail(text)}
					/>
				</Item>

				<Item rounded floatingLabel>
					<Label>Location</Label>
					<Input
						value={location}
						onChangeText={text => setLocation(text)}
					/>
				</Item>

				<Item rounded floatingLabel>
					<Label>gender</Label>
					<Input
						value={gender}
						onChangeText={text => setGender(text)}
					/>
				</Item> */}
				<View><Image style={styles.avatar} source={averaSource} /></View>
				<Button
					title="upload image"
					icon={
						<Icon
							name="upload"
							style={{backgroundColor:"white", right:"100%"}}

						/>

					}
					onPress={() => {
						myfunction()
					}}
				/>
				{/* <Button
					title="Save"
					style={styles.inputStyle}
					icon={
						<Icon
							name="save"

						/>

					}
					onPress={() => {
						console.log("save")
					}}
				/> */}

				<Modal
					animationType="fade"
					transparent={true}
					visible={Modalcon}
					onRequestClose={() =>
						setModalcon(false)}
				>

					<View style={styles.modalView}>
						<View>
							<View style={styles.modalButtonView}>

								{/* <Button title="camera" onPress={() => {
									console.log("clicked ")
								}} icon={
									<Icon
										name="camera"
										size={10}
										color="white"

									/>
								}>
								</Button> */}
								<Button title="gallery" onPress={() => {
									myfunction()
								}} icon={
									<Icon
										name="image"
										size={10}
										color="white"

									/>
								}>
								</Button>
							</View>

							<Button title="cancel" style={styles.body} onPress={() => {
								setModalcon(false)
							}}
							// <Icon
							// 	name="camera"
							// 	size={10}
							// 	color="white"

							// />
							>
							</Button>
						</View>
					</View>
				</Modal>



			</View>
		</SafeAreaView>

	)



}

// const theme = {
// 	colors: {
// 		primary: "#006aff"
// 	}
// }

const styles = StyleSheet.create({

	root: {
		flex: 1

	},
	modalButtonView: {
		flexDirection: "row",
		justifyContent: "center",
		padding: 10,

	},
	modalView: {
		position: "absolute",
		bottom: "50%",
		alignItems:"center",
		backgroundColor: "gray",
		width: '100%',
	},
	body: {
		backgroundColor: Colors.gray,
	},
	inputStyle: {
		backgroundColor:"white",
		margin: "5%"
	},
	avatar: {
		borderRadius: 50,
		width: 100,
		height: 100,
		alignItems: "center",
		justifyContent: "center",
		left: "38%",
	}

})


export default upload