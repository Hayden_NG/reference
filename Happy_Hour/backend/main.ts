import express from "express"
//-----socketio-----//
import bodyParser from "body-parser"
import SocketIO from "socket.io"
import http from 'http'
//------socketio-----//
import dotenv from "dotenv"
//------DB --------//
import Knex from 'knex'
import path from 'path'

import * as admin from 'firebase-admin';

import { UserService, User } from "./services/UserService"
import { UserRouter } from "./routers/UserRouter"
import { FormService } from "./services/FormService"
import { FormRouter } from "./routers/FormRouter"
import { AttenderService } from "./services/AttenderService"
import { AttenderRouter } from './routers/AttenderRouter'
import {isLoggedIn} from "./guard"
import {OngoingRouter} from "./routers/OngoingRouter"
import {OngoingService} from "./services/OngoingService"
import { ChatService } from "./services/ChatService"
import { ChatRouter } from "./routers/ChatRouter"
import engines from 'consolidate'
import paypal from 'paypal-rest-sdk'

declare global {
    namespace Express {
        interface Request {
            user?:User
        }
    }
}

dotenv.config()

var serviceAccount = require("./google-service-account.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://project3-278110.firebaseio.com"
});
  
const knexConfig = require("./knexfile")
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])
const app = express()

app.engine("ejs", engines.ejs);
app.set("views", './views');
app.set("view engine", "ejs");

const server = http.createServer(app)
const io = SocketIO(server)

const chatService=new ChatService(knex)
const chatRouter =new ChatRouter(chatService,io)
const formService = new FormService(knex);
const formRouter = new FormRouter(formService);
const userService = new UserService(knex)
const userRouter = new UserRouter(userService)
const attenderService = new AttenderService(knex);
const attenderRouter = new AttenderRouter(attenderService);
const ongoingService = new OngoingService(knex);
const ongoingRouter = new OngoingRouter(ongoingService)

const isLoggedInGuard = isLoggedIn(userService)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use('/', userRouter.router());
app.use('/Chat',chatRouter.router())
app.use('/post/form', express.static(path.join(__dirname + '/routers' + '/uploads')) ) 
app.use('/post',formRouter.router());
app.use('/attender', isLoggedInGuard,attenderRouter.router());
app.use('/test', isLoggedInGuard,ongoingRouter.router());

paypal.configure({
  mode: 'sandbox', // Sandbox or live
  client_id: `${process.env.CLIENT_ID}`,
  client_secret: `${process.env.CLIENT_SECRET}`});


  app.get("/",(req,res)=>{
    res.render("index")
})


app.get('/paypal',(req,res)=>{
    let create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "https://hphour.xyz/success",
            "cancel_url": "https://hphour.xyz/cancel"
        },
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": "item",
                    "sku": "item",
                    "price": "1.00",
                    "currency": "USD",
                    "quantity": 1
                }]
            },
            "amount": {
                "currency": "USD",
                "total": "1.00"
            },
            "description": "This is the payment description."
        }]
    };


    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            throw error;
        } else {
            console.log("Create Payment Response");
            console.log(payment);
           //@ts-ignore
            res.redirect(payment.links[1].href)
        }
    });
})


    app.get("/success", (req, res) => {
        // res.send("Success");
        var PayerID = req.query.PayerID;
        var paymentId = req.query.paymentId;
        var execute_payment_json = {
            payer_id: PayerID,
            transactions: [
                {
                    amount: {
                        currency: "USD",
                        total: "1.00"
                    }
                } 
            ]
        };
    //@ts-ignore
        paypal.payment.execute(paymentId, execute_payment_json, function(
            error: { response: any; },
            payment: any
        ) {
            if (error) {
                console.log(error.response);
                throw error;
            } else {
                console.log("Get Payment Response");
                console.log(JSON.stringify(payment));

                admin.messaging().sendAll([
                    {
                        notification: {
                            title: '你好~現在有活動推廣',
                            body: '埋嚟睇,埋嚟JOIN啦~',
                        },
                        "condition": "!('anytopicyoudontwanttouse' in topics)"
                    }
                ])

                res.render("success");
            }
        });
  });

  app.get('/cancel',(req,res)=>{
    res.render('cancel')
})

io.on('connection', socket => {
    socket.on('join_posting', (postingId: number) => {
      socket.join('posting:' + postingId )
    })

    socket.on('leave_posting', (postingId: number ) => {
      socket.leave('posting:' + postingId )
    })
  });

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Listening on PORT ${PORT}`)
})

