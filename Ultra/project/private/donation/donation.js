async function user() {
    const fetchRes = await fetch('/user')
    const visitor = await fetchRes.json()
    console.log(visitor)
    // console.log(visitor.icon)

    document.querySelector(".visitor").innerHTML = `<form action="/user/logout" method="POST"><input type="submit" value="Logout ${visitor.nickname}"></form>`
    document.querySelector(".userIcon").innerHTML = `<a href="/user/userProfile"><img class="image" src=${visitor.icon}></a>`

}

document.querySelector("#payButton").addEventListener('click', (event)=> {
    event.currentTarget.remove();
    document.querySelector(".section4 ").innerHTML +=  (/*html*/`<div class="paypalButton">
    <div id="paypal-button-container1"></div>
  </div>`)

  paypal.Buttons({
    createOrder: function(data, actions) {
        const selectedValue = document.querySelector("#donation").value
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        //   intent:"CAPTURE",
          application_context:{
            brand_name: "UltraFx"
          },
        purchase_units: [{
        description: "Donation for Ultra Fx",
          amount: {
            value: selectedValue 
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(async function(details) {
        // This function shows a transaction success message to your buyer.
        console.log(details.purchase_units[0].amount.value)
        alert('Transaction completed by ' + details.payer.name.given_name);
        const fetches = await fetch('/user/userDonation', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                value: details.purchase_units[0].amount.value,
                orderID: data.orderID
            })
        });

        const result = await fetches.json();
        window.location.reload();
      });
    }
  }).render('#paypal-button-container1');
  //This function displays Smart Payment Buttons on your web page.
})

// paypal.Buttons({
//     createOrder: function(data, actions) {
//         const selectedValue = document.querySelector("#donation").value
//       // This function sets up the details of the transaction, including the amount and line item details.
//       return actions.order.create({
//         //   intent:"CAPTURE",
//           application_context:{
//             brand_name: "UltraFx"
//           },
//         purchase_units: [{
//         description: "Donation for Ultra Fx",
//           amount: {
//             value: selectedValue 
//           }
//         }]
//       });
//     },
//     onApprove: function(data, actions) {
//       // This function captures the funds from the transaction.
//       return actions.order.capture().then(async function(details) {
//         // This function shows a transaction success message to your buyer.
//         console.log(details.purchase_units[0].amount.value)
//         alert('Transaction completed by ' + details.payer.name.given_name);
//         const fetches = await fetch('/user/userDonation', {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify({
//                 value: details.purchase_units[0].amount.value,
//                 orderID: data.orderID
//             })
//         });

//         const result = await fetches.json();

//       });
//     }
//   }).render('#paypal-button-container1');
//   //This function displays Smart Payment Buttons on your web page.

window.onload = ()=> {
    user();
}