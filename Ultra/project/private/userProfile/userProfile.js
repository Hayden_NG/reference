async function user() {
    const fetchRes = await fetch('/user')
    const visitor = await fetchRes.json()
    // console.log(visitor)
    // console.log(visitor.icon)

    document.querySelector(".visitor").innerHTML = `<form action="/user/logout" method="POST"><input type="submit" value="Logout ${visitor.nickname}"></form>`
    document.querySelector(".userIcon").innerHTML = `<a href="/user/userProfile"><img class="image" src=${visitor.icon}></a>`

}

async function userDisplaySave() {
    document.querySelector("#userSaved").addEventListener('click', async (event) => {
        event.preventDefault();

        const fetchUserSaved = await fetch('/user/userSaved');
        const result = await fetchUserSaved.json();

        console.log(result.response);

        let container = $('#pagination');
        container.pagination({
            dataSource: result.response,
            pageSize: 4,
            callback: function (data, pagination) {
                document.querySelector("#userDisplay").innerHTML = "";
                let InfoCardsHTML = "";
                if(data.length === 0){
                    document.querySelector("#userDisplay").innerHTML = "<p>沒有紀錄</p>"
                    return;
                }
                for (let i = 0; i < data.length; i++) {
                    InfoCardsHTML += `<div class="infoCards col-md-9">`
                    InfoCardsHTML += `<a href="/exchange/exchangePage/${data[i].exchange_id}" class="cardContainer row">`;
                    InfoCardsHTML += '<div class="cardImage col-md-3">';
                    InfoCardsHTML += `<img src="/exchange/exchangeImage/${data[i].Image}" alt="">`;
                    InfoCardsHTML += '</div>';

                    //put the content into exchange container
                    InfoCardsHTML += '<div class="cardContent col-md-8">';
                    InfoCardsHTML += `<h5 class="cardTitle">${data[i].name}</h5>`
                    InfoCardsHTML += `<div class="cardRating"><i class="fas fa-star"></i>：${data[i].round == null ? "-" : data[i].round}</div>`;
                    InfoCardsHTML += `<div class="cardAddress">地址：${data[i].address}</div>`;
                    InfoCardsHTML += `<div class="cardArea">地區：${data[i].area}</div>`;
                    InfoCardsHTML += '</div>';
                    InfoCardsHTML += '</a>';
                    InfoCardsHTML += '</div>';
                }
                document.querySelector("#userDisplay").innerHTML = InfoCardsHTML;
            }
        })
    })
}

async function userDisplayComment(){
    document.querySelector("#userComment").addEventListener('click', async (event) => {
        event.preventDefault();
        const fetchUserComment = await fetch('/user/userComment');
        const result = await fetchUserComment.json();
        console.log(result.response);
        
        let container = $('#pagination');
        container.pagination({
            dataSource: result.response,
            pageSize: 4,
            callback: function (data, pagination) {
                document.querySelector("#userDisplay").innerHTML = "";
                let InfoCardsHTML = "";
                if(data.length === 0){
                    document.querySelector("#userDisplay").innerHTML = "<p>沒有紀錄</p>"
                    return;
                }
                for (let i = 0; i < data.length; i++) {
                    InfoCardsHTML += `<a href="/exchange/exchangePage/${data[i].exchange_id}" class="commentContainer row">`
                    InfoCardsHTML += `<div class="exchangeTitle col-8">Re: ${data[i].name}</div>`;
                    InfoCardsHTML += `<div class="commentTitle col-8">標題：${data[i].title}</div>`;
                    InfoCardsHTML += `<div class="commentRating col-8">評分：${data[i].rating}</div>`;
                    InfoCardsHTML += `<div class="commentRating col-8">評語：${data[i].content}</div>`;
                    InfoCardsHTML += `<div class="commentDate col-8">${data[i].date_trunc.replace(/T|.000Z/g," ")}</div>`;
                    InfoCardsHTML += '</a>';
                   
                }
                document.querySelector("#userDisplay").innerHTML = InfoCardsHTML;
            }
        })

    })
   
}


async function userDisplayDonation(){
    document.querySelector("#userDonate").addEventListener('click', async(event)=>{
        event.preventDefault();
        const fetchDonateRecord = await fetch('/user/donateRecord');
        const result = await fetchDonateRecord.json();

        let container = $('#pagination');
        container.pagination({
            dataSource: result.response,
            pageSize: 5,
            callback: function (data, pagination) {
                console.log(data.length);
                document.querySelector("#userDisplay").innerHTML = "";
                let InfoCardsHTML = "";
                if(data.length === 0){
                    document.querySelector("#userDisplay").innerHTML = "<p>沒有紀錄</p>"
                    return;
                }
                for (let i = 0; i < data.length; i++) {
                    InfoCardsHTML += '<div class="donateRecord">'
                    InfoCardsHTML += `<div class="commentTitle col-8">金額：$${data[i].value}</div>`;
                    InfoCardsHTML += `<div class="commentRating col-8">捐款ID：${data[i].orderID}</div>`;
                    InfoCardsHTML += `<div class="commentRating col-8">日期：${data[i].to_char}</div>`;
                    InfoCardsHTML += '</div>`';
                   
                }
                document.querySelector("#userDisplay").innerHTML = InfoCardsHTML;
            }
        })


    })
}

window.onload = () => {
    user()
    userDisplaySave();
    userDisplayComment();
    userDisplayDonation();
}