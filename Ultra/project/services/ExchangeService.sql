-- JUST SELECT all from Exchange
SELECT "Exchanges".*, "Area".area from "Exchanges" INNER JOIN "Area" ON "Exchanges".area_id = "Area".id ORDER BY id;


--JUST SELECT IMAGE from "Exchange_image" JOIN "Image"
SELECT "Exchange_image".*, "Image"."Image", "Exchanges".id from "Exchange_image" INNER JOIN "Image" ON "Exchange_image".image_id = "Image".id LEFT JOIN "Exchanges" ON "Exchange_image".exchange_id = "Exchanges".id; 

SELECT id,link FROM "Exchanges";


SELECT "Exchange_image".*, "Image"."Image", "Exchanges".* from "Exchange_image" INNER JOIN "Image" ON "Exchange_image".image_id = "Image".id LEFT JOIN "Exchanges" ON "Exchange_image".exchange_id = "Exchanges".id where "Exchanges".id = 7;

-- SELECT "Exchange_image".*, "Image"."Image", "Exchanges".* and AVG rating from 4 table
SELECT "Exchange_image".*, "Image"."Image", "Exchanges".*,  ROUND(AVG("Exchange_comment".rating),2)
from "Exchange_image" INNER JOIN "Image" ON "Exchange_image".image_id = "Image".id 
LEFT JOIN "Exchanges" ON "Exchange_image".exchange_id = "Exchanges".id 
LEFT JOIN "Exchange_comment" ON "Exchange_image"."exchange_id" = "Exchange_comment"."exchange_id"
where "Exchanges".id = 7
GROUP BY "Exchanges".id,"Exchange_image".ID, "Image"."Image";

-- SELECT "Exchanges".*, "Area".area, "Exchange_image".*, "Image"."Image", and AVG rating from 5 table
SELECT "Exchanges".*, "Area".area, "Exchange_image".*, "Image"."Image", ROUND(AVG("Exchange_comment".rating),2)
from "Exchanges" INNER JOIN "Area" ON "Exchanges".area_id = "Area".id
LEFT JOIN  "Exchange_image" ON "Exchanges".id = "Exchange_image"."exchange_id"
LEFT JOIN  "Image" ON "Exchange_image".image_id = "Image".id
LEFT JOIN  "Exchange_comment" ON "Exchanges".id = "Exchange_comment"."exchange_id"
GROUP BY "Exchanges".id,"Exchange_image".ID, "Image"."Image", "Area".area;


SELECT "Area".id FROM "Area" WHERE "Area"."area" = '香港';