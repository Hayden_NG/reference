import axios from "axios";

export class FxInfoService {

    async getMinChart(fromCur: string, toCur: string, interval: number): Promise<any[]> {

        let response = await axios({
            "method": "GET",
            "url": "https://alpha-vantage.p.rapidapi.com/query",
            "headers": {
                "content-type": "application/octet-stream",
                "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
                "x-rapidapi-key": "d8b6be954bmsh453b3dae7a76ecbp1308a4jsnd13c65da6c59"
            }, "params": {
                "datatype": "json",
                "outputsize": "compact",
                "function": "FX_INTRADAY",
                "to_symbol": `${toCur}`,
                "interval": `${interval}min`,
                "from_symbol": `${fromCur}`
            }
        });

        let reverseData: any[] = [];
        const priceHistory = response.data[`Time Series FX (${interval}min)`]

        let result = Object.entries(priceHistory);

        for (let i = 0; i < result.length; i++) {
            reverseData.unshift(result[i])
        }

        let dataArray = [];
        for (let i = 0; i < reverseData.length; i++) {
            dataArray.push({
                x: new Date(reverseData[i][0]),
                y: [
                    parseFloat(reverseData[i][1]['1. open']),
                    parseFloat(reverseData[i][1]['2. high']),
                    parseFloat(reverseData[i][1]['3. low']),
                    parseFloat(reverseData[i][1]['4. close'])]
            })
        }

        return dataArray;
    }

    async getDayChart(fromCur: string, toCur: string): Promise<any[]> {

        let response = await axios({
            "method": "GET",
            "url": "https://alpha-vantage.p.rapidapi.com/query",
            "headers": {
                "content-type": "application/octet-stream",
                "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
                "x-rapidapi-key": "d8b6be954bmsh453b3dae7a76ecbp1308a4jsnd13c65da6c59"
            }, "params": {
                "datatype": "json",
                "outputsize": "compact",
                "function": "FX_DAILY",
                "to_symbol": `${toCur}`,
                "from_symbol": `${fromCur}`
            }
        });

        let reverseData: any[] = [];
        const priceHistory = response.data["Time Series FX (Daily)"]

        let result = Object.entries(priceHistory);

        for (let i = 0; i < result.length; i++) {
            reverseData.unshift(result[i])
        }

        let dataArray = [];
        for (let i = 0; i < reverseData.length; i++) {
            dataArray.push({
                x: new Date(reverseData[i][0]),
                y: [
                    parseFloat(reverseData[i][1]['1. open']),
                    parseFloat(reverseData[i][1]['2. high']),
                    parseFloat(reverseData[i][1]['3. low']),
                    parseFloat(reverseData[i][1]['4. close'])]
            })
        }
        return dataArray;
    }

    async getWeekChart(fromCur: string, toCur: string): Promise<any[]> {

        let response = await axios({
            "method": "GET",
            "url": "https://alpha-vantage.p.rapidapi.com/query",
            "headers": {
                "content-type": "application/octet-stream",
                "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
                "x-rapidapi-key": "d8b6be954bmsh453b3dae7a76ecbp1308a4jsnd13c65da6c59"
            }, "params": {
                "datatype": "json",
                "outputsize": "compact",
                "function": "FX_WEEKLY",
                "to_symbol": `${toCur}`,
                "from_symbol": `${fromCur}`
            }
        });

        let reverseData: any[] = [];
        const priceHistory = response.data['Time Series FX (Weekly)']

        let result = Object.entries(priceHistory);

        let yearData = result.slice(0, 156)

        for (let i = 0; i < yearData.length; i++) {
            reverseData.unshift(result[i])
        }

        let dataArray = [];
        for (let i = 0; i < reverseData.length; i++) {
            dataArray.push({
                x: new Date(reverseData[i][0]),
                y: [
                    parseFloat(reverseData[i][1]['1. open']),
                    parseFloat(reverseData[i][1]['2. high']),
                    parseFloat(reverseData[i][1]['3. low']),
                    parseFloat(reverseData[i][1]['4. close'])]
            })
        }
        return dataArray;
    }

    async getQuote(fromCur: string, toCur: string): Promise<any[]> {

        let response = await axios({
            "method": "GET",
            "url": "https://alpha-vantage.p.rapidapi.com/query",
            "headers": {
                "content-type": "application/octet-stream",
                "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
                "x-rapidapi-key": "d8b6be954bmsh453b3dae7a76ecbp1308a4jsnd13c65da6c59"
            }, "params": {
                "function": "CURRENCY_EXCHANGE_RATE",
                "to_currency": `${toCur}`,
                "from_currency": `${fromCur}`
            }
        });

        const quote = response.data["Realtime Currency Exchange Rate"]

        return quote;
    }

    async getDailyDif(fromCur: string, toCur: string) {

        let response = await axios({
            "method": "GET",
            "url": "https://alpha-vantage.p.rapidapi.com/query",
            "headers": {
                "content-type": "application/octet-stream",
                "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
                "x-rapidapi-key": "d8b6be954bmsh453b3dae7a76ecbp1308a4jsnd13c65da6c59"
            }, "params": {
                "datatype": "json",
                "outputsize": "compact",
                "function": "FX_DAILY",
                "to_symbol": `${toCur}`,
                "from_symbol": `${fromCur}`
            }
        });


        const priceHistory = response.data["Time Series FX (Daily)"]

        var result: any[] = Object.entries(priceHistory);

        const difPercent = ((parseFloat(result[0][1]['4. close']) /
            parseFloat(result[1][1]['4. close']) - 1) * 100).toFixed(3);

        const curPrice = parseFloat(result[0][1]['4. close'])

        return { difPercent, curPrice };
    }
}