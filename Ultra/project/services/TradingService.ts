import Knex from "knex";

export class TradingService {
    constructor(private knex: Knex) { }

    async showPortfolio(id: number) {
        const result = await this.knex("Portfolio2").where({
            user_id: id
        }).select()
        return result;
    }

    async checkAmount(id: number, from_currency: string, amount: number) {
        const status: Object[] = await this.knex("Portfolio2").where({
            user_id: id,
            holding_currency: from_currency
        }).select('amount')

        return status[0]["amount"] - amount > 0;
    }

    async orderInstruction(id: number, from_cur: string, to_cur: string, amount: number) {
        const netAmount: Object[] = await this.knex("Portfolio2").where({
            user_id: id,
            holding_currency: from_cur
        }).select('amount')

        await this.knex("Portfolio2").where({
            user_id: id,
            holding_currency: from_cur
        }).update({
            amount: netAmount[0]["amount"] - amount
        })

        const minData1 = await this.knex.select().from("Min_chart1");
        const minData2 = await this.knex.select().from("Min_chart2");

        const mergedMinData = minData1.concat(minData2);

        let reverseMinData = [];
        for (let i = 0; i < mergedMinData.length; i++) {
            reverseMinData.unshift(mergedMinData[i])
        }

        let quoteResult = {}; // 最新Quote價
        for (let i = 0; i < reverseMinData.length; i++) {
            if (!quoteResult[reverseMinData[i].from_cur]) {
                quoteResult[reverseMinData[i].from_cur] = reverseMinData[i].close
            }
        }

        if (from_cur == "HKD") {
            const quote = quoteResult[to_cur]

            await this.knex("Trading_history").insert({
                from_currency: from_cur,
                from_amount: amount,
                to_currency: to_cur,
                fx_rate: quote,
                to_amount: amount / quote,
                user_id: id
            })

            const netAmount: Object[] = await this.knex("Portfolio2").where({
                user_id: id,
                holding_currency: to_cur
            }).select('amount')

            await this.knex("Portfolio2").where({
                user_id: id,
                holding_currency: to_cur
            }).update({
                amount: parseFloat((netAmount[0]["amount"] + amount / quote).toFixed(3))
            })

        } else if (to_cur == "HKD") {
            const quote = quoteResult[from_cur];

            await this.knex("Trading_history").insert({
                from_currency: from_cur,
                from_amount: amount,
                to_currency: to_cur,
                fx_rate: quote,
                to_amount: amount * quote,
                user_id: id
            })

            const netAmount: Object[] = await this.knex("Portfolio2").where({
                user_id: id,
                holding_currency: to_cur
            }).select('amount')

            await this.knex("Portfolio2").where({
                user_id: id,
                holding_currency: to_cur
            }).update({
                amount: parseFloat((netAmount[0]["amount"] + amount * quote).toFixed(3))
            })

        } else {
            const quote1 = quoteResult[from_cur];
            const quote2 = quoteResult[to_cur];
            const convertedQuote = parseFloat((quote1 / quote2).toFixed(6));


            await this.knex("Trading_history").insert({
                from_currency: from_cur,
                from_amount: amount,
                to_currency: to_cur,
                fx_rate: convertedQuote,
                to_amount: amount * convertedQuote,
                user_id: id
            })

            const netAmount: Object[] = await this.knex("Portfolio2").where({
                user_id: id,
                holding_currency: to_cur
            }).select('amount')

            await this.knex("Portfolio2").where({
                user_id: id,
                holding_currency: to_cur
            }).update({
                amount: parseFloat((netAmount[0]["amount"] + amount * convertedQuote).toFixed(3))
            })
        }
    }


    async restartPortfolio(id: number) {
        await this.knex("Portfolio2").where({
            user_id: id
        }).del()

        await this.knex("Portfolio2").insert([
            {
                holding_currency: "HKD",
                amount: 1000000,
                user_id: id
            },
            {
                holding_currency: "JPY",
                amount: 0,
                user_id: id
            }, {
                holding_currency: "USD",
                amount: 0,
                user_id: id
            }, {
                holding_currency: "EUR",
                amount: 0,
                user_id: id
            }, {
                holding_currency: "CNY",
                amount: 0,
                user_id: id
            }, {
                holding_currency: "AUD",
                amount: 0,
                user_id: id
            }, {
                holding_currency: "CAD",
                amount: 0,
                user_id: id
            },
            {
                holding_currency: "CHF",
                amount: 0,
                user_id: id
            },
            {
                holding_currency: "TWD",
                amount: 0,
                user_id: id
            }
        ])

        await this.knex("Trading_history").where({
            user_id: id
        }).del()
    }


    async showTradingHistory(id: number) {
        const result = this.knex("Trading_history").where({
            user_id: id
        }).select();

        return result;
    }

    async showConvertedAmt(from_cur: string, to_cur: string, amount: number) {
        const minData1 = await this.knex.select().from("Min_chart1");
        const minData2 = await this.knex.select().from("Min_chart2");

        const mergedMinData = minData1.concat(minData2);

        let reverseMinData = [];
        for (let i = 0; i < mergedMinData.length; i++) {
            reverseMinData.unshift(mergedMinData[i])
        }

        let quoteResult = {}; // 最新Quote價
        for (let i = 0; i < reverseMinData.length; i++) {
            if (!quoteResult[reverseMinData[i].from_cur]) {
                quoteResult[reverseMinData[i].from_cur] = reverseMinData[i].close
            }
        }

        if (from_cur == "HKD") {
            const quote = quoteResult[to_cur]
            const toAmount = amount / quote

            return [quote, toAmount];

        } else if (to_cur == "HKD") {
            const quote = quoteResult[from_cur];
            const toAmount = amount * quote

            return [quote, toAmount];

        } else {
            const quote1 = quoteResult[from_cur];
            const quote2 = quoteResult[to_cur];
            const convertedQuote = parseFloat((quote1 / quote2).toFixed(6));

            const toAmt = amount * convertedQuote

            return [convertedQuote, toAmt]
        }
    }
}