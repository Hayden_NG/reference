import knex from "knex"
import puppeteer from 'puppeteer'

export class ExchangeService {

    constructor(private knex: knex) { }

    async getExchanges() {
        const result = await this.knex.raw(/*SQL*/`SELECT "Exchanges".*, "Area".area, "Exchange_image".*, "Image"."Image", ROUND(AVG("Exchange_comment".rating),2)
        from "Exchanges" INNER JOIN "Area" ON "Exchanges".area_id = "Area".id
        LEFT JOIN  "Exchange_image" ON "Exchanges".id = "Exchange_image"."exchange_id"
        LEFT JOIN  "Image" ON "Exchange_image".image_id = "Image".id
        LEFT JOIN  "Exchange_comment" ON "Exchanges".id = "Exchange_comment"."exchange_id"
        GROUP BY "Exchanges".id,"Exchange_image".ID, "Image"."Image", "Area".area ORDER BY "Exchange_image".id;
        `);

        const exchanges = result.rows;
        // console.log(exchanges);
        return exchanges;
    }

    async getExchangesImage() {
        const result = await this.knex.raw(/*SQL*/`SELECT "Exchange_image".*, "Image"."Image", "Exchanges".id from "Exchange_image" INNER JOIN "Image" ON "Exchange_image".image_id = "Image".id LEFT JOIN "Exchanges" ON "Exchange_image".exchange_id = "Exchanges".id;`);
        const exchangesImage = result.rows;
        return exchangesImage;
    }

    async getOneExchange(id: number | string) {
        const result = await this.knex.raw(/*SQL*/`SELECT "Exchange_image".*, "Image"."Image", "Exchanges".*,  ROUND(AVG("Exchange_comment".rating),2)
        from "Exchange_image" INNER JOIN "Image" ON "Exchange_image".image_id = "Image".id 
        LEFT JOIN "Exchanges" ON "Exchange_image".exchange_id = "Exchanges".id 
        LEFT JOIN "Exchange_comment" ON "Exchange_image"."exchange_id" = "Exchange_comment"."exchange_id"
        where "Exchanges".id = ${id}
        GROUP BY "Exchanges".id,"Exchange_image".ID, "Image"."Image";`)
        const exchange = result.rows;
        // console.log(exchange);
        return exchange;
    }

    async createExchange(formObj: object, filename: string) {
        // console.log(formObj["exchangeName"]);
        // console.log(filename);

        const trx = await this.knex.transaction();
        try {
            const area_id = await trx.raw(/*SQL*/`SELECT "Area".id FROM "Area" WHERE "Area"."area" = '${formObj["areaOption"]}'`)


            const result = await trx.raw(/*SQL*/`INSERT INTO "Exchanges" (name, area_id, address, intro, link, tel) values(?, ?, ? ,? ,? ,?) RETURNING id`,
                [formObj["exchangeName"],
                area_id.rows[0].id,
                formObj["exchangeAddress"],
                formObj["exchangeIntro"],
                formObj["exchangeLink"],
                formObj["exchangeTel"]
                ])

            // console.log(result.rows)

            const image = await trx.raw(/*SQL*/`INSERT INTO "Image" ("Image") values (?) RETURNING id`, [filename])

            // console.log(image.rows)

            await trx("Exchange_image").insert([
                {
                    exchange_id: result.rows[0].id,
                    image_id: image.rows[0].id
                }
            ]);
            
            //Capture the innerHTML of rate table from the link
            const browser = await puppeteer.launch({
                // headless: false,
                args: ['–no-sandbox', '–disable-setuid-sandbox'],
                ignoreDefaultArgs: ['–disable-extensions']
            });
            const page = await browser.newPage();
            await page.goto(`${formObj["exchangeLink"]}`);
    
            const rateBoard = await page.$(".rate-board")
            // const aHandle = await page.evaluateHandle(() => document.querySelector(".f4 w-100 center"));
            const resultHandle = await page.evaluateHandle(body => body.innerHTML, rateBoard);
            const results = await resultHandle.jsonValue()
            
            console.log(typeof(results));
    
            await browser.close();

            await trx("rateHTML").insert([
                { exchange_id: result.rows[0].id, innerhtml: results }
            ]).returning("id");
            

            console.log("[info] transaction commit");
            await trx.commit();
        }catch(err){
            console.error(err);
            console.log("[info] transaction rollback");
            await trx.rollback();
        }finally{
            await trx.destroy();
        }
        
    }


    
}