import {ExchangeService} from './ExchangeService';
import Knex from "knex"



const knexConfig = require('../knexfile');

// const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
const knex = Knex(knexConfig["testing"]);



describe('UserService', ()=> {

    it('getUserComment', async()=>{
        //Arrange
        const exchangeService = new ExchangeService(knex);

        //Act
        const result = await exchangeService.getExchanges();
       

        console.log(result[0].created_at.toString());

        //Assert
        expect(result).toEqual(
            [
                {
                 "Image": "exchange1.jpg",
                 "address": "尖沙咀彌敦道36-44號重慶大廈地下G80B號舖",
                 "area": "九龍",
                 "area_id": 2,
                 "created_at": result[0].created_at,
                 "exchange_id": 1,
                 "id": 1,
                 "image_id": 1,
                 "intro": "經營金錢服務牌照號碼 16-11-01985",
                 "link": "https://www.ybex.io/providers/066df5e1-72f7-a7ca-919d-f58d4627ce7b/c117be22-ba41-ed7f-a66c-cedbed69a74f",
                 "name": "Eman Exchange HK",
                 "round": null,
                 "tel": "+85237021812",
                 "updated_at": result[0].updated_at
               },
                {
                 "Image": "exchange2.jpg",
                 "address": "香港銅鑼灣糖街15-23號銅鑼灣中心商場地庫B9-B10號鋪",
                 "area": "香港",
                 "area_id": 1,
                 "created_at": result[1].created_at,
                 "exchange_id": 2,
                 "id": 2,
                 "image_id": 2,
                 "intro": "經營金錢服務牌照號碼 12-07-00347",
                 "link": "https://www.ybex.io/providers/62364a61-e296-406d-b1c1-fcce501b4e94/28afce12-6a0b-45fc-800b-fbbc567df0a3",
                 "name": "專業外幣兌換 – 銅鑼灣中心",
                 "round": null,
                 "tel": "+85228056310",
                 "updated_at": result[1].updated_at
               },
                {
                 "Image": "exchange3.png",
                 "address": "香港新界港鐵沙田站 35 號舖",
                 "area": "新界",
                 "area_id": 3,
                 "created_at": result[2].created_at,
                 "exchange_id": 3,
                 "id": 3,
                 "image_id": 3,
                 "intro": "第一國際資源有限公司成立於1979年，本公司憑藉多年的豐富經驗，專營各國外幣及人民幣兌換，信譽昭著，取價公道，並提供特惠匯率，深得客戶愛戴。分店網絡遍佈九龍、新界各地，兌換外幣更方便快捷",
                 "link": "https://www.ybex.io/providers/765c9a56-c176-4af1-9589-f4b24d8a0030/1e696130-2337-4894-90f9-ea8f2be7aba8",
                 "name": "第一兌換店 (沙田站) – 港鐡沙田站 經營金錢服務牌照號碼",
                 "round": null,
                 "tel": "+85226062218",
                 "updated_at": result[2].updated_at
               },
             ]


        );

    })


})


