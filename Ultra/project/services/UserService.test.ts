import {UserService} from './UserService';
import Knex from "knex"



const knexConfig = require('../knexfile');

// const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
const knex = Knex(knexConfig["testing"]);



describe('UserService', ()=> {

    it('getUserComment', async()=>{
        //Arrange
        const userService = new UserService(knex);

        //Act
        const result = await userService.getUserComment(15);


        //Assert
        expect(result).toEqual([]);

    })

    it('getUserSave', async()=>{
        //Arrange
        const userService = new UserService(knex);

        //Act
        const result = await userService.getUserSave(15);

        //Assert
        expect(result).toEqual([]);
    })








})