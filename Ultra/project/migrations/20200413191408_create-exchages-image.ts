import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('Image', (table)=> {
        table.increments();
        table.string("Image").notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('Exchange_image', (table)=>{
        table.increments();
        table.integer("exchange_id").notNullable();
        table.foreign("exchange_id").references("Exchanges.id");
        table.integer("image_id").notNullable();
        table.foreign("image_id").references("Image.id");
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('Exchange_image');
    await knex.schema.dropTable('Image');
}
