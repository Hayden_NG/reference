import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("Min_chart1", table => {
        table.increments();
        table.string("from_cur")
        table.string("to_cur")
        table.dateTime("date");
        table.float("open");
        table.float("high");
        table.float("low");
        table.float("close");
    })

    await knex.schema.createTable("Min_chart2", table => {
        table.increments();
        table.string("from_cur")
        table.string("to_cur")
        table.dateTime("date");
        table.float("open");
        table.float("high");
        table.float("low");
        table.float("close");
    })

    await knex.schema.createTable("Day_chart1", table => {
        table.increments();
        table.string("from_cur")
        table.string("to_cur")
        table.date("date");
        table.float("open");
        table.float("high");
        table.float("low");
        table.float("close");
    })

    await knex.schema.createTable("Day_chart2", table => {
        table.increments();
        table.string("from_cur")
        table.string("to_cur")
        table.date("date");
        table.float("open");
        table.float("high");
        table.float("low");
        table.float("close");
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("Min_chart1");
    await knex.schema.dropTable("Min_chart2");
    await knex.schema.dropTable("Day_chart1");
    await knex.schema.dropTable("Day_chart2");
}

