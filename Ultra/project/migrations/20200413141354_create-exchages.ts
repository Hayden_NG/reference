import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("Area", table=> {
        table.increments();
        table.string("area").notNullable();
        table.timestamps(false, true);
    })


    await knex.schema.createTable("Exchanges", table=> {
        table.increments();
        table.string("name").notNullable().unique();
        table.integer("area_id").notNullable();
        table.foreign("area_id").references("Area.id")
        table.string("address").notNullable()
        table.text("intro");
        table.text("link");
        table.string("tel");
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("Exchanges");
    await knex.schema.dropTable("Area");
}

