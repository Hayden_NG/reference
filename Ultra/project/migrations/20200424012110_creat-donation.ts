import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {

    await knex.schema.createTable("donation", table=> {
        table.increments();
        table.decimal("value", 5, 2).notNullable();
        table.string("orderID").notNullable().unique();
        table.timestamps(false, true);
    })

    await knex.schema.createTable("user_donation", table=> {
        table.increments();
        table.integer("value_id").notNullable();
        table.foreign("value_id").references("donation.id");
        table.integer("user_id").notNullable();
        table.foreign("user_id").references("Users.id");
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("user_donation");
    await knex.schema.dropTable("donation");
}
