import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable("Exchange_comment", table=> {
        table.increments();
        table.integer("user_id").notNullable();
        table.foreign("user_id").references("Users.id");
        table.integer("exchange_id").notNullable();
        table.foreign("exchange_id").references("Exchanges.id");
        table.integer("rating").notNullable();
        table.string("title").notNullable()
        table.text("content").notNullable();
        table.timestamps(false, true);
    })
    
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable("Exchange_comment");
}
