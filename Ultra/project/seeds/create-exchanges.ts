import * as Knex from "knex";
// import { create } from "domain";

// export async function seed(knex: Knex): Promise<any> {
//     // Deletes ALL existing entries
//     return knex("table_name").del()
//         .then(() => {
//             // Inserts seed entries
//             return knex("table_name").insert([
//                 { id: 1, colName: "rowValue1" },
//                 { id: 2, colName: "rowValue2" },
//                 { id: 3, colName: "rowValue3" }
//             ]);
//         });
// };

export async function seed(knex: Knex): Promise<any> {
    try {
        await knex("Exchange_image").del();
        await knex("Image").del();
        await knex("Exchanges").del();
        await knex("Area").del();
        
        


        const areaID: number[] = await knex("Area").insert([
            { id: 1, area: "香港" },
            { id: 2, area: "九龍" },
            { id: 3, area: "新界" }
        ]).returning("id");

        const exchangeID: number[] = await knex("Exchanges").insert([
            {
                name: "Eman Exchange HK",
                area_id: areaID[1],
                address: "尖沙咀彌敦道36-44號重慶大廈地下G80B號舖",
                intro: "經營金錢服務牌照號碼 16-11-01985",
                link: "https://www.ybex.io/providers/066df5e1-72f7-a7ca-919d-f58d4627ce7b/c117be22-ba41-ed7f-a66c-cedbed69a74f",
                tel: "+85237021812"
            },
            {
                name: "專業外幣兌換 – 銅鑼灣中心",
                area_id: areaID[0],
                address: "香港銅鑼灣糖街15-23號銅鑼灣中心商場地庫B9-B10號鋪",
                intro: "經營金錢服務牌照號碼 12-07-00347",
                link: "https://www.ybex.io/providers/62364a61-e296-406d-b1c1-fcce501b4e94/28afce12-6a0b-45fc-800b-fbbc567df0a3",
                tel: "+85228056310"
            },
            {
                name: "第一兌換店 (沙田站) – 港鐡沙田站 經營金錢服務牌照號碼",
                area_id: areaID[2],
                address: "香港新界港鐵沙田站 35 號舖",
                intro: "第一國際資源有限公司成立於1979年，本公司憑藉多年的豐富經驗，專營各國外幣及人民幣兌換，信譽昭著，取價公道，並提供特惠匯率，深得客戶愛戴。分店網絡遍佈九龍、新界各地，兌換外幣更方便快捷",
                link: "https://www.ybex.io/providers/765c9a56-c176-4af1-9589-f4b24d8a0030/1e696130-2337-4894-90f9-ea8f2be7aba8",
                tel: "+85226062218"
            }
        ]).returning("id");


        // --------------create-exchanges-images.ts-------------------

        // await knex("Exchange_image").del();
        // await knex("Image").del();

        const trx = await knex.transaction();

        try {
            const imageID: number[] = await trx("Image").insert([
                { Image: "exchange1.jpg" },
                { Image: "exchange2.jpg" },
                { Image: "exchange3.png" },
            ]).returning("id");


            await trx("Exchange_image").insert([
                { exchange_id: exchangeID[0], "image_id": imageID[0] },
                { exchange_id: exchangeID[1], "image_id": imageID[1] },
                { exchange_id: exchangeID[2], "image_id": imageID[2] }
            ]);

            console.log("[info] transaction commit");
            await trx.commit();
        }
        catch (err) {
            console.error(err)
            console.log("[info] transaction rollback");
            await trx.rollback();
        }
    }
    catch (err) {
        console.error(err)
    } finally {
        await knex.destroy();
    }
}
