import Knex from "knex";

export async function seed(knex: Knex): Promise<any> {

    try {
        await knex.raw(/*SQL*/`DELETE FROM "Portfolio_currency"`);
        await knex.raw(/*SQL*/`DELETE FROM "Portfolio"`);
        await knex.raw(/*SQL*/`DELETE FROM "Users"`);

        const trx = await knex.transaction();

        try {
            const result = await trx.raw(/*SQL*/`INSERT INTO "Users" (username,password,nickname,icon,"isAdmin") VALUES (?,?,?,?,?) RETURNING id`, ['project2', '1234', 'nmsl', '', 1]);
            const userId = result.rows[0].id;

            const result2 = await trx.raw(/*SQL*/`INSERT INTO "Portfolio" (name,currency,amount,up_down,up_down_percent,user_id) VALUES (?,?,?,?,?,?) RETURNING id`, ['project2', 'HKD', 1000000, 0, 0, userId])
            const portfolioId = result2.rows[0].id;

            await trx.raw(/*SQL*/`INSERT INTO "Portfolio_currency" (amount,from_currency,to_currency,fx_rate,"userID",exchange_id,portfolio_id) VALUES (?,?,?,?,?,?,?)`, [300000, 'HKD', 'USD', 7.76605, 1, 1, portfolioId])

            await trx.commit();
        } catch (e) {
            console.error(e)
            await trx.rollback()
        }
        await knex.destroy();

    } catch (e) {
        console.error(e)
    }
};
