import Knex from "knex";
import puppeteer from 'puppeteer';

const knexConfig = require("../knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

async function capHtml() {
    const result = await knex.raw(/*SQL*/`SELECT id, link FROM "Exchanges";`);

    const linkSArr = result.rows;

    const trx = await knex.transaction();
    await trx ("rateHTML").del();
    try{

    
    for(const link of linkSArr){ 
        // console.log(linksArr[0]["link"]);

        // const specificObj = linksArr.find( (element:object) => element["id"] == id )
        // console.log(specificObj.link);

        const browser = await puppeteer.launch({
            // headless: false,
            args: ['–no-sandbox', '–disable-setuid-sandbox'],
            ignoreDefaultArgs: ['–disable-extensions']
        });
        const page = await browser.newPage();
        await page.goto(`${link.link}`);

        const rateBoard = await page.$(".rate-board")
        // const aHandle = await page.evaluateHandle(() => document.querySelector(".f4 w-100 center"));
        const resultHandle = await page.evaluateHandle(body => body.innerHTML, rateBoard);
        const result = await resultHandle.jsonValue()
        
        console.log(typeof(result));

        await browser.close();
        await trx("rateHTML").insert([
            { exchange_id: link.id, innerhtml: result }
        ]).returning("id");
        }

        console.log("[info]insert html successfully");

        await trx.commit();
        console.log("[info]transactions done");
    }catch(err){
        console.error(err);
        await trx.rollback();
    }finally {
        await knex.destroy();
    }

}




capHtml();