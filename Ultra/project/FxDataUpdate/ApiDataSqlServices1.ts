import axios from "axios";
import Knex from "knex";

async function getMinChart(fromCur: string, toCur: string, interval: number): Promise<any[]> {

    let response = await axios({
        "method": "GET",
        "url": "https://alpha-vantage.p.rapidapi.com/query",
        "headers": {
            "content-type": "application/octet-stream",
            "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
            "x-rapidapi-key": "d8b6be954bmsh453b3dae7a76ecbp1308a4jsnd13c65da6c59"
        }, "params": {
            "datatype": "json",
            "outputsize": "compact",
            "function": "FX_INTRADAY",
            "to_symbol": `${toCur}`,
            "interval": `${interval}min`,
            "from_symbol": `${fromCur}`
        }
    });

    const priceHistory = response.data[`Time Series FX (${interval}min)`]

    var result = Object.entries(priceHistory);

    let reverseData: any[] = [];
    for (let i = 0; i < result.length; i++) {
        reverseData.unshift(result[i])
    }

    let dataArray = [];
    for (let i = 0; i < reverseData.length; i++) {
        dataArray.push([
            new Date(reverseData[i][0]),
            [
                parseFloat(reverseData[i][1]['1. open']),
                parseFloat(reverseData[i][1]['2. high']),
                parseFloat(reverseData[i][1]['3. low']),
                parseFloat(reverseData[i][1]['4. close'])]
        ])
    }

    return dataArray;
}

async function getDayChart(fromCur: string, toCur: string): Promise<any[]> {

    let response = await axios({
        "method": "GET",
        "url": "https://alpha-vantage.p.rapidapi.com/query",
        "headers": {
            "content-type": "application/octet-stream",
            "x-rapidapi-host": "alpha-vantage.p.rapidapi.com",
            "x-rapidapi-key": "d8b6be954bmsh453b3dae7a76ecbp1308a4jsnd13c65da6c59"
        }, "params": {
            "datatype": "json",
            "outputsize": "compact",
            "function": "FX_DAILY",
            "to_symbol": `${toCur}`,
            "from_symbol": `${fromCur}`
        }
    });

    let reverseData: any[] = [];
    const priceHistory = response.data["Time Series FX (Daily)"]

    var result = Object.entries(priceHistory);

    for (let i = 0; i < result.length; i++) {
        reverseData.unshift(result[i])
    }

    let dataArray = [];
    for (let i = 0; i < reverseData.length; i++) {
        dataArray.push([
            new Date(reverseData[i][0]),
            [
                parseFloat(reverseData[i][1]['1. open']),
                parseFloat(reverseData[i][1]['2. high']),
                parseFloat(reverseData[i][1]['3. low']),
                parseFloat(reverseData[i][1]['4. close'])]
        ])
    }

    return dataArray;
}

const fxArray1 = [
    ["JPY", "HKD"],
    ["USD", "HKD"],
    ["EUR", "HKD"],
    ["CNY", "HKD"]
]

const fxArray2 = [
    ["AUD", "HKD"],
    ["CAD", "HKD"],
    ["CHF", "HKD"],
    ["TWD", "HKD"]
]


async function updatingMin(fxArray: string[][], table: string) {
    console.log("[info]Updating Min chart data!")
    const knexConfig = require("../knexfile");
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
    try {
        await knex(table).del();
        const trx = await knex.transaction();
        try {

            for (let i = 0; i < fxArray.length; i++) {
                const sixtyMinData = await getMinChart(fxArray[i][0], fxArray[i][1], 60)
                console.log(sixtyMinData.length)
                for (let j = 0; j < sixtyMinData.length; j++) {

                    await trx(table).insert([
                        {
                            from_cur: fxArray[i][0],
                            to_cur: fxArray[i][1],
                            date: sixtyMinData[j][0],
                            open: sixtyMinData[j][1][0],
                            high: sixtyMinData[j][1][1],
                            low: sixtyMinData[j][1][2],
                            close: sixtyMinData[j][1][3]
                        }
                    ])
                }
                console.log(`[Info]Finish ${i + 1} Min chart data`)
            }
            console.log("[info] transaction commit");
            await trx.commit();
        } catch (e) {
            console.error(e);
            await trx.rollback();
        }
    } catch (e) {
        console.error(e);
    } finally {
        await knex.destroy();
    }
}

async function updatingDay(fxArray: string[][], table: string) {
    console.log("[info]Updating Day chart data!")
    const knexConfig = require("../knexfile");
    const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
    try {
        await knex(table).del();
        const trx = await knex.transaction();
        try {

            for (let i = 0; i < fxArray.length; i++) {
                const sixtyMinData = await getDayChart(fxArray[i][0], fxArray[i][1])
                console.log(sixtyMinData.length)
                for (let j = 0; j < sixtyMinData.length; j++) {

                    await trx(table).insert([
                        {
                            from_cur: fxArray[i][0],
                            to_cur: fxArray[i][1],
                            date: sixtyMinData[j][0],
                            open: sixtyMinData[j][1][0],
                            high: sixtyMinData[j][1][1],
                            low: sixtyMinData[j][1][2],
                            close: sixtyMinData[j][1][3]
                        }
                    ])
                }
                console.log(`[Info]Finish ${i + 1} Day chart data`)
            }
            console.log("[info] transaction commit");
            await trx.commit();
        } catch (e) {
            console.error(e);
            await trx.rollback();
        }
    } catch (e) {
        console.error(e);
    } finally {
        await knex.destroy();
    }
}


// setInterval(() => {
updatingMin(fxArray1, "Min_chart1");

setTimeout(() => {
    updatingMin(fxArray2, "Min_chart2");
    setTimeout(() => {
        updatingDay(fxArray1, "Day_chart1");
        setTimeout(() => {
            updatingDay(fxArray2, "Day_chart2");
        }, 240000);
    }, 240000);
}, 240000);

// }, 3600000)

