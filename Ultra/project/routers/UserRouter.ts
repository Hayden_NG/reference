import express from "express"
import { Request, Response } from "express"
import fetch from 'node-fetch';
import { UserService } from "../services/UserService"
import path from 'path';
import { isLoggedIn, isLoggedInToLogin, isLoggedForSQL } from '../guards'

export class UserRouter {
    constructor(private userService: UserService) { }

    router() {
        const router = express.Router()
        router.get("/login/google", this.loginGoogle)
        router.get("/", this.visitorInfo)
        router.post("/logout", this.logout)
        // KinKo
        router.use(express.static(path.join(__dirname, "../public/exchangeInfo")));
        router.use(express.static(path.join(__dirname, "../picture")));
        router.post("/save/:id", this.saveExchange)
        router.get("/save/:id", this.checkSave)
        router.delete("/save/:id", this.cancelSave)
        router.use("/userProfile", isLoggedIn, express.static(path.join(__dirname, "../private/userProfile")))
        router.use("/Donation", isLoggedInToLogin, express.static(path.join(__dirname, "../private/donation")))
        router.get("/userComment", this.showComment)
        router.get("/userSaved", this.showSavedExchange)
        router.post("/userDonation", this.acceptDonation)
        router.get("/donateRecord", isLoggedForSQL, this.donationRecord)
        return router
    }

    loginGoogle = async (req: Request, res: Response) => {
        const accessToken = req.session?.grant.response.access_token;
        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            method: 'get',
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        });
        const result = await fetchRes.json();
        // console.log(result)
        const user = await this.userService.getUsersName(result.email);
        const icon = result.picture;
        const nickname = result.given_name;
        let userId = user ? user.id : undefined;
        if (!user) {
            userId = await this.userService.createUser(result.email, "123456", nickname, icon)
        }
        if (req.session) {
            req.session.user = {
                id: userId
            }
            // console.log(req.session.user.id)
            return res.redirect('/welcome')
        }

    }

    visitorInfo = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                if (req.session.user) {
                    const visitor = await this.userService.getUsersId(req.session.user.id)
                    // console.log(visitor);
                    res.json(visitor)
                }
                else {
                    res.json({ "login": false });
                }
            }
        } catch (e) {
            console.error(e)
            res.status(401).json({ error: e.massage })
            // res.redirect('/connect/google')
        }
    }

    logout = async (req: Request, res: Response) => {
        if (req.session) {
            delete req.session.user
        }

        res.redirect('/')
    }

    saveExchange = async (req: Request, res: Response) => {
        if (req.session && req.session.user) {
            // console.log(req.params.id)
            // console.log(req.session.user.id)
            const exchangeID = parseInt(req.params.id);
            const result = await this.userService.saveExchange(req.session.user.id, exchangeID);
            console.log(result);
            res.json({ message: "save successfully" })

        } else {
            res.json({ message: "you should login first" })
        }

    }

    checkSave = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const exchangeID = parseInt(req.params.id);
                const result = await this.userService.checkExchange(req.session.user.id, exchangeID);
                res.json({ response: result })
            } else {
                res.json({ login: false })
            }
        } catch (err) {
            console.error(err);
            res.json({ massage: err })
        }

    }

    cancelSave = async (req: Request, res: Response) => {
        if (req.session && req.session.user) {
            const exchangeID = parseInt(req.params.id);
            await this.userService.cancelSave(req.session.user.id, exchangeID);
            res.json({ cancel: "success" })
        } else {
            res.json({ login: false })
        }
    }

    showComment = async (req: Request, res: Response) => {
        if (req.session && req.session.user) {
            const result = await this.userService.getUserComment(req.session.user.id);
            res.json({ response: result });
        } else {
            res.json({ login: false })
        }
    }

    showSavedExchange = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const result = await this.userService.getUserSave(req.session.user.id);
                res.json({ response: result });

            } else {
                res.json({ login: false })
            }
        } catch (err) {
            console.error(err);
            res.json({ massage: err })
        }
    }

    acceptDonation = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const value = parseFloat(req.body.value);

                const receiveID = await this.userService.acceptDonation(value, req.body.orderID);
                const donatedUser = await this.userService.donationRecord(receiveID, req.session.user.id);

                console.log(`user[${donatedUser}]: donate success`);
                res.json({ donation: "success" })


            } else {
                res.json({ login: false })
            }
        } catch (err) {
            console.error(err);
            res.json({ errorMassage: err })
        }

    }

    donationRecord = async (req: Request, res: Response) => {
        try {
            const result = await this.userService.selectDonationRecord(req.session?.user.id);
            res.json({response:result});

        } catch (err) {
            console.error(err);
            res.json({ errorMassage: "401 Error" })
        }
    }
    
}