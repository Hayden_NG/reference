import express from 'express';
import { Request, Response } from 'express';
import { ExPageService } from '../services/ExPageService';
import path from 'path';


export class ExPageRouter {
    constructor(private exPageService: ExPageService) { }

    router() {
        const router = express.Router();
        router.use(express.static(path.join(__dirname, "../public/exchangePage")));
        router.get('/exchangePage/:id', this.exchangePage)
        router.get('/rate/:id', this.getRates)
        router.post('/comment', this.postComment)
        router.get('/comments/:id', this.getComment)
        router.delete("/userComment/:id", this.deleComment)
        return router;
    }

    exchangePage = async (req: Request, res: Response) => {
        const exchangePageHTML = path.join(__dirname, '../public/exchangePage/exchangePage.html');
        // console.log(exchangePageHTML);
        res.sendFile(exchangePageHTML);
    }


    getRates = async (req: Request, res: Response) => {
        try {
            // console.log(req.params.id);
            const exchange_ID = parseInt(req.params.id);
            const rateTable = await this.exPageService.getRate(exchange_ID);
            rateTable == undefined? (console.log("not rate table")): (console.log("display rate table"));
            res.json(rateTable);
        }
        catch (err) {
            console.error(err);
            res.status(404).json({ response: false });
        }
    }

    postComment = async (req: Request, res: Response) => {
        try {
            const userID = req.session?.user.id
            const exchangeID = parseInt(req.body.exchangeID)
            const rating = parseInt(req.body.rating)
            const title = req.body.title
            const comment = req.body.comment

            // console.log(`${userID}, ${exchangeID}, ${title}, ${rating}, ${comment}`)

            await this.exPageService.insertComment(userID, exchangeID, rating, title, comment);
            res.json({ insert: "success" });
        }catch(err){
            console.error(err);
            res.json({insert:"fail"})
        }

    }

    getComment =  async (req: Request, res: Response) => {
        const exchangeID = parseInt(req.params.id);
        const comments = await this.exPageService.getComment(exchangeID);
        // console.log(comments);
        res.send(comments);
    }

    deleComment = async (req: Request, res: Response) => {
        try {
            if (req.session && req.session.user) {
                const commentID = parseInt(req.params.id)
                const result = await this.exPageService.deleteComment(commentID, req.session.user.id);
                res.json({message: result});

            } else {
                res.json({ login: "notAdmin" })
            }
        } catch (err) {
            console.error(err);
            res.json({ errorMassage: err })
        }

    }

}
