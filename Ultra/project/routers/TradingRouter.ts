import express from "express"
import { Request, Response } from "express"
import { TradingService } from "../services/TradingService"

export class TradingRouter {
    constructor(private tradingService: TradingService) { }

    router() {
        const router = express.Router();
        router.post("/order", this.orderInstruction);
        router.get("/test", this.test);
        router.post("/restart", this.restartPortfolio);
        router.get("/portfolio_status", this.showPortfolio);
        router.get("/convert_amt", this.showConvertedAmt);
        router.get("/history",this.showTradingHistory);
        return router;
    }

    orderInstruction = async (req: Request, res: Response) => {
        try {
            if (req.body.from_cur == req.body.to_cur) {
                console.log(req.body.from_cur, req.body.to_cur)
                res.status(403).json({ message: "Convert to same currency!" })
                return
            }

            if (req.session) {
                const enoughAmount = await this.tradingService.checkAmount(
                    req.session.user.id, req.body.from_cur, req.body.from_amount)

                if (enoughAmount) {

                    await this.tradingService.orderInstruction(
                        req.session.user.id, req.body.from_cur,
                        req.body.to_cur, req.body.from_amount)

                        res.status(200).json({message:"succssd"})
                } else {
                    res.status(403).json({ message: "You don't have this currency or enough amount!" })
                }

            }
        } catch (e) {
            console.error(e);
        }
    }

    test = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                // const enoughAmount = await this.tradingService.checkAmount(
                //     req.session.user.id, "HKD", 0)


                // if (enoughAmount) {
                // await this.tradingService.orderInstruction(
                //     req.session.user.id, "HKD",
                //     "JPY", 500)

                // await this.tradingService.orderInstruction(
                //     req.session.user.id, "JPY",
                //     "HKD", 6973.5)

                await this.tradingService.orderInstruction(
                    req.session.user.id, "JPY",
                    "USD", 500)

                res.json(true)
                // } else {
                //     res.json({ message: "You don't have enough amount" })
                // }

            }
        } catch (e) {
            console.error(e);
        }
    }

    restartPortfolio = async (req: Request, res: Response) => {
        try {
            console.log("restart")
            if (req.session){
                await this.tradingService.restartPortfolio(req.session.user.id)
            res.json({ message: "Portfolio is restarted" })
            }
            
        } catch (e) {
            console.error(e)
        }
    }

    showPortfolio = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                const status = await this.tradingService.showPortfolio(req.session.user.id);
                res.json(status);
            }
        } catch (e) {
            console.error(e);
        }
    }

    showTradingHistory = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                const tradingHistory = await this.tradingService.showTradingHistory(req.session.user.id)
                if (tradingHistory) {
                    res.json(tradingHistory)
                } else {
                    res.json({ message: "You don't have any trading history" })
                }
            }
        } catch (e) {
            console.error(e);
        }
    }

    showConvertedAmt = async (req: Request, res: Response) => {
        try {
            const convertedAmt = await this.tradingService.showConvertedAmt(
                req.body.from_cur, req.body.to_cur, 0)

            res.json(convertedAmt);
        } catch (e) {
            console.error(e)
        }
    }
}