import { SqlFxDataService } from '../services/SqlFxDataServices';
import express from 'express';
import { Request, Response } from 'express';


export class SqlFxDataRouter {
    constructor(private SqlFxDataService: SqlFxDataService) { }

    router() {
        const router = express.Router();
        router.get("/", this.getFxChartData);
        router.get("/fxQuote", this.getFxQuote);
        return router;
    }

    getFxChartData = async (req: Request, res: Response) => {
        try {
            const data = await this.SqlFxDataService.getFxChartData();
            res.json(data);
        } catch (e) {
            console.error(e);
        }
    }

    getFxQuote = async (req: Request, res: Response) => {
        try {
            const data = await this.SqlFxDataService.getFxQuote();
            res.json(data);
        } catch (e) {
            console.error(e);
        }
    }
}