import { ExchangeService } from '../services/ExchangeService';
import express from 'express';
import { Request, Response } from 'express';
import path from 'path';
import multer from 'multer'

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../public/exchangeInfo/exchangeImage'));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})

const upload = multer({ storage: storage })




export class ExchangeRouter {

    constructor(private exchangeService: ExchangeService) { }

    router() {
        const router = express.Router();
        router.use(express.static(path.join(__dirname, "../public/exchangeInfo")));
        router.use(express.static(path.join(__dirname, "../picture")));
        router.get("/exchangesInfo", this.getInfo)
        router.get("/exchangesImage", this.getExchangesImage)
        router.get("/getExchanges", this.getExchanges)
        router.get("/theExchange/:id", this.getOneExchange)
        router.post('/newExchange', upload.single('photo'), this.postExchange)
        // router.get("/table", this.getTable)

        return router;
    }

    getInfo = async (req: Request, res: Response) => {
        const exchangeHTML = path.join(__dirname, '../public/exchangeInfo/exchanges.html');
        // console.log(exchangeHTML);
        res.sendFile(exchangeHTML);
    }

    getExchangesImage = async (req: Request, res: Response) => {
        try {
            const exchangesImage = await this.exchangeService.getExchangesImage();
            // console.log(exchangesImage);
            res.send(exchangesImage);
        } catch (err) {
            console.log(err.massage);
            res.json({ response: 404 });
        }
    }

    getExchanges = async (req: Request, res: Response) => {
        try {
            const exchanges = await this.exchangeService.getExchanges();
            // console.log(exchanges);
            res.send(exchanges);

        } catch (err) {
            console.log(err.massage);
            res.json({ response: false });
        }

    }

    getOneExchange = async (req: Request, res: Response) => {
        // console.log(`req.params.id:${req.params.id}`)
        const exchange = await this.exchangeService.getOneExchange(req.params.id);
        res.send(exchange);


    }

    postExchange = async (req: Request, res: Response) => {
        try {
            const formObj = req.body
            const filename = req.file.filename
            await this.exchangeService.createExchange(formObj, filename);
            console.log("create successfully")
            res.json({ fetch: "success" });
        }catch(err){
            console.error(err);
            res.json({fetch : "fail"})
            return; 
        }

    }



}