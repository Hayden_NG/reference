export interface Stock {
    id: number;
    name: string;
    listed_no: string;
    per_lot: number;
    exchange_id: number;
    index_id: number;
    category_id: number;
    industry_id: number;
}