import express, { Request, Response } from "express";
import fetch from 'node-fetch';
import { UserService } from "../services/UserService";
import { checkPassword } from "../hash";

// import { checkPassword } from "../hash";

export class UserRouter {
    constructor(private userService: UserService) { }

    router() {
        const router = express();
        router.get('/user', this.getUser);
        router.post('/login', this.login);
        router.get('/logout', this.logout);
        router.get('/google', this.google);
        return router;
    }

    getUser = async (req: Request, res: Response) => {
        try{
        if(req.session && req.session.userID){
            const result = await this.userService.getUserByID(req.session.userID);
            res.status(200).json(result);
            return result;
        }
       
        }
        catch(err){
            console.error(err);
            res.status(500).json({ status: false, message: 'Server Error' });
            return;
        }
    }

    login = async (req: Request, res: Response) => {
        const findUser = await this.userService.getUserByUsername(req.body.username);
        console.log(findUser);
        try {
            if (findUser !== undefined) {
                const findPW = await checkPassword(req.body.password, findUser.password);
                console.log('match:' + findPW);
                if (findPW == true) {
                    if(req.session){
                        req.session.userID = findUser.userID
                        req.session.name = findUser.username;
                        console.log(req.session);
                    }
                   
                    // return res.redirect('/');
                    res.status(200).json({ login: true, exist: true })
                }
                else {
                    res.status(400).json({ password: false })
                }
            }
            else {
                this.userService.createUser(req.body.username, req.body.password);
                res.status(200).json({ createUser: true, login: true })
            }
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ status: false, message: 'Server Error' });
            return;
        }
    }

    logout = async (req:express.Request , res:express.Response)=> {
        if(req.session){
            delete req.session.user;
        }
        res.redirect('/login/login');
    }

    google = async (req:express.Request , res:express.Response)=>{
        console.log(req.session?.grant)
        const accessToken = req.session?.grant.response.access_token;
        const fetchRes = await fetch(
            "https://www.googleapis.com/oauth2/v2/userinfo",
            {
              method: "get",
              headers: {
                Authorization: `Bearer ${accessToken}`
              }
            }
          );
          const result = await fetchRes.json();
          const userDataSet = await this.userService.getUserDataset();
          const users = userDataSet.users;
          const user = users.find((user:any) => user.username == result.email);
          console.log(user);
          let tmpUserId: number;
          if (!user) {
            console.log("first time")
            tmpUserId = await this.userService.createUser(result.email, "kinko1234");
          }else {
            console.log("second time")
            tmpUserId = user.userID;
          }
          if (req.session) {
            req.session.userID = tmpUserId;
            console.log(req.session);
        }
        return res.redirect("/");
    }
}