import express, { Request, Response } from "express";
import path from "path";
import { LoadVideoService } from "../services/LoadVideoService";
import { guardVideoPAge } from "../routers/Rs/isLoggedIn";


export class LoadVideoRouter{
    

    constructor(private loadVideoService : LoadVideoService){}
    
    
    
    router(){
        const router = express();
        router.get('/video/:id',guardVideoPAge,this.loadVideo)
        router.get('/comments/:id', this.readComments)
        router.post('/comments/:id', this.postComments)
        router.get('/addLike/:id', this.addLike)
        router.delete('/deleteComment/:id', this.deleteComment)
        router.post('hello', this.hello)
        return router;
    }

    loadVideo = async(req:Request, res:Response)=>{
        res.sendFile(path.join(__dirname,"../private/VideoPage(template)/video.html"));
    }

    hello = async(req:Request, res:Response)=>{
        const result = await this.loadVideoService.sayHello();
        return result;
    }

    readComments = async(req:Request, res:Response)=>{
        const videoID = req.originalUrl.replace('/comments/','')
        try{
            const comments = await this.loadVideoService.readComments(videoID);
            res.json(comments);
        }
        catch(err){
            console.error(err);
            console.log(`comment${videoID} jsonfile no exists, creat json comments${videoID} now`);
            await this.loadVideoService.creatCommentsJson(videoID);
            res.status(500).json({success: false, createJson: true});
        }
    }

    postComments = async(req:Request, res:Response)=>{
        const videoID = req.originalUrl.replace('/comments/','');
        await this.loadVideoService.postComment(videoID, req.body.comment, req.body.sendUsername)
        res.redirect(req.originalUrl);
    }

    deleteComment = async(req:Request, res:Response)=>{
        const commentId = req.originalUrl.replace('/deleteComment/','');
        console.log("commentid :" + commentId);
        console.log("videoid:" + req.body.videoid);
        await this.loadVideoService.deleteComment(commentId, req.body.videoid);
        res.json({success:true});
    }
    // postComments = async(req:Request, res:Response)=>

    addLike = async(req:Request, res:Response)=>{
        const likeID = req.originalUrl.replace('/addLike/','');
        if(req.session && req.session.userID){
            await this.loadVideoService.addLike(likeID, req.session.userID);
            res.json({addLikeSuccess:true});
            
        }
      

    }

}