import express from "express";
import { Request, Response } from "express";
import { UserServiceRS } from "../../services/Rs/userService";
import {guard} from '../Rs/isLoggedIn'

export class UserRouterRS {
  constructor(private userService: UserServiceRS) {}

  router() {
    const router = express.Router();
    //router.get("/", this.getMemos);
    router.post("/", this.createUser);
    router.get("/currentUser", this.getCurrentUser );
    router.get("/isThereAUser", this.isThereAUser );
    router.get("/userID/:id",guard, this.getUserByID );
    router.post("/currentUser",guard,this.updateRefreshTime );
    router.get("/notification",guard, this.getNotification);
    router.post("/notification",guard, this.saveNotification);
    router.put("/notification",guard,this.setNotificationIsRead);
    router.get("/name",guard,this.getNameofUser);
    return router;
  }

  createUser = async (req: Request, res: Response) => {
    try {
        await this.userService.createUser(req.body.username, req.body.password,false);
       res.status(200).json({status:true,message:"A user is created"});
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error' });
      return ;
    }
  };

  getCurrentUser = async (req: Request, res: Response) => {
      if(req.session && req.session.userID){
        console.log("getCurrentUser")
         console.log(`currentUser:id ${req.session.userID}`);
        try {
            const user = await this.userService.getUserByID(req.session.userID);
            console.log("Now present the user");
            console.log(user);
           res.status(200).json(user);
           return ;
        } catch (e) {
          console.error(e);
          res.status(500).json({ status: false,message:'Server Error' });
          return ;
        }
      }else{
        res.status(200).json({}); 
        return;
      }
  };//return whole user object
  isThereAUser = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
       // console.log("OK");
      try {
         res.status(200).json(true);
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error' });
        return ;
      }
    }else{
      res.status(200).json(false); 
      return;
    }
};//return true or false
  
  getUserByID = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
      try {
          const user = await this.userService.getUserByID(parseInt(req.params.id));
          // console.log("test!!! aaaaaa")
          // console.log(user);
          if(user.name){
            res.status(200).json(user.name);
          }else{
            res.status(200).json(user.username);
          }
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
};//return other people username/name only

getNameofUser = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
      if(req.session.name){
        res.status(200).json(req.session.name);
        return;
      }
      const user = await this.userService.getUserByID(req.session.userID);
      console.log(req.session);
      res.status(200).json(user.username);
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};//return name depends whether the user is logged in with FB or local login
  
updateRefreshTime = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
      try {
          const lastRefreshTime = await this.userService.getAndUpdateLastRefreshTime(req.session.userID);
         res.status(200).json(lastRefreshTime);
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
};//return last refresh date
getNotification = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
      try {
          const notifications = await this.userService.getNotification(req.session.userID);
         res.status(200).json(notifications);
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error, fail to get notifications' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
};
saveNotification = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
      try {
          await this.userService.saveNotification(req.session.userID,req.body.notifications);
         res.status(200).json({status:true,message:"update notifications successfully"});
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error, fail to get notifications' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
};
setNotificationIsRead = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
      try {
          await this.userService.notificationIsRead(req.session.userID);
         res.status(200).json({status:true,message:"all notification isRead is set to true"});
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error, fail to get notifications' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
};

  


}
