import express from "express";
import { Request, Response } from "express";
import { QuestionService } from "../../services/Rs/questionService";
import socketIO from 'socket.io';

export class QuestionRouter {
  //@ts-ignore
  constructor(private questionService: QuestionService,private io:socketIO.Server) {}

  router() {
    const router = express.Router();
    //router.get("/", this.getMemos);
    router.post("/", this.postQuestion);
    router.get("/askedQuestion", this.getUserAskedQuestions );
    router.get("/contribution", this.getAnsweredQuestions );
    router.post("/contribution", this.postAnswer );
    router.delete("/askedQuestion/:id", this.removeQuestion );
    router.put("/askedQuestion/:questionID/:userID",this.chooseBestAnswer);
    router.put("/contribution/:questionID",this.editAnswer);
    router.delete("/contribution/:questionID",this.deleteAnswer);
    router.get("/videoQuestions/:videoID", this.getVideoQuestions);
    return router;
  }
  
  postQuestion = async (req: Request, res: Response) => {
      if(req.session && req.session.userID){
        try {
          const result = await this.questionService.postQuestion(parseInt(req.body.videoID),req.session.userID,req.body.content)
           res.status(200).json({status:true,message:'Raised question successfully'});
           return ;
        } catch (e) {
          console.error(e);
          res.status(500).json({ status: false,message:'Server Error, failed to raised question' });
          return ;
        }
      }else{
        res.status(400).json({ status: false, message: "Please Login First" }); 
        return;
      }
  };
  postAnswer = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
      try {
         const isPost = await this.questionService.postAnswer(req.session.userID, parseInt(req.body.questionID),req.body.content)
         if(isPost){
           const question = await this.questionService.getQuestionByID(parseInt(req.body.questionID))
           this.io.to(`${question.userID}`).emit('answer');
           //this.io.emit('answer','I have answer your questions!');
           res.status(200).json({status:true,message:'Answer successfully'});
          }else{
            
            res.status(200).json({status:false,message:'You have already answered this question'});
         }
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error, failed to raised question' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
};
  getUserAskedQuestions = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
      try {
        const questions = await this.questionService.getAskedQuestion(req.session.userID);
         res.status(200).json(questions);
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error, failed to get asked questions' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
}; //get asked questions, return questions objects
getAnsweredQuestions = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
      const questions = await this.questionService.getAnsweredQuestion(req.session.userID);
      res.status(200).json({contributionQuestions:questions,userID:req.session.userID});
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to get answered questions' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};//return question objects that you have answered, and client's userID
removeQuestion = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       await this.questionService.removeAskedQuestion(req.session.userID,parseInt(req.params.id))
      res.status(200).json({status:true,message:"Delete question successfully"});
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to delete question' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
chooseBestAnswer = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       await this.questionService.chooseBestSolution(parseInt(req.params.userID),parseInt(req.params.questionID))
       res.status(200).json({status:true,message:"Choose the best solution successfully"});
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to choose the best solution' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
editAnswer = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       if(await this.questionService.editAnswer(req.session.userID,parseInt(req.params.questionID),req.body.uploadValue)){
        res.status(200).json({status:true,message:"Edit Answer successfully"});
        const question = await this.questionService.getQuestionByID(parseInt(req.params.questionID));
        this.io.to(`${question.userID}`).emit('answer');
        return;
       }else{
        res.status(500).json({status:false,message: "Failed to edit"})
        return ;
       }     
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to edit the answer' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
deleteAnswer = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       if(await this.questionService.deleteAnswer(req.session.userID,parseInt(req.params.questionID))){
        res.status(200).json({status:true,message:"Delete Answer successfully"});
        return;
       }else{
        res.status(500).json({status:false,message: "Failed to delete answer"})
        return ;
       }     
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to delete the answer' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
getVideoQuestions = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
      const questions = await this.questionService.getVideoQuestion(parseInt(req.params.videoID));
     // console.log(questions);
      res.status(200).json(questions);
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to get video questions' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};



}
