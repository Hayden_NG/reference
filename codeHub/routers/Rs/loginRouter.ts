import express from "express";
import { Request, Response } from "express";
import fetch from 'node-fetch';
import {UserServiceRS} from '../../services/Rs/userService';
import {hashPassword} from '../../services/Rs/bscriptHash';
import {guard} from '../Rs/isLoggedIn';
//import socketIO from 'socket.io';

//import { isObject } from "util";
//var CryptoTS = require("crypto-ts");
//import * as sha256 from "fast-sha256";

export class LoginRouter{
    //@ts-ignore
    constructor(private userService:UserServiceRS){}
    router(){
        const router = express.Router();
         router.get('/facebook',this.loginFacebook);
         router.get('/logout',guard, this.logout);
         
         //router.get('/logout',this.logout);
         return router;
     }
     loginFacebook = async (req:Request,res:Response)=>{
         
        const accessToken = req.session?.grant.response.access_token;
        // if(process.env.FACEBOOK_CLIENT_SECRET!=undefined){
        //     const proof = sha256.hmac(accessToken,process.env.FACEBOOK_CLIENT_SECRET)
        // }
        
        // console.log(accessToken);
       // console.log(req.session?.grant.response.access_token);
         const fetchRes = await fetch('https://graph.facebook.com/me?fields=email,name',{
             method:"get",
             headers:{
                 "Authorization":`Bearer ${accessToken}`,
                 //"appsecret_proof": `${proof}`,
             }
         });
         const result = await fetchRes.json();
    
         let user = await this.userService.getUserByUserName(result.email);
         if(!user){
             //console.log(result.name);
             await this.userService.createUser(result.email,await hashPassword(result.id),false,result.name);
             user = await this.userService.getUserByUserName(result.email);
         }
         if(req.session){
            // console.log(user);
             req.session.userID = user.userID;
             //console.log(req.session.userID);
             req.session.name = result.name;
             res.redirect('/');
             return;
         }
     }
     logout = async (req:express.Request,res:express.Response) =>{
        if(req.session && req.session.userID){
           
            delete  req.session.userID;
        }
        res.redirect('/');
    }

 }