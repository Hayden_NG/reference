import express from "express";
import { Request, Response } from "express";
import { VideoService } from "../../services/Rs/videoService";

export class VideoRouter {
  constructor(private videoService: VideoService) {}

  router() {
    const router = express.Router();
    //router.get("/", this.getMemos);
    router.post("/", this.postVideo);
    router.get("/watchedVideo", this.getWatchedVideos );
    router.get("/savedVideo", this.getSavedVideos );
    router.post("/savedVideo/:videoID", this.saveVideo );
    router.post("/watchedVideo/:videoID", this.postWatchedVideo );
    router.get("/:videoID", this.getVideoByID );
    router.delete("/watchedVideo/:videoID",this.deleteWatchedVideoByID);
    router.delete("/savedVideo/:videoID",this.deleteSavedVideoByID);
    return router;
  }
  
  postVideo= async (req: Request, res: Response) => {
      if(req.session && req.session.userID && req.session.isAdmin){
        try {
          await this.videoService.postVideo(req.body.videoName,req.body.videoURL,req.body.topic)
           res.status(200).json({status:true,message:'Upload video success'});
           return ;
        } catch (e) {
          console.error(e);
          res.status(500).json({ status: false,message:'Server Error, failed to upload video' });
          return ;
        }
      }else{
        res.status(400).json({ status: false, message: "You are not administrator" }); 
        return;
      }
  };
  getWatchedVideos = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
     // console.log(req.session.userID);
      try {
        const videos = await this.videoService.getWatchedVideo(req.session.userID);
         res.status(200).json(videos);
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error, failed to get watched videos' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
}; //watched videos, return partial videos object
getSavedVideos = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
      const videos = await this.videoService.getSavedVideo(req.session.userID);
      res.status(200).json(videos);
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to get saved videos' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};//return question objects that you have answered, and client's userID
getVideoByID = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
      //console.log(req.params.videoID);
       const video = await this.videoService.getVideoByID(parseInt(req.params.videoID));
       console.log(video);
      res.status(200).json(video);
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to get video by ID' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
saveVideo = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       await this.videoService.saveVideo(req.session.userID,parseInt(req.params.videoID));
        res.status(200).json({status:true,message:"You have saved the video"})
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to get video by ID' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
postWatchedVideo = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       await this.videoService.watchedVideo(req.session.userID,parseInt(req.params.videoID));
        res.status(200).json({status:true,message:"You have saved the video"})
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to put video to history' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
deleteWatchedVideoByID = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       await this.videoService.deleteWatchedVideoByID(req.session.userID,parseInt(req.params.videoID));
       res.status(200).json({status:true,message:"Delete watched video by ID successfully"});
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to delete watched video by ID' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
deleteSavedVideoByID = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       await this.videoService.deleteSavedVideoByID(req.session.userID,parseInt(req.params.videoID))
      res.status(200).json({status:true,message:"Delete saved video by ID"})
        return;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to delete saved video by ID' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};


}
