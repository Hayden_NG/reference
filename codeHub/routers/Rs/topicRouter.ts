import express from "express";
import { Request, Response } from "express";
import { TopicService } from "../../services/Rs/topicService";

export class TopicRouter {
  constructor(private topicService: TopicService) {}

  router() {
    const router = express.Router();
    //router.get("/", this.getMemos);
    router.get("/subscribed", this.getSubscribedTopic);
    router.get("/subscribed/:topic", this.getSubscribedVideos );
    router.delete("/subscribed/:topic", this.removeSubscribedTopic );
    router.post("/subscribed/:topic", this.subscribeTopic);
    return router;
  }
  

  getSubscribedTopic = async (req: Request, res: Response) => {
    if(req.session && req.session.userID){
      try {
        const topics = await this.topicService.getSubscribedTopic(req.session.userID);
         res.status(200).json(topics);
         return ;
      } catch (e) {
        console.error(e);
        res.status(500).json({ status: false,message:'Server Error, failed to get subscribed topics' });
        return ;
      }
    }else{
      res.status(400).json({ status: false, message: "Please Login First" }); 
      return;
    }
}; //returns string array
getSubscribedVideos = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
      const videos = await this.topicService.getSubscribedVideos(req.params.topic);
      res.status(200).json(videos);
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to get subscribed videos' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};//return question objects that you have answered, and client's userID
removeSubscribedTopic = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       await this.topicService.removeSubscribedTopic(req.session.userID,req.params.topic);
      res.status(200).json({status:true,message:"Successfully removed subscribed topic"});
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to removed subscribed topic' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};
subscribeTopic = async (req: Request, res: Response) => {
  if(req.session && req.session.userID){
    try {
       await this.topicService.Subscribe(req.session.userID,req.params.topic);
      res.status(200).json({status:true,message:"Successfully removed subscribed topic"});
       return ;
    } catch (e) {
      console.error(e);
      res.status(500).json({ status: false,message:'Server Error, failed to  subscribe a topic' });
      return ;
    }
  }else{
    res.status(400).json({ status: false, message: "Please Login First" }); 
    return;
  }
};



}
