import {Request,Response,NextFunction} from 'express';

export function guard(req:Request,res:Response,next:NextFunction){
    if(req.session?.userID){
        //console.log(req.session?.userID)
        next();
    }else{
        res.redirect('/?loggedIn=false');
        return;
    }
}


// Kinko============================================================
export function guardVideoPAge(req:Request,res:Response,next:NextFunction){
    if(req.session?.userID){
        //console.log(req.session?.userID)
        next();
    }else{
        res.redirect('/?loggedIn=false');
        return;
    }
}

export function guardAllVideo(req:Request,res:Response,next:NextFunction){
    if(req.session?.userID){
        //console.log(req.session?.userID)
        next();
    }else{
        res.json({login:false});
        return;
    }
}