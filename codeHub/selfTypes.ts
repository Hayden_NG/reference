export enum Topic {
    "javascript" = "Javascript",
    "java" = "Java",
    "CSharp" = "CSharp",
    "python" = "Python"
}
export type video = 
{
videoID:number,
videoName:string,
videoURL:string,
likes:number,
topic:string,
uploadDate:Date
isDeleted:boolean
}

export type comment = {
commentID:number,
videoID: any,
userID: number,
content: string,
uploadDate: string   
}

export type question = {
    questionID: number;
    videoID:number,
    userID: number,
    content:string,
    answers: answer[],
    isSolved: boolean,
    isDelete: boolean,
    uploadDate: Date
}

export type answer = {
userID:number,
content:string,
isBestAnswer: boolean,
uploadDate: Date
}

export type notification = {
content:string,
isRead: boolean
uploadDate: Date;
}

export type user = {
userID:number,
username:string,
password:string,
name?:string,
notifications:notification[],
videoRelated:{watchedVideos:number[],
                savedVideos:number[],
                likedVideos:number[]},
subscribedTopic: string[],//change this to type topicSearch
questions:number[],
contributions:number[],
lastRefreshTime:Date,
isAdmin:boolean
}
export type topicSearch = {topic:Topic,searchIndex:number} ;