import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import jsonfile from 'jsonfile';

const app = express();
app.use(express.static(path.join(__dirname, './private/VideoPage(template)')))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());


app.get('/user', (req, res)=> {
    res.sendFile('/Users/supermanalanko/Desktop/project1/public/login/login.html')
})


app.get('/comments', async(req, res)=>{
        const comments = await jsonfile.readFile('./comments.json');
        res.json(comments);
})

app.post('/comments', async(req, res)=>{
    let comments = await jsonfile.readFile('./comments.json');
    comments.push({
        content: req.body.comment,
        date: new Date().toLocaleString()
    })
    console.log(req.body.comment);
    await jsonfile.writeFile('./comments.json', comments);
    res.redirect('/');
})

app.post('/login', async(req, res)=> {
    let users = await jsonfile.readFile('./users.json');
    
    let loginusername = req.body.username;

    const userSet = users.users;
    console.log(userSet);
    console.log(loginusername);
   
     const findUser =  userSet.find((username: any)=> username.username == loginusername)
     console.log(findUser);
     if (findUser !== undefined){
         const findPassword = userSet.find((password: any)=> password.password == req.body.password);
         console.log(findPassword);
        if(findPassword !== undefined){
            console.log("user alreay exist");
            res.json({duplicate:true,
               success: true
           });
        }else{
            console.log("Wrong password!")
            res.json({password:false})
        }
        
     }
     else{
        users.users.push(
            {
                UserId: users.nextID++,
                username: req.body.username,
                password: req.body.password,
            }
        )
        await jsonfile.writeFile('./users.json', users);
        res.json({success:true});
     }
   
    

})

app.listen(8080, ()=> {
    console.log('Listening on port 8080')
})