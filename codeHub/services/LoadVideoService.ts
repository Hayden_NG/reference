import path from "path";
import jsonfile from "jsonfile";
import { comment } from "../selfTypes";

export class LoadVideoService {

    async sayHello() {
        return "hello world";
    }

    async readComments(id: string) {
        const commentFilePath = path.join(__dirname, `../database/commentFile/comments${id}.json`)
        const comments = await jsonfile.readFile(commentFilePath);
        const commentSet = comments.commentSet;
        console.log(commentSet);
        return commentSet;
    }

    async creatCommentsJson(id: string) {
        const commentFilePath = path.join(__dirname, `../database/commentFile/comments${id}.json`)
        const writeFile = await jsonfile.writeFile(commentFilePath, {NextID:1, commentSet:[]} );
        return writeFile;
    }

    async postComment(id: string, comment: string, userName:any) {
        const commentFilePath = path.join(__dirname, `../database/commentFile/comments${id}.json`)
        const comments = await jsonfile.readFile(commentFilePath);
    

        const pushComment: comment = {
            commentID: comments.NextID++,
            videoID: id,
            userID: userName,
            content: comment,
            uploadDate: new Date().toLocaleString()
        }


        comments.commentSet.push(pushComment);
        await jsonfile.writeFile(commentFilePath, comments);
    }

    async deleteComment(commentID:string, videoid:string){
        const commentFilePath = path.join(__dirname, `../database/commentFile/comments${videoid}.json`);
        const comments = await jsonfile.readFile(commentFilePath);

        const deleteCommentObj = comments.commentSet.find((eachComment:any)=> eachComment.commentID == commentID);

        console.log(deleteCommentObj);
        
        const newArr = comments.commentSet.filter( (currentComment:any)=> currentComment.commentID != deleteCommentObj.commentID);

        console.log(newArr);
        comments.commentSet = newArr;

        console.log(comments.commentSet);
        await jsonfile.writeFile(commentFilePath, comments);


    }

    async addLike(videoid:string, userid:any){
        const videoFilePath = path.join(__dirname, `../database/videos.json`);
        const videoJson = await jsonfile.readFile(videoFilePath);
        const videoArr = videoJson.videos;

        const currentVideoObj = videoArr.find((current:any)=> current.videoID == videoid);

        const findIndex = videoArr.indexOf(currentVideoObj);
        videoArr[findIndex].likes += 1;
        // console.log(videoArr[findIndex]);

        await jsonfile.writeFile(videoFilePath, videoJson);
        // =======================================================
        
        const userFilePath = path.join(__dirname, `../database/users.json`);
        const userJson = await jsonfile.readFile(userFilePath);
        const userArr = userJson.users;

        const currentUserObj = userArr.find ( (current:any)=> current.userID == userid);
        // console.log(currentUserObj);
        const currentUserIndex = userArr.indexOf(currentUserObj);

        const uservideoRelated = userArr[currentUserIndex].videoRelated

        uservideoRelated.likedVideos.push(videoid);

        await jsonfile.writeFile(userFilePath, userJson);
        // console.log(currentUserObj);

    }


    
}