import path from 'path'
import jsonfile from 'jsonfile';
import * as selfTypes from "../../selfTypes";


export class UserServiceRS {
	private userJsonPath: string;

	constructor() {
		this.userJsonPath = path.join(__dirname, "../../database/users.json");
	}

    async createUser(username:string,password:string,isAdmin:boolean,nameOfUser?:string){
        const userDataSet = await jsonfile.readFile(this.userJsonPath);
        const user:selfTypes.user ={
            userID: userDataSet.nextID++,
            username: username,
            password: password,
            notifications:[],
            videoRelated:{
                watchedVideos:[],
                savedVideos:[],
                likedVideos:[],
            },
            subscribedTopic:[],
            questions:[],
            contributions:[],
            lastRefreshTime:new Date(),
            isAdmin:isAdmin
        }
        if(nameOfUser != undefined){
            user.name = nameOfUser;
        }
        userDataSet.users.push(user);
        await jsonfile.writeFile(this.userJsonPath, userDataSet);
    }
async getUsers(){
    const usersDataSet = await jsonfile.readFile(this.userJsonPath);
    const users: selfTypes.user[] = usersDataSet.users;
    return users;   
}
    //get last resfresh time and update it too
    async getAndUpdateLastRefreshTime(id:number){
                const user = await this.getUserByID(id);
                const {lastRefreshTime} = user;
                user.lastRefreshTime = new Date();
                await this.modifyUserInformation(user);
                return lastRefreshTime;
    }
    async getNotification(id:number){
        const user = await this.getUserByID(id);
        return user.notifications;
}
async saveNotification(id:number,notifications:string[]){
    const user = await this.getUserByID(id);
    for(const notification of notifications){
        const n:selfTypes.notification = {content:notification,isRead:false,uploadDate:new Date()}
        user.notifications.unshift(n);
    }
    if(user.notifications.length>10){
        user.notifications.splice(10);
    }
    await this.modifyUserInformation(user);
    return;
}
async notificationIsRead(id:number){
    const user = await this.getUserByID(id);
    for(let notification of user.notifications){
        notification.isRead = true;
    }
    await this.modifyUserInformation(user);
    return;
}
   async getUserByID(id:number){
                const usersDataSet = await jsonfile.readFile(this.userJsonPath);
                const users: selfTypes.user[] = usersDataSet.users;
                const userIndex = users.findIndex(elem => elem.userID === id);
                return users[userIndex];        
    }
    async getUserByUserName(userName:string){
        const usersDataSet = await jsonfile.readFile(this.userJsonPath);
        const users: selfTypes.user[] = usersDataSet.users;
        const userIndex = users.findIndex(elem => elem.username === userName);
        return users[userIndex];        
}
    async modifyUserInformation(user:selfTypes.user){
        const usersDataSet = await jsonfile.readFile(this.userJsonPath);
        const users: selfTypes.user[] = usersDataSet.users;
        const userIndex = users.findIndex(elem => elem.userID === user.userID);
        users[userIndex] = user;
        await jsonfile.writeFile(this.userJsonPath,usersDataSet);
        return;
    }
}


