import path from 'path'
import jsonfile from 'jsonfile';
import * as selfTypes from "../../selfTypes";
import {UserServiceRS} from "../../services/Rs/userService"
//import  {VideoService} from "../../services/Rs/videoService"

export class QuestionService {
    private questionsJsonPath: string;
    private userService:UserServiceRS;
   // private videoService:VideoService;

	constructor(userService:UserServiceRS) {
        this.questionsJsonPath = path.join(__dirname, "../../database/questions.json");
        this.userService = userService;
        //this.videoService = videoService;
	}

    async postQuestion(videoID:number,userID:number,content:string){
        const questionDataSet = await jsonfile.readFile(this.questionsJsonPath);
        const question:selfTypes.question ={
            questionID: questionDataSet.nextID++,
            videoID:videoID,
            userID:userID,
            content: content,
            answers:[],
            isSolved:false,
            isDelete:false,
            uploadDate:new Date()
        } 
        questionDataSet.questions.push(question);
        await jsonfile.writeFile(this.questionsJsonPath, questionDataSet);
        const user = await this.userService.getUserByID(userID)
        const questions: number[] = user.questions;
        questions.unshift(question.questionID);
        await this.userService.modifyUserInformation(user);
        return;
    }
    async getAskedQuestion(userID:number){
        const user = await this.userService.getUserByID(userID);
        const questions: number[] = user.questions;
        const questionDataSet = await jsonfile.readFile(this.questionsJsonPath);
        const allQuestions: selfTypes.question[] = questionDataSet.questions;
        let userAskedQuestions = questions.map(id => {
            const obj = allQuestions.find(elem => elem.questionID === id); //compare the ID of in user save to the all videos in the database
            if (obj != undefined) {
                return obj;
            }
            return;
        })
        return userAskedQuestions;
    }
    async getAnsweredQuestion(userID:number){
        const user = await this.userService.getUserByID(userID);
        const contributions: number[] = user.contributions;
        const questionDataSet = await jsonfile.readFile(this.questionsJsonPath);
        const allQuestions: selfTypes.question[] = questionDataSet.questions;
        let contributionQuestions = contributions.map(id => {
            const obj = allQuestions.find(elem => elem.questionID === id); //compare the ID of in user save to the all videos in the database
            if (obj != undefined) {
                return obj;
            }
            return;
        })
        return contributionQuestions;
    }
    async getVideoQuestion(videoID:number){
        const questionDataSet = await jsonfile.readFile(this.questionsJsonPath);
        const allQuestions: selfTypes.question[] = questionDataSet.questions;
        let questions = allQuestions.filter(elem => {

            if (elem.videoID == videoID) {
                return elem;
            }
            return;
        })
        return questions;
    }
    async removeAskedQuestion(userID:number,questionID:number){
        const user = await this.userService.getUserByID(userID);
        user.questions.splice(user.questions.findIndex(elem => elem==questionID),1);
        await this.userService.modifyUserInformation(user);
       const question = await this.getQuestionByID(questionID);
        const amendedQuestion = { ...question, isDelete: true };
        //question.isDelete = true;
        await this.modifyQuestionInformation(amendedQuestion);
        return;
    }

    async chooseBestSolution(paramsUserID:number,questionID:number){
       const question = await this.getQuestionByID(questionID)
        question.isSolved=true;
        let answers = question.answers;
        answers[answers.findIndex(elem=>elem.userID==paramsUserID)].isBestAnswer = true;
        await this.modifyQuestionInformation(question);
        return;
    }
    async editAnswer(userID:number,questionID:number,uploadValue:string){
        const question = await this.getQuestionByID(questionID)
        if(question!=null||question!=undefined){
            const answers = question.answers;
            const ansIndex = answers.findIndex(elem=>elem.userID==userID);
            answers[ansIndex].content = uploadValue;
            answers[ansIndex].uploadDate = new Date();
            await this.modifyQuestionInformation(question);
            return true;
     }
     return false;
    }
    async postAnswer(userID:number,questionID:number,uploadValue:string){
        const question = await this.getQuestionByID(questionID)
        if(question!=null||question!=undefined){
            const answers = question.answers;
            const ansIndex = answers.findIndex(elem=>elem.userID==userID);
            if(ansIndex != -1){
                return false;
            }
           let answer = {
                userID:userID,
                content:uploadValue,
                isBestAnswer:false,
                uploadDate: new Date()
                }
                answers.unshift(answer);
            await this.modifyQuestionInformation(question);
            const user = await this.userService.getUserByID(userID)
            const contributions: number[] = user.contributions;
            contributions.unshift(questionID);
            await this.userService.modifyUserInformation(user);
            return true;
     }
     return false;
    }
    async deleteAnswer(userID:number,questionID:number){
        const question = await this.getQuestionByID(questionID)
        if(question!=null||question!=undefined){
            const answers = question.answers;
            const ansIndex = answers.findIndex(elem=>elem.userID==userID);
            answers.splice(ansIndex,1);
            await this.modifyQuestionInformation(question);
            const user = await this.userService.getUserByID(userID);
            user.contributions.splice(user.contributions.indexOf(questionID),1);
            await this.userService.modifyUserInformation(user);
            return true;
        }        
    return false;
     }

    async getQuestionByID(questionID:number){
        const questionDataSet = await jsonfile.readFile(this.questionsJsonPath); //read from question.json
        const allQuestions: selfTypes.question[] = questionDataSet.questions;//take the questions array from questions.json
        const questionIndex = allQuestions.findIndex(elem => elem.questionID==questionID);
        return allQuestions[questionIndex];
    }
    async modifyQuestionInformation(question:selfTypes.question){
        const questionDataSet = await jsonfile.readFile(this.questionsJsonPath); //read from question.json
        const allQuestions: selfTypes.question[] = questionDataSet.questions;//take the questions array from questions.json
        const questionIndex = allQuestions.findIndex(elem => elem.questionID==question.questionID);
        allQuestions[questionIndex] = question;
        await jsonfile.writeFile(this.questionsJsonPath,questionDataSet);
        return;
    }

}


