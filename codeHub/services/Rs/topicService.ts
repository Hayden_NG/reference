import path from 'path'
import jsonfile from 'jsonfile';
import * as selfTypes from "../../selfTypes";
import {UserServiceRS} from "../../services/Rs/userService"

export class TopicService {
    private topicJsonPath: string;
    private userService:UserServiceRS

	constructor(userService:UserServiceRS) {
        this.topicJsonPath = path.join(__dirname, "../../database/topic.json");
        this.userService = userService;
	}

    async getSubscribedTopic(userID:number){
        const user = await this.userService.getUserByID(userID);
        const subscribedTopics: string[] = user.subscribedTopic;
        return subscribedTopics;
    }
    async Subscribe(userID:number,topic:string){
        const user = await this.userService.getUserByID(userID);
        if(!user.subscribedTopic.includes(topic)){
            user.subscribedTopic.unshift(topic);
            await this.userService.modifyUserInformation(user);
        }
        return;
    }
    async getSubscribedVideos(topic:string){
        const topicDataSet = await jsonfile.readFile(this.topicJsonPath);
        const subVideosID:number[]= topicDataSet[topic];
        const videoDataSet = await jsonfile.readFile(path.join(__dirname, "../../database/videos.json"));
        const videos:selfTypes.video[] = videoDataSet.videos;
        let subscribedVideos = subVideosID.map(id=>{
            return videos.find(elem=>elem.videoID== id)
        })
        return subscribedVideos;
    }
    async removeSubscribedTopic(userID:number, topic:string){
       const user = await this.userService.getUserByID(userID);
        user.subscribedTopic.splice(user.subscribedTopic.indexOf(topic), 1);
       await this.userService.modifyUserInformation(user);
        return;
    }
    

}


