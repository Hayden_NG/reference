import path from 'path'
import jsonfile from 'jsonfile';
import * as selfTypes from "../../selfTypes";
import {UserServiceRS} from "../../services/Rs/userService"

export class VideoService {
    private videoJsonPath: string;
    private userService:UserServiceRS

	constructor(userService:UserServiceRS) {
        this.videoJsonPath = path.join(__dirname, "../../database/videos.json");
        this.userService = userService;
	}

    async postVideo(videoName:string,videoURL:string,topic:string){
        //console.log(topic);
        const videoData = await jsonfile.readFile(this.videoJsonPath);
        const video: selfTypes.video = {
            videoID: videoData.nextID++,
            videoName: videoName,
            videoURL: videoURL,
            likes: 0,
            topic: topic,
            uploadDate: new Date(),
            isDeleted: false
        }
        const topicDataSet= await jsonfile.readFile(path.join(__dirname, "../../database/topic.json"));
       // console.log(topicDataSet);
       // console.log(video.topic);
        topicDataSet[video.topic].unshift(video.videoID);
        await jsonfile.writeFile(path.join(__dirname, "../../database/topic.json"), topicDataSet);
        videoData.videos.unshift(video);
        await jsonfile.writeFile(this.videoJsonPath, videoData);
        return;
    }
    async getWatchedVideo(userID:number){
        const user = await this.userService.getUserByID(userID);
        const watchedVideos: number[] =user.videoRelated.watchedVideos;
        const videoDataSet = await jsonfile.readFile(this.videoJsonPath);
        const videos: selfTypes.video[] = videoDataSet.videos;
        let userWatchedVideos = watchedVideos.map(id => {
            const obj = videos.find(elem => elem.videoID === id);
            if (obj != undefined) {
                return {
                    videoID: obj.videoID,
                    videoName: obj.videoName,
                    videoURL: obj.videoURL,
                    isDeleted: obj.isDeleted
                }
            }
            return;
        })
        //console.log(userWatchedVideos);
        return userWatchedVideos;
    }
    async getSavedVideo(userID:number){
        const user = await this.userService.getUserByID(userID);
        const savedVideos: number[] = user.videoRelated.savedVideos;
        const videoDataSet = await jsonfile.readFile(this.videoJsonPath);
        const allVideos: selfTypes.video[] = videoDataSet.videos;
        let userSavedVideos = savedVideos.map(id => {
            const obj = allVideos.find(elem => elem.videoID === id); //compare the ID of in user save to the all videos in the database
            if (obj != undefined) {
                return {
                    videoID: obj.videoID,
                    videoName: obj.videoName,
                    videoURL: obj.videoURL,
                    isDeleted: obj.isDeleted
                }
            }
            return;
        })
        return userSavedVideos;
    }
    async saveVideo(userID:number,videoID:number){
        const user = await this.userService.getUserByID(userID);
        const savedVideos: number[] = user.videoRelated.savedVideos;
        const video = savedVideos.find(elem=>elem == videoID);
        if(!video){
            savedVideos.unshift(videoID);
            await this.userService.modifyUserInformation(user);
        }
    }
    async watchedVideo(userID:number,videoID:number){
        const user = await this.userService.getUserByID(userID);
        let watched: number[] = user.videoRelated.watchedVideos;
        const videoIndex = watched.findIndex(elem=>elem == videoID);
        if(videoIndex < 0){
            watched.unshift(videoID);
        }else {
        watched.splice(videoIndex,1);
        watched.unshift(videoID);

        }
        await this.userService.modifyUserInformation(user);
    }
    async getVideoByID(videoID:number){
        const videoDataSet = await jsonfile.readFile(this.videoJsonPath);
        const videos: selfTypes.video[] = videoDataSet.videos;
        const videoIndex = videos.findIndex(elem => elem.videoID == videoID);
        const obj = videos[videoIndex]
        return obj;
    }
    async deleteWatchedVideoByID(userID:number,videoID:number){
        const user = await this.userService.getUserByID(userID)
        user.videoRelated.watchedVideos.splice(user.videoRelated.watchedVideos.indexOf(videoID), 1);
        await this.userService.modifyUserInformation(user);
        return;
    }
    async deleteSavedVideoByID(userID:number,videoID:number){
        const user = await this.userService.getUserByID(userID);
        user.videoRelated.savedVideos.splice(user.videoRelated.savedVideos.indexOf(videoID), 1);
       await this.userService.modifyUserInformation(user);
        return;
     }

}


