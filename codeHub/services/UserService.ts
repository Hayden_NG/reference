import jsonfile from "jsonfile";
import path from "path";
import { hashPassword } from "../hash";
import { user } from "../selfTypes";

export class UserService {
    private userJsonPath: string

    constructor() {
        this.userJsonPath = path.join(__dirname, "../database/users.json");
    }

    async getUserDataset() {
        const data = await jsonfile.readFile(this.userJsonPath);
        return data;
    }

    async getUserByUsername(reqUsername: string) {
        const users = await this.getUserDataset();
        const user = users.users.find((user: any) => user.username == reqUsername)
        return user;
    }

    async getUserByID(reqUserID:any){
        const users = await this.getUserDataset();
        const user = users.users.find((user: any) => user.userID == reqUserID);
        return user;
    }

    async getPwByPassword(reqPassword: string) {
        const users = await this.getUserDataset();
        const user = users.users.find((user: any) => user.password == reqPassword)
        return user;
    }

    async createUser(reqUsername: string, reqPassword: string) {
        const dataset = await this.getUserDataset();

        const hashedPassword = await hashPassword(reqPassword);
        const newUser: user = {
            userID: dataset.nextID++,
            username: reqUsername,
            password: hashedPassword,
            notifications: [],
            videoRelated: {
                watchedVideos: [],
                savedVideos: [],
                likedVideos: []
            },
            subscribedTopic: [],//change this to type topicSearch
            questions: [],
            contributions: [],
            lastRefreshTime: new Date(),
            isAdmin: false

        }
        dataset.users.push(newUser);

        await jsonfile.writeFile(this.userJsonPath, dataset);
        return newUser.userID;
    }


}