


// const Link = window.location.pathname;
// console.log(Link);
// const videoid = Link.replace('/video/', '');
// console.log(videoid);

let link = window.location.search;
console.log(link);
let params = new URLSearchParams(link);
let videoid = params.get("id");
console.log(videoid);

async function readVideo(){
  const fetchRes = await fetch('/allVideo')
  const videos = await fetchRes.json();
  let VideoObj = videos.find((video) => video.videoID == videoid);
  // console.log(VideoObj);
  const iframeUrl = VideoObj.videoURL.replace('https://www.youtube.com/watch?v=', '');
  console.log(iframeUrl);
  document.querySelector('.section1').innerHTML = `<iframe class="video" src="https://www.youtube.com/embed/${iframeUrl}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
}




async function readComments() {
    const fetchRes = await fetch(`/comments/${videoid}`);
    const comments = await fetchRes.json();

    document.querySelector('.section4').innerHTML = '';
    for (let i = 0; i < comments.length; i++) {
        const comment = comments[i];
        let commentHTML = `<div class="CommentContainer">`;
        if (comment.content != null) {
            commentHTML += '<div id="content">' + comment.content + '</div>';
        }
        commentHTML += `<div class="date">` + comment.date + '</div>';
        commentHTML += '</div>'

        document.querySelector('.section4').innerHTML += commentHTML;
    }
}


document.querySelector('form#commentForm').addEventListener('submit', async (event) => {
    const form = event.currentTarget;
    event.preventDefault();
  
    const comment = document.querySelector('[name=comment]').value

    console.log(comment);
    await fetch(`/comments/${videoid}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({comment})
      })
    
    readComments();
  })
  
  readVideo()
readComments();