import express from "express"
import path from "path"
import jsonfile from "jsonfile"
import bodyParser from 'body-parser';
// import expressSession from "express-session"

const app = express()

app.use(express.static(path.join(__dirname,"public")))
app.use(express.static(path.join(__dirname,"pageImage")))
app.use(express.static(path.join(__dirname,"VideoPage(template)")))


app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());



app.get('/allVideo',async (req,res)=> {
    try {
        const allVideo = await jsonfile.readFile('../database/videos.json')
        res.json(allVideo.videos)
    }catch (e) {
        console.error(e)
        res.status(500).json({success:false})
    }
})

app.get('/trendVideo',async (req,res)=> {
    try {
        const trendVideo = await jsonfile.readFile('../database/videos.json')
        res.json(trendVideo.videos)
    }catch(e) {
        console.error(e)
        res.status(500).json({success:false})
    }
})

app.get('/currentUser',async (req,res)=> {
    if(req.session && req.session.isAdmin) {
        res.json({role:'admin'})
    }else {
        res.json({role:'guest'})
    }
})


app.delete('/allVideo/:id',async (req,res)=> {
    try{
        if(!(req.session && req.session.isAdmin)) {
            res.status(401).json({success:false,message:'unauthorized'})
        }
        const deletePic = await jsonfile.readFile('../database/videos.json')

        deletePic.videos.splice(req.params.id,1)

        await jsonfile.writeFile('../database/videos.json',deletePic)

        res.json({success:true})
        
    }catch (e){
        console.error(e)
        res.status(500).json({success:false})
    }
})

app.get('/video/:id', async (req, res)=> {
    res.sendFile(path.join(__dirname,"/VideoPage(template)/video.html"));
});



// app.get('/videoPage', async (req, res)=>{
//     res.sendFile('/Users/supermanalanko/Desktop/project1/private/VideoPage(template)/index.html')
// })


app.get('/comments/:id', async(req, res)=>{
    // console.log(req.originalUrl);
    const videoID = req.originalUrl.replace('/comments/','');
    console.log(videoID);

    const commentFilePath = path.join(__dirname, `../database/commentFile/comments${videoID}.json`);
    
    console.log(commentFilePath);
    try{
        const comments = await jsonfile.readFile(commentFilePath);
        res.json(comments);
    }
    catch(err){
        console.error(err);
        console.log(`comment${videoID} jsonfile no exists, creat json comments${videoID} now`);

        await jsonfile.writeFile(commentFilePath, []);
        res.status(500).json({success: false})
    }
})

app.post('/comments/:id', async(req, res)=>{

    const videoID = req.originalUrl.replace('/comments/','');
    console.log(videoID);

    const commentFilePath = path.join(__dirname, `../database/commentFile/comments${videoID}.json`);

    let comments = await jsonfile.readFile(commentFilePath);
    comments.push({
        content: req.body.comment,
        date: new Date().toLocaleString()
    })
    console.log(req.body.comment);
    await jsonfile.writeFile(commentFilePath, comments);
    res.redirect(req.originalUrl);
})








app.listen(8080, ()=> {
    console.log("Listening on port 8080")
})