let isAdmin = false

async function allVideo() {
    const fetchRes = await fetch('/allVideo')
    const videos = await fetchRes.json()

    document.querySelector('.allVideoList').innerHTML = ''
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i]
        let picHTML = '<div class="adminPage">'
        // if(1>0) {
        picHTML += '<div class="adminBar">'
        picHTML += `<button class="delete" data-id="${i}"><i class="far fa-trash-alt fa-2x"></i></button>`
        picHTML += '</div>'
        // }

        const videoId = videos[i].videoID;
        const sendPath = "watch?id=" + videoId;
        
        picHTML += `<a href="/video/${sendPath}"  data-id="${i}">`
        let videoHTML = video.videoURL
        let url = new URL(videoHTML)
        let id = new URLSearchParams(url.search)
        let result = id.get('v')
        picHTML += `<img class="imageSize" src="https://img.youtube.com/vi/${result}/0.jpg">`
        picHTML += '</a>'
        picHTML += '</div>'

        document.querySelector('.allVideoList').innerHTML += picHTML
    }



    const trashs = document.querySelectorAll('.delete')
    // console.log(trashs)
    for (let trash of trashs) {
        trash.addEventListener('click', async (event) => {
            const button = event.currentTarget;

            await fetch('/allVideo/' + button.dataset.id, {
                method: "DELETE"
            })
            allVideo()
        })
    }

    // const allATag = document.querySelectorAll('.adminPage>a>img')
    // for (let ATag of allATag) {
    //     ATag.addEventListener('click', async (event) => {
    //         event.preventDefault();
    //         const Click = event.currentTarget;
    //         console.log(Click);
    //         console.log(Click.dataset.id);
    //         const sendReq = await fetch('/video/'+ Click.dataset.id, {
    //             method: 'post'
    //         })
    //         const result = await sendReq.redirected();
    //     })
    // }
}




document.querySelector('.allVideo').addEventListener('click', (event) => {
    allVideo()
    document.querySelector('.allVideoTitle').innerHTML = '<p>All Video</p>'
})


async function trendVideo() {
    const fetchRes = await fetch('/trendVideo')
    const trendVideo = await fetchRes.json()

    let trendPic = ''
    for (let like of trendVideo) {
        if (like.likes > 1) {
            trendPic += '<a href="http://www.google.com">'
            let likeHTML = like.videoURL
            let url = new URL(likeHTML)
            let id = new URLSearchParams(url.search)
            let result = id.get('v')
            trendPic += `<img class=imageSize src='https://img.youtube.com/vi/${result}/0.jpg'>`
            trendPic += '</a>'
            trendPic += '</div>'

            document.querySelector('#trendVideoReq').addEventListener('submit', (event) => {
                event.preventDefault()
                document.querySelector('.trendVideoList').innerHTML = trendPic
                document.querySelector('.trendVideoTitle').innerHTML = '<p>Trend Video</p>'
            })
        } else {
            document.querySelector('.trendVideo').addEventListener('click', (event) => {
                event.preventDefault()
                document.querySelector('.trendVideoList').innerHTML = trendPic
                document.querySelector('.trendVideoTitle').innerHTML = '<p>No Trend Video ar! Go Back Home La! Not Your Home ar! Home Page ar!</p>'
            })
        }
    }
}


window.onload = () => {
    allVideo();
    trendVideo();
}
