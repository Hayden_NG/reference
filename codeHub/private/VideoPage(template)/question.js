
const commentQuestionContainer = document.querySelector("#comment-question-container");
const commentTab = document.querySelector("#comment-tab");
const questionTab = document.querySelector('#question-tab');
const section4 = document.querySelector('.section4');
const section3 = document.querySelector('.section3');
const getQuestionsRequest = `/rs/question/videoQuestions`;
const getCurrentUserRequest =`/rs/user/currentUser`;
const getUserNameByIDRequest = `/rs/user/userID`;
const askedQuestionsRequest = `/rs/question/askedQuestion`;
const saveVideoRequest = `/rs/video/savedVideo`;
const postQuestionRequest = `/rs/question/`;
const postAnswerRequest =`/rs/question/contribution`;
const saveButton = document.querySelector('#save');
const queryString = window.location.search;
let parameters = new URLSearchParams(queryString);
const paramVideoID = parseInt(parameters.get("id"));
const questionButton = document.querySelector("#ask-question");
const maxQuestionCount = 2;
let user = {};


saveButton.addEventListener('click',async (event)=>{
    if(saveButton.classList.contains("saved")){
        const isDelete = await deleteSaveVideo(paramVideoID)
        if(isDelete){
            saveButton.setAttribute("save","false");
            saveButton.classList.remove("saved");
        }
    }else{
        const isSave =  await saveVideo(paramVideoID);
        if(isSave){
            saveButton.setAttribute("save","true");
            saveButton.classList.add("saved");
        }
    }
});

commentTab.addEventListener('click',async(event)=>{
   // console.log("Comments");
    questionButton.setAttribute("display","false")
    commentQuestionContainer.innerHTML = "";
    commentQuestionContainer.append(section3);
    commentQuestionContainer.append(section4);
});

questionTab.addEventListener('click',async(event)=>{
    console.log("questions");
    commentQuestionContainer.innerHTML = "";
    questionButton.setAttribute("display","true")
    await displayQuestions();
   //constructCollapseContainer();
   addEventListenerToClearButtons();

})
 async function getQuestions(id){
     const res = await fetch(`${getQuestionsRequest}/${id}`);
     const questions = await res.json();
     return questions;
 }
 async function getCurrentUser(){
    const res = await fetch(getCurrentUserRequest);
    const user = await res.json();
    return user;
}
async function saveVideo(id){
    const res = await fetch(`${saveVideoRequest}/${id}`,{
        method:"POST"
    });
    const isSave = await res.json();
    return isSave.status;
}
async function deleteSaveVideo(id){
    const res = await fetch(`${saveVideoRequest}/${id}`,{
        method:"DELETE"
    });
    const isDelete = await res.json();
    return isDelete.status;
}
async function getUserNameByID(id){
    const res = await fetch(`${getUserNameByIDRequest}/${id}`);
    const name = await res.json();
    return name;
}
 async function displayQuestions(){
     //const user = await getCurrentUser();
     function constructCollapseContainer(){
     
        let collapseTextAreaContainer = document.createElement('div');
        collapseTextAreaContainer.className = "collapse collapse-textarea-container";
        collapseTextAreaContainer.id = `ask-question-container`;
        const textAreaContainer = constructTextAreaContainer("Feel free to ask any questions",`${postQuestionRequest}`,0,0);
        collapseTextAreaContainer.append(textAreaContainer);
       // console.log(collapseTextAreaContainer);
       commentQuestionContainer.insertBefore(collapseTextAreaContainer,commentQuestionContainer.childNodes[0]);
      
     }
    const questions = await getQuestions(paramVideoID);
    questions.sort(function (a, b) {
        return new Date(b.uploadDate) - new Date(a.uploadDate);
      });
    commentQuestionContainer.innerHTML="";
    let questionCount = 0;
    
    if(questions.length == 0){
         commentQuestionContainer.innerHTML = `<div id="no-content">No any questions</div>` 
     }else{
        let i=0;
         for (const question of questions){ 
            if(question.isDelete == false){
                    if ((question.userID == user.userID)&& question.isSolved == false){
                        console.log(question);
                        console.log(user.userID);
                        i=i+1;
                    }
                if(true){
                    console.log(i);
                }
                let whoAskTheQuestion ="";
                if(question.userID ==user.userID){
                    whoAskTheQuestion = "YOU"
                }else{
                    whoAskTheQuestion = await getUserNameByID(question.userID);
                }
                
            /*Constructing Div*/
            let divCol12 = document.createElement("div");
            divCol12.className="col-12";

            let answerQuestionContainer = document.createElement("div");
            answerQuestionContainer.className = "answer-question-container";

            let questionContainer = document.createElement("div");
            questionContainer.className="question-container";

            let questionButton = document.createElement("div");
            questionButton.className = "question-button";
                //start from here
            
            const temp = constructTextAreaContainer("Write your answer here",postAnswerRequest,2,question.questionID);
            const replyCollapseContainer = document.createElement('div');
            replyCollapseContainer.className = "collapse reply-container";
            replyCollapseContainer.id = `a${user.userID}-t${question.questionID}`;
            replyCollapseContainer.append(temp);
            
            const isAnswered = question.answers.find(elem=>elem.userID == user.userID);
            let notSolved =isAnswered?`<i class="fas fa-circle-notch"></i>`:`<a class="no-color-change reply-button" data-toggle="collapse" data-id=${question.questionID} href="#a${user.userID}-t${question.questionID}"><i class="fas fa-reply"></i></a>`;
            
            if(question.userID == user.userID){

                notSolved = `<a class="no-color-change delete-button" data-id=${question.questionID} href="#"><i class="fas fa-minus-circle"></i></a>`
            } 
            let solvedStr = question.isSolved?"[solved]":notSolved;
            
           
            //Question content
            questionContainer.innerHTML=`<div class="question">${question.content} <span class="show-time">By ${whoAskTheQuestion} [${new Date(question.uploadDate).toLocaleString()}]<span></div>`
            
            //Add a delete button if you are the one who ask the question , judge by solvedStr
            questionButton.innerHTML =`<span><a class="no-color-change collapsed" data-toggle="collapse" aria-expanded="false" href="#v${question.userID}-${question.questionID}"><i
            class="fas fa-chevron-down"></i><i class="fas fa-chevron-up"></i></a></span>
            <span>${solvedStr}</span>`;
            
            questionContainer.append(questionButton);
            answerQuestionContainer.append(questionContainer);
            answerQuestionContainer.append(replyCollapseContainer);
            divCol12.append(answerQuestionContainer);
            commentQuestionContainer.append(divCol12);
            if(question.answers.length !== 0){
                
                let collapse = document.createElement("div")
                collapse.className="collapse";
                collapse.id = `v${question.userID}-${question.questionID}`;
                for(const answer of question.answers){
                    let star = "";
                    let collapseTextAreaContainer = document.createElement('div');
                    collapseTextAreaContainer.className = "collapse collapse-textarea-container";
                    collapseTextAreaContainer.id = `v${user.userID}-t${question.questionID}`
                    let username = await getUserNameByID(answer.userID);
                    let answerContainer = document.createElement('div');
                    answerContainer.className = "answer-container";

                    if(question.isSolved){
                        star = ""
                        if(answer.isBestAnswer){
                            star= `<i class="fas fa-star"></i>`;
                        }
                    }else{
                        if(question.userID == user.userID){
                            star=`<a class="no-color-change tick-button" data-id="${answer.userID}" href="#"><i
                            class="fas fa-check-circle"></i></a>`;
                        }else if(answer.userID == user.userID){
                            //if this answer is you answer
                            username = "YOU";
                            star = `<div class="delete custom-button" data-id="${question.questionID}"><i class="fas fa-trash-alt"></i></div><div class="edit custom-button"><a class="no-color-change collapsed" data-toggle="collapse" aria-expanded = "false" href="#v${user.userID}-t${question.questionID}"><i class="fas fa-pencil-alt"></i></a></div>`

                const textAreaContainer = constructTextAreaContainer(answer.content,`${postAnswerRequest}/${question.questionID}`,1,0);
                collapseTextAreaContainer.append(textAreaContainer);
                        }else{
                            star="";
                        }
                    }
                    let innerHTML = `<div class="abc"><div class="answer">${answer.content}<span class="show-time"> By ${username} [${new Date(answer.uploadDate).toLocaleString()}]<span></div>
                    <div class="answer-button">
                        <span>${star}</span>
                    </div></div>`
                    answerContainer.innerHTML = innerHTML;
                    if(user.userID == answer.userID){
                
                        answerContainer.append(collapseTextAreaContainer);
                    }
                    collapse.append(answerContainer);
                }
                answerQuestionContainer.append(collapse);

                //tick button event listener, do it in every questions when they have answers
                const tickButtons = answerQuestionContainer.querySelectorAll('.no-color-change.tick-button');
                for(tickButton of tickButtons){              
                tickButton.addEventListener('click', async(event)=>{
                    //console.log(event.target.dataset.id);
                    
                    const res = await fetch(`${askedQuestionsRequest}/${question.questionID}/${event.target.dataset.id}`,{
                        method:"PUT"
                    })
                    const result = await res.json();
                    if(result.status){
                        displayQuestions();
                    }else{
                        window.alert(result.message);
                    }
                 })

                }
                //=============================
            }
            }
            }
            /*add event listern to delete question button*/
            let deleteCommentsButtons = commentQuestionContainer.querySelectorAll('.delete.custom-button');
            if(deleteCommentsButtons.length != 0){
                for(let deleteCommentButton of deleteCommentsButtons){
                    deleteCommentButton.addEventListener('click',async function(event){
                            console.log(event.target.dataset.id);
                           const res =  await fetch(`${postAnswerRequest}/${event.target.dataset.id}`, {
                                method: 'DELETE'
                              });
                              const result = await res.json();
                              if(result.status){
                                await displayQuestions();
                               // constructCollapseContainer();
                                addEventListenerToClearButtons(); 
                              }else{
                                  window.alert(`${result.message}`);
                              }            
                    })

                }
            }
            let deleteButtons = commentQuestionContainer.querySelectorAll('.delete-button');
            if(deleteButtons.length != 0){
                for(let deleteButton of deleteButtons){
                    deleteButton.addEventListener('click',async function(event){
                       console.log(event.target.dataset.id);
                            await fetch(`${askedQuestionsRequest}/${event.target.dataset.id}`, {
                                method: 'DELETE'
                              });
                              await displayQuestions();
                              //constructCollapseContainer();
                              addEventListenerToClearButtons();          
                    })
                }
            }
           questionCount = i;
     }
     if(questionCount<maxQuestionCount){
        constructCollapseContainer();  
        questionButton.innerHTML = `Ask Questions (${maxQuestionCount-questionCount})`
    }else{
        questionButton.innerHTML = `You can only ask ${maxQuestionCount} questions`
    }

     
 }
 async function displaySaveButton(){
    user = await getCurrentUser();
    const savedVideos = user.videoRelated.savedVideos;
    if(savedVideos.find(id=>id==paramVideoID)){
        saveButton.setAttribute("save","true");
        saveButton.classList.add("saved");
    }
 }
 function constructTextAreaContainer(textAreaContent,request,config,questionID){
    let configuration={};
    const textAreaContainer = document.createElement('div');
    const textArea = document.createElement('textarea');
    const textAreaButtonContainer = document.createElement('div');
    const textAreaButtonUpload = document.createElement('div');
    const textAreaButtonClear = document.createElement('div');
    textAreaButtonContainer.className="textarea-button-container";
    textAreaButtonUpload.className ="custom-button upload";
    textAreaButtonClear.className ="custom-button clear";
    textAreaButtonUpload.innerHTML=`<i class="fas fa-cloud-upload-alt"></i>`;
    textAreaButtonClear.innerHTML=`<i class="fas fa-eraser"></i>`;
    textAreaButtonContainer.append(textAreaButtonClear);
    textAreaButtonContainer.append(textAreaButtonUpload);
    textAreaContainer.append(textArea);
    textAreaContainer.append(textAreaButtonContainer);
    textAreaContainer.className = "text-box";
    textArea.value = textAreaContent;
    let uploadButtons = textAreaContainer.querySelector('.upload.custom-button');
                uploadButtons.addEventListener('click',async function(event){
                   //console.log(event.target.parentNode.parentNode);
                uploadValue = event.target.parentNode.parentNode.querySelector("textarea").value;
                   //     console.log(uploadValue);
                    if(uploadValue != ""){
                        if(config == 0){
                            configuration=  {
                                  method:"POST",
                                  headers:{
                                      "Content-Type":'application/json'
                                  },
                                  body:JSON.stringify({videoID:paramVideoID,content:uploadValue})
                              }
          
                          }else if (config == 1){
                            configuration = {
                                method:"PUT",
                                headers:{
                                    "Content-Type":'application/x-www-form-urlencoded'
                                },
                                body:`uploadValue=${uploadValue}`
                            }
                          }else if(config == 2){
                            configuration=  {
                                  method:"POST",
                                  headers:{
                                      "Content-Type":'application/json'
                                  },
                                  body:JSON.stringify({questionID:questionID,content:uploadValue})
                              }
                            }
                            console.log(uploadValue)
                         // `${postQuestionRequest}`
                       const res = await fetch(request,configuration);
                        const result = await res.json();
                        if(result.status){
                            await displayQuestions();
                            //constructCollapseContainer();
                            addEventListenerToClearButtons();
                        }else{
                            window.alert(`${result.message}`);
                        } 
                    }else{
                        window.alert("You must enter something");
                    }          
            })
        
    return textAreaContainer;
 }


 function addEventListenerToClearButtons(){
     const clearButtons = document.querySelectorAll('.clear.custom-button')
    if(clearButtons.length != 0){
        for(let clear of clearButtons){
            clear.addEventListener('click',async function(event){
                    event.target.parentNode.parentNode.querySelector("textarea").value="";           
            })
        }
   }
 }
 displaySaveButton();