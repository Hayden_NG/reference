


// const Link = window.location.pathname;
// console.log(Link);
// const videoid = Link.replace('/video/', '');
// console.log(videoid);

let link = window.location.search;
console.log(link);
let params = new URLSearchParams(link);
let videoid = params.get("id");
console.log(videoid);
/*======RS======*/
async function putThisVideoToWatched() {
  await fetch(`/rs/video/watchedVideo/${videoid}`, {
    method: "POST"
  })
}
putThisVideoToWatched();
/*=================*/
async function readVideo() {
  const fetchRes = await fetch('/allVideo')
  const videos = await fetchRes.json();
  let VideoObj = videos.find((video) => video.videoID == videoid);
  // console.log(VideoObj);
  const iframeUrl = VideoObj.videoURL.replace('https://www.youtube.com/watch?v=', '');
  console.log(iframeUrl);
  document.querySelector('.section1').innerHTML = `<iframe class="video" src="https://www.youtube.com/embed/${iframeUrl}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
}




async function readComments() {
  const fetchRes = await fetch(`/comments/${videoid}`);
  const comments = await fetchRes.json();

  const getUser = await fetch('/login/user');
  const CurrentUser = await getUser.json();

  document.querySelector('.section4').innerHTML = '';
  for (let i = 0; i < comments.length; i++) {
    const comment = comments[i];
    let commentHTML = `<div class="CommentContainer">`;
    commentHTML += '<div class="userName">' + comment.userID + '</div>';
    if (comment.content != null) {
      commentHTML += '<div class="content">' + comment.content + '</div>';
    }
    commentHTML += `<div class="date">` + comment.uploadDate + '</div>';
    commentHTML += '</div>'


    if (CurrentUser.isAdmin === true) {
      commentHTML += `<button class="delete" data-id="${comment.commentID}"><i class="far fa-trash-alt fa-2x"></i></button>`
    }

    document.querySelector('.section4').innerHTML += commentHTML;

  }
  const trashs = document.querySelectorAll('.delete')
  console.log(trashs)
  for (const trash of trashs) {
    trash.addEventListener('click', async (event) => {
      const button = event.currentTarget;


      await fetch('/deleteComment/' + button.dataset.id, {
        method: "DELETE",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ videoid })
      });
      readComments();

    })
  }
}


document.querySelector('form#commentForm').addEventListener('submit', async (event) => {
  const form = event.currentTarget;
  event.preventDefault();

  const getUserByID = await fetch('/login/user');
  const userObj = await getUserByID.json();

  let name = userObj.name;
  let username = userObj.username;

  let sendUsername = "";

  if (name == undefined) {
    sendUsername = username;
  } else {
    sendUsername = name;
  }

  console.log(sendUsername);

  const comment = document.querySelector('[name=comment]').value
  if (comment == "") {
    alert('error: please type something');
    return;
  }

  console.log(comment);
  await fetch(`/comments/${videoid}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ comment, sendUsername })
  })

  readComments();
})




async function readLikes() {
  const fetchRes = await fetch('/allVideo')
  const videos = await fetchRes.json();
  let VideoObj = videos.find((video) => video.videoID == videoid);

  document.querySelector('#likeCount').innerHTML = `Like: ${VideoObj.likes}`;
  // ==============================================
  const getUser = await fetch('/login/user');
  const CurrentUser = await getUser.json();
  const likedVideosArr = CurrentUser.videoRelated.likedVideos;

  const isLiked = likedVideosArr.find((current) => current == VideoObj.videoID)
  // console.log(isLiked);
  // console.log(likedVideosArr);
  if (isLiked !== undefined) {
    let likeInnerHTML = "";
    likeInnerHTML += `<span id="likeCount">Like: ${VideoObj.likes}</span>`;
    likeInnerHTML += '<button id="clickedLike" type="button" class="btn btn-light"><i class="far fa-thumbs-up"></i></button>';
    document.querySelector('#like').innerHTML = likeInnerHTML;
  }


}

document.querySelector('#clickLike').addEventListener('click', async (event) => {
  const fetchRes = await fetch(`/addLike/${videoid}`);
  const videos = await fetchRes.json();
  readLikes();

})

readVideo()
readLikes()
readComments();