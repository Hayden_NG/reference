
const topicSubscribeRequest = `/rs/topic/subscribed`
const titleDiv = document.createElement('div');
titleDiv.id = "topic-container";
const topic = document.createElement('div');
topic.id="topic";
const subscribeButton = document.createElement('div');
subscribeButton.id="subscribe-button";
subscribeButton.className = "subscribe";
subscribeButton.innerHTML = `<span class = "a"> Subscribed </span><span class="b"> Subscribe </span>`
subscribeButton.setAttribute("subscribe", "false");
titleDiv.append(topic);
titleDiv.append(subscribeButton);
const body = document.querySelector("body");
let videoSub;

body.insertBefore(titleDiv,body.childNodes[0]);

async function getVideoByID(){
    const res = await fetch(`/rs/video/${paramVideoID}`);
    const video = await res.json();
    return video;
}
async function displayTitle(){
    videoSub = await getVideoByID();
    topic.innerHTML = videoSub.topic;
   
  
 }  
 async function displaySubscribe(){
    const user = await getCurrentUser();
    const topicSubscriptions = user.subscribedTopic;
    if(topicSubscriptions.find(topic=>topic==videoSub.topic)){
       console.log("add subscribed to classList");
        subscribeButton.setAttribute("subscribe","true");
        subscribeButton.classList.add("subscribed");
    }
 }
 subscribeButton.addEventListener('click',async (event)=>{
    console.log("press");
 if(subscribeButton.classList.contains("subscribed")){
     const res =  await fetch(`${topicSubscribeRequest}/${videoSub.topic}`,{
         method:"DELETE"
     });
     const msg = await res.json();
     if(msg.status){
         subscribeButton.setAttribute("subscribe","false");
         subscribeButton.classList.remove("subscribed");
         console.log("server - remove subscription");
         displaySubscribe();
     }else{
         window.alert(msg.message);
     }
 }else{
     const res =  await fetch(`${topicSubscribeRequest}/${videoSub.topic}`,{
         method: "POST"
     });
     const msg = await res.json();
     if(msg.status){
         subscribeButton.setAttribute("subscribe","true");
         subscribeButton.classList.add("subscribed");
         console.log("server - subscribed");
         displaySubscribe();
     }else{
         window.alert(msg.message);
     }
 }
});

 

 async function runThis(){
   await displayTitle();
    await displaySubscribe();
 }
 runThis();