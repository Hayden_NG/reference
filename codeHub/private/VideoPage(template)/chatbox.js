const chatBoxIcon = document.querySelector("#chat-box-icon");
const getNameofUserRequest = '/rs/user/name'
let nameoftheuser;
let spamFlag = true;
let typingFlag = true;
let isTyping = false;
let peopleCount;
function createChatBox(){
    const chatBoxContainer = document.createElement('div');
    chatBoxContainer.classList.add("none");
    chatBoxContainer.id = "chat-box-container";
    const chatBoxHeader = document.createElement('div');
    chatBoxHeader.id = "chat-box-header";
    chatBoxHeader.innerHTML ="Chat Room";
    const typingContainer = document.createElement('div');
    typingContainer.id = "typing-container";
    const chatBoxTitle = document.createElement('div');
    chatBoxTitle.id = "chat-box-title";
    chatBoxTitle.innerHTML ="Video Name";
    const messageContainer = document.createElement('div');
    messageContainer.id = "message-container";
    const messageArea = document.createElement('div');
    messageArea.id = "message-area";
    const inputContainer = document.createElement('div');
    inputContainer.id = "input-container";
    inputContainer.innerHTML = `<textarea id="text-area-cb"></textarea>
    <button id="send-button" type="button" class = "none"><i class="fas fa-paper-plane"></i></button>`;
  chatBoxContainer.append(chatBoxTitle);
    chatBoxContainer.append(chatBoxHeader);
    messageContainer.append(messageArea);
    chatBoxContainer.append(messageContainer);
    chatBoxContainer.append(typingContainer);
    chatBoxContainer.append(inputContainer);
    body.append(chatBoxContainer);
    const textArea = document.querySelector("#text-area-cb");
    const sendButton = document.querySelector("#send-button");

    chatBoxIcon.addEventListener('click',(event)=>{   
    if(chatBoxContainer.classList.contains("none")){
 
        chatBoxContainer.classList.remove("none");
    }else{ 
        chatBoxContainer.classList.add("none");
    }
    })
    textArea.addEventListener('keyup',(event)=>{
        if(event.target.value.trim() !=""){
            sendButton.className ="show";
        }else{
            sendButton.className ="none";  
        }
    })
    textArea.addEventListener('keypress',(event)=>{
        if(event.keyCode ==13){
            event.preventDefault();
            if(!(textArea.value.trim()=="")){
            sendButton.click();
            //event.target.blur();
        }
            //emit typing event
        }else{
            if(typingFlag){
                isTyping = true;
                typingFlag = false;
                socket.emit('typing');
                setTimeout(()=>{
                    isTyping = false;
                    setTimeout(()=>{
                        if(!isTyping){
                            socket.emit('stopTyping')
                        }
                    },500);
                    typingFlag = true;
                },1000)
            }
    }
})
let spam=[];
sendButton.addEventListener('click',(event)=>{
    
    spam.push(1);
    //console.log(spam);
    if(spamFlag){
        spamFlag = false;
        setTimeout(function(){
            if(spam.length >10){
                spam = [];
                sendButton.disabled = true;
                setTimeout(function(){
                      sendButton.disabled = false;  
                },3000);
            }
            spamFlag = true; 
           }, 1000);
    }
    const tempDiv = document.createElement('div');
    const timeSpan = document.createElement('span');
    timeSpan.className = 'time-span';
    timeSpan.innerHTML = ` ${new Date().getHours()}:${new Date().getMinutes()}`
    const msg = textArea.value;
    tempDiv.innerHTML = `<span class="name-cb">You:</span>${textArea.value}`;
    tempDiv.append(timeSpan);
    textArea.value="";
    messageArea.append(tempDiv);
    messageArea.scrollTop = messageArea.scrollHeight;
    socket.emit('message', msg);
    sendButton.className ="none"; 
    })
const socket = io.connect('/room');
socket.on('whichRoom',async ()=>{
    nameoftheuser = await getNameofUser()
    const obj = {
        videoID:`${paramVideoID}`,
        name:nameoftheuser
    }
 socket.emit('joinRoom',obj);
})
socket.on('someoneJoin',(name)=>{
    const tempDiv = document.createElement('div');
    tempDiv.className = "participants";
    console.log(name);
    tempDiv.innerHTML = `${name} has joined the room`;
    if(name == nameoftheuser){
        tempDiv.innerHTML = `You has joined the room`;
    }
    messageArea.append(tempDiv);  
})
socket.on('message',(obj)=>{
   // console.log(obj);
   console.log("you recevie a message");
    const tempDiv = document.createElement('div');
    tempDiv.className= 'message';
    const timeSpan = document.createElement('span');
    timeSpan.className = 'time-span';
    timeSpan.innerHTML = ` ${new Date().getHours()}:${new Date().getMinutes()}`
    tempDiv.innerHTML = `<span class = "name-cb">${obj.name}:</span>${obj.message}`;
    tempDiv.append(timeSpan);
    messageArea.append(tempDiv);
    messageArea.scrollTop = messageArea.scrollHeight;
})
socket.on('type',(obj)=>{
    if(typingContainer.querySelector(`#a${obj.id}`)){
        typingContainer.querySelector(`#a${obj.id}`).remove();
    }
    
    const tempDiv = document.createElement('span');
    tempDiv.className = "typing";
    tempDiv.id = `a${obj.id}`;
    typingContainer.insertBefore(tempDiv,typingContainer.childNodes[0]);
    peopleCount= typingContainer.childNodes.length-1;
    if(peopleCount>0){
        tempDiv.innerHTML = `${obj.name} is typing and other ${peopleCount} people are typing...`;
    }else{
        tempDiv.innerHTML = `${obj.name} is typing...`;
    }
    

})
socket.on('stopType',(id)=>{
    console.log("stop typing");
   typingContainer.querySelector(`#a${id}`).remove();
})
socket.on('someoneLeft',(name)=>{
    const tempDiv = document.createElement('div');
    tempDiv.className = "participants";
    tempDiv.innerHTML = `${name} has left the room`;
    messageArea.append(tempDiv); 
    messageArea.scrollTop = messageArea.scrollHeight;
})
window.addEventListener('load',async (event)=>{
    
    await displayTitle();
    console.log(videoSub);
    chatBoxTitle.innerHTML = videoSub.videoName;
    document.querySelector("#Title").innerHTML = videoSub.videoName;


})
}

async function getNameofUser(){
    const res = await fetch(getNameofUserRequest);
    const name = await res.json();
    console.log(name);
    return name;
}
createChatBox();


// socket.on('someoneTyping',()=>{ 
//     //update in every 500 millisecond
//  //you join/someone join, construct message div
// })