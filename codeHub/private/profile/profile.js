let navigationElement = document.querySelector('#nav');
let commentQuestionContainer = document.querySelector('#content-container');
let notificationElement = document.querySelector('#notification-icon');
let notificationTextContainer = document.querySelector('#notification-text-container');
let user = {};
const k = "123";
const notificationRequest = `/rs/user/notification`
const watchedVideoRequest = `/rs/video/watchedVideo`;
const savedVideoRequest = `/rs/video/savedVideo`;
const subscribedTopicRequest = `/rs/topic/subscribed`;
const lastRefreshTimeRequest = `/rs/user/currentUser`;
//const subscribedVideoIDRequest =`/topic/subscribed/videoID`
const videoPageRequest = `../video/watch?id=`
const askedQuestionsRequest = `/rs/question/askedQuestion`;
const getVideobyID = `/rs/video`;
const getUserNamebyID = `/rs/user/userID`;
const getContributionRequest = '/rs/question/contribution'
const getNameofUserRequest = '/rs/user/name';
const navEnum = Object.freeze({
    "watchedVideo": "Watched Video",
    "savedVideo": "Saved Video",
    "subscribedTopic": "Subscribed Topic",
    "questions": "Questions",
    "contributions": "Contributions"
})
//mainPage
mainPage();
async function mainPage()
{
  //await getCurrentUser();//not used in future
    displayNameofUser();
    displayWatchedorSavedVideos(watchedVideoRequest);
    await fetchNotifications();
    addIcontoNotification();
   // console.log(new Date().getFullYear()-new Date(2018, 11 ,28, 10, 33, 31, 0).getFullYear());
}
async function getCurrentUser() //not used in future
{
    const fetchRes = await fetch('/user/currentUser');
    // const currentUser = await fetchRes.json();
    // user = currentUser;
    //console.log(user);
}
async function displayWatchedorSavedVideos(req) //tidy up html
{
    async function getWatchedorSavedVideos(req) //fetch from server
    {
        const res = await fetch(req);
        const videoList = await res.json();
        console.log(videoList);
        return videoList;
    }
   const videoList = await getWatchedorSavedVideos(req);
   let tempStr="";
   commentQuestionContainer.innerHTML = "";
   if (videoList.length == 0)
   {
       if(req == watchedVideoRequest){
           commentQuestionContainer.innerHTML = `<div id="no-videos">No watched history</div>`  
       }else{
           commentQuestionContainer.innerHTML = `<div id="no-videos">No any saved videos</div>`
       }
      
   }else
   {
    for (const video of videoList){
        console.log(video);
        console.log(video.isDeleted);
        const imglink= video.isDeleted?`/profile/deleted.png`:getYoutubeImageLink(video.videoURL);
        const url = video.isDeleted?`#`:`${videoPageRequest}${video.videoID}`;
        tempStr += `<div class ="col-6 col-xs-6 col-sm-4 col-lg-3 animated fadeIn">
        <div class="video-container">
        <button type="button" class="delete-button" data-id="${video.videoID}"><i class="fas fa-minus-circle"></i></button>
            <a href="${url}">
                <img src="${imglink}">
                <div class="video-name">${video.videoName}</div>
            </a>
        </div>
    </div>`
        }
        commentQuestionContainer.innerHTML=tempStr;
        //console.log(req);
        let listItems = commentQuestionContainer.querySelectorAll('.delete-button');
        for(let listItem of listItems){
        listItem.addEventListener('click',async function(event){
            if(req==savedVideoRequest){
                await fetch(`${savedVideoRequest}/${event.target.dataset.id}`, {
                    method: 'DELETE'
                  });
                  displayWatchedorSavedVideos(req);
                }else{
                    await fetch(`${watchedVideoRequest}/${event.target.dataset.id}`, {
                        method: 'DELETE'
                      });
                      displayWatchedorSavedVideos(req);
                }
        })
}        
   }  
}
async function displaySubscribedTopic(){

    const subscribedTopics = await getSubscribedTopic();
   // console.log(subscribedTopics);
  commentQuestionContainer.innerHTML="";
   if(subscribedTopics.length == 0){
         commentQuestionContainer.innerHTML = `<div id="no-content">No Subscribed Topics</div>` 
    }else{
        let searchBarContainer = document.createElement('div');
            searchBarContainer.className = "col-12";
            searchBarContainer.id = "search-bar-container";
            let inputBox = document.createElement('input');
            inputBox.type = "text";
            inputBox.id = "search-box";
            inputBox.placeholder = "Search"
            let searchIconContainer = document.createElement('div');
            searchIconContainer.id = "search-icon-container";
            searchIconContainer.innerHTML =`<i class="fas fa-search"></i>` ;
            let showAll = document.createElement('div');
            showAll.id = "show-all";
            showAll.innerHTML =`<i class="fas fa-arrows-alt-v"></i>` ;
            searchBarContainer.append(inputBox);
            searchBarContainer.append(searchIconContainer);
            searchBarContainer.append(showAll);
            commentQuestionContainer.append(searchBarContainer);
            searchIconContainer.addEventListener('click',(event)=>{
                if(!inputBox.value.trim()==""){
                    const searchText = inputBox.value.trim().toLowerCase();
                    let topicNames =document.querySelectorAll('.topic-name');
                    for (let topicName of topicNames){
                        const str = topicName.innerText.toLowerCase();
                        //console.log(str);
                        if(!str.includes(searchText)&&!topicName.parentNode.parentNode.parentNode.classList.contains("sb-none")){
                            topicName.parentNode.parentNode.parentNode.classList.add("sb-none");
                        }else if(str.includes(searchText)&&topicName.parentNode.parentNode.parentNode.classList.contains("sb-none")){
                            topicName.parentNode.parentNode.parentNode.classList.remove("sb-none");
                            } 
                    }
                }
            });
            showAll.addEventListener('click',(event)=>{
                const nones = document.querySelectorAll(".sb-none");
                inputBox.value="";
                for(let none of nones){
                    none.classList.remove("sb-none");
                }
            });
            inputBox.addEventListener('keypress',(event)=>{
                if(event.keyCode==13){
                    searchIconContainer.click();
                    inputBox.blur();
                }
            })
         for (const topic of subscribedTopics){
            //console.log(topic);
            let divCol12 = document.createElement('div');
            let outerDiv = document.createElement('div');
            let topicContainer = document.createElement('div');
            let subscribedVideosContainer = document.createElement('div');
            divCol12.className ="col-12";
            subscribedVideosContainer.className ="subscribed-videos-container"
             topicContainer.className= "topic-container";
            topicContainer.innerHTML = `
            <div class="topic-name">${topic}</div>
            <div class="unsubscribe" data-id="${topic}"><i class="fas fa-minus-circle"></i></div>`;
           const videos = await getSubscribedVideos(topic);
        //    videos.sort(function(a,b){
        //        return new Date(b.uploadDate)- new Date(a.uploadDate);
        //    })
        videos.sort(function(a,b){
            return new Date(b.uploadDate) - new Date(a.uploadDate);
        })
           for (const video of videos){
               
               if(!video.isDeleted){
               let subscribedVideoContainer = document.createElement('div');
               subscribedVideoContainer.className="subscribed-video-container"
            const imglink= getYoutubeImageLink(video.videoURL);
            subscribedVideoContainer.innerHTML=`<a href="${videoPageRequest}${video.videoID}">
                                                <img src="${imglink}">
                                                <div class="video-name">${video.videoName}</div>
                                                </a>`
            const dateDiv  = document.createElement('div');
            dateDiv.className ="video-date";
            let durationStr = "";
            const uploadDuration = (new Date() - new Date(video.uploadDate))/1000;
            console.log(uploadDuration);
            if(uploadDuration<60){
                const time = Math.floor(uploadDuration);
                durationStr = `${time}s ago`
            }else if(uploadDuration<3600){
                const time = Math.floor(uploadDuration/60);
                durationStr = `${time} minutes ago`
            }else if (uploadDuration <86400){
                const time = Math.floor(uploadDuration/3600);
                durationStr = `${time} hours ago`
            }else if (uploadDuration <86400*30){
                const time = Math.floor(uploadDuration/86400);
                durationStr = `${time} days ago`
            }else{
                const time = new Date().getFullYear()-new Date(video.uploadDate).getFullYear();
                durationStr = `${time} year ago`;
            }
            dateDiv.innerHTML = durationStr;
                /*let a = new Date(2018, 11, 28, 10, 33, 30, 0);
    let b = new Date(2018, 11 ,28, 10, 33, 31, 0);
    console.log(((b-a)/1000));*/

            subscribedVideoContainer.append(dateDiv);
            subscribedVideosContainer.append(subscribedVideoContainer);
        }
            }
            outerDiv.append(topicContainer);
            divCol12.append(outerDiv);
            divCol12.append(subscribedVideosContainer);
            commentQuestionContainer.append(divCol12);
            }
            let listItems = commentQuestionContainer.querySelectorAll('.unsubscribe');
            for(let listItem of listItems){
            listItem.addEventListener('click',async function(event){
                    console.log(event.target.dataset.id);
                    await fetch(`${subscribedTopicRequest}/${event.target.dataset.id}`, {
                        method: 'DELETE'
                      });
                      displaySubscribedTopic();
                    
            })
        }

    }
}
async function getSubscribedVideos(topicStr){
    const res = await fetch(`${subscribedTopicRequest}/${topicStr}`
        // method:"POST",
        // headers:{
        //     'Content-Type':'application/json'
        // },
        // body: JSON.stringify(topics)
    );
    const videoList = await res.json();
    //console.log(videoList);
    return videoList;
}
async function displayNameofUser(){
const name = await getNameofUser();
document.querySelector('#name').innerHTML = name;
}
async function displayContribution(){

    const contribution = await getContribution();
    const contributions = contribution.contributionQuestions;
    const userID = contribution.userID;
    commentQuestionContainer.innerHTML="";
    console.log(contributions);
    if(contributions.length == 0){
         commentQuestionContainer.innerHTML = `<div id="no-content">No any contributions</div>` 
    }else{
        
        commentQuestionContainer.innerHTML=`<div class = "col-12"><div id="contribution-number">Total contributions:${contributions.length}</div></div>`
          for (const question of contributions){ 
            const questionAskedUser = await getUserNameByID(question.userID);
            const answer = question.answers.find(elem=>elem.userID == userID);
             const video = await getVideoByID(question.videoID);
             console.log(video);
            let divCol12 = document.createElement("div");
            let answerQuestionContainer = document.createElement("div");
            let questionContainer = document.createElement("div");
            let questionButton = document.createElement("div");
            let collapse = document.createElement('div');
            let answerContainer = document.createElement('div');
            divCol12.className = "col-12";
            answerQuestionContainer.className="answer-question-container";
            questionContainer.className = "question-container";
            questionButton.className = "question-button";
            collapse.className = "collapse";
            collapse.id = `${userID}-a${question.questionID}`;
            answerContainer.className="answer-container";
            questionContainer.innerHTML = `<div class="question"><a href="../video/watch?id=${video.videoID}">[${video.videoName}]</a> ${question.content} <span class = "show-time">By ${questionAskedUser} ${new Date(question.uploadDate).toLocaleString()}<span></div>`
             if(question.isDelete == true){
                const tempStr = answer.isBestAnswer?`<i class="fas fa-star"></i>`:"";
                questionButton.innerHTML = `<span><a class="no-color-change collapsed" data-toggle="collapse" aria-expanded = "false" href="#${userID}-a${question.questionID}"><i class="fas fa-chevron-down"></i><i class="fas fa-chevron-up"></i></a></span><span>[Deleted]</span>`
                answerContainer.innerHTML = `<div class="abc"><div class="answer">${answer.content} <span class="show-time">${new Date(answer.uploadDate).toLocaleString()}<span></div><div class="answer-button"><span>${tempStr}</span></div></div>` 

             }else if (question.isSolved){
                questionButton.innerHTML = `<span><a class="no-color-change collapsed" data-toggle="collapse" aria-expanded = "false" href="#${userID}-a${question.questionID}"><i class="fas fa-chevron-down"></i><i class="fas fa-chevron-up"></i></a></span><span>[Solved]</span>`
                const tempStr = answer.isBestAnswer?`<i class="fas fa-star"></i>`:"";
                answerContainer.innerHTML = `<div class="abc"><div class="answer">${answer.content} <span class="show-time">${new Date(answer.uploadDate).toLocaleString()}<span></div><div class="answer-button"><span>${tempStr}</span></div></div>`
             }else{
                questionButton.innerHTML = `<span><a class="no-color-change collapsed" data-toggle="collapse" aria-expanded = "false" href="#${userID}-a${question.questionID}"><i class="fas fa-chevron-down"></i><i class="fas fa-chevron-up"></i></a></span><span>[Unsolved]</span>`
                answerContainer.innerHTML = `<div class="abc"><div class="answer">${answer.content} <span class="show-time">${new Date(answer.uploadDate).toLocaleString()}<span></div><div class="answer-button"><span><div class="delete custom-button" data-id="${question.questionID}"><i class="fas fa-trash-alt"></i></div><div class="edit custom-button"><a class="no-color-change collapsed" data-toggle="collapse" aria-expanded = "false" href="#${userID}-t${question.questionID}"><i class="fas fa-pencil-alt"></i></a></div></span></div></div>`
                const textAreaContainer = document.createElement('div');
                const textArea = document.createElement('textarea');
                const textAreaButtonContainer = document.createElement('div');
                const textAreaButtonUpload = document.createElement('div');
                const textAreaButtonClear = document.createElement('div');
                const collapseTextAreaContainer = document.createElement('div');
                collapseTextAreaContainer.className = "collapse collapse-textarea-container";
                collapseTextAreaContainer.id = `${userID}-t${question.questionID}`
                textAreaButtonContainer.className="textarea-button-container";
                textAreaButtonUpload.className ="custom-button upload";
                textAreaButtonClear.className ="custom-button clear";
                textAreaButtonUpload.innerHTML=`<i class="fas fa-cloud-upload-alt"></i>`;
                textAreaButtonClear.innerHTML=`<i class="fas fa-eraser"></i>`;
                textAreaButtonContainer.append(textAreaButtonClear);
                textAreaButtonContainer.append(textAreaButtonUpload);
                textAreaContainer.append(textArea);
                textAreaContainer.append(textAreaButtonContainer);
                textAreaContainer.className = "text-box";
                textAreaContainer.id =`${userID}-t${question.questionID}`;
                textArea.value = `${answer.content}`;
                collapseTextAreaContainer.append(textAreaContainer);
                answerContainer.append(collapseTextAreaContainer);
                let uploadButtons = answerContainer.querySelector('.upload.custom-button');
                uploadButtons.addEventListener('click',async function(event){
                    //console.log(event.target.parentNode.parentNode.querySelector("textarea").value);
                uploadValue = event.target.parentNode.parentNode.querySelector("textarea").value;
                    if(uploadValue != ""){
                       const res = await fetch(`${getContributionRequest}/${question.questionID}`,{
                            method:"PUT",
                            headers:{
                                "Content-Type":'application/x-www-form-urlencoded'
                            },
                            body:`uploadValue=${uploadValue}`
                        })
                        const result = await res.json();
                        //console.log(result);
                        if(result.status){
                            displayContribution();
                        }else{
                            window.alert(`${result.message}`);
                        } 
                    }else{
                        window.alert("You must enter something");
                    }          
            })
            //let deleteButtons = answerContainer.querySelector('.delete.custom-button');
                
             }
             collapse.append(answerContainer);
             questionContainer.append(questionButton);
             answerQuestionContainer.append(questionContainer);
             answerQuestionContainer.append(collapse);
             divCol12.append(answerQuestionContainer);
         
             commentQuestionContainer.append(divCol12);
       }
            let deleteButtons = commentQuestionContainer.querySelectorAll('.delete.custom-button');
            let clearButtons = commentQuestionContainer.querySelectorAll('.clear.custom-button');
            //let uploadButtons = contentElement.querySelectorAll('.upload.custom-button');
            if(deleteButtons.length != 0){
                for(let deleteButton of deleteButtons){
                    deleteButton.addEventListener('click',async function(event){
                            //console.log(event.target.dataset.id);
                           const res =  await fetch(`${getContributionRequest}/${event.target.dataset.id}`, {
                                method: 'DELETE'
                              });
                              const result = await res.json();
                              if(result.status){
                                  displayContribution();
                              }else{
                                  window.alert(`${result.message}`);
                              }            
                    })

                }
           }
           if(clearButtons.length != 0){
            for(let clear of clearButtons){
                clear.addEventListener('click',async function(event){
                        event.target.parentNode.parentNode.querySelector("textarea").value="";           
                })
            }
       }
      }
}
async function displayAskedQuestions(){

   const askedQuestions = await getAskedQuestions();
   askedQuestions.sort(function(a,b){
       return new Date(b.uploadDate) - new Date(a.uploadDate);
   })
    
    commentQuestionContainer.innerHTML="";
    let a ;
    //console.log(askedQuestions);
    if(askedQuestions.length == 0){
         commentQuestionContainer.innerHTML = `<div id="no-content">No any questions</div>` 
     }else{
         for (const question of askedQuestions){ 
             
            if(question.isDelete == false){
                console.log(question);
                const video = await getVideoByID(question.videoID);
            //console.log(video);
            let divCol12 = document.createElement("div");
            let answerQuestionContainer = document.createElement("div");
            let questionContainer = document.createElement("div");
            let questionButton = document.createElement("div");
            const notSolved = `<a class="no-color-change delete-button" data-id=${question.questionID} href="#"><i class="fas fa-minus-circle"></i></a>`
            let solvedStr = question.isSolved?"[solved]":notSolved;
            //console.log(solvedStr);
            divCol12.className="col-12";
            answerQuestionContainer.className = "answer-question-container";
            questionContainer.className="question-container";
            questionContainer.innerHTML=`<div class="question"><a href="../video/watch?id=${video.videoID}">[${video.videoName}]</a> ${question.content} <span class="show-time">[${new Date(question.uploadDate).toLocaleString()}]<span></div>`
            questionButton.className = "question-button";
            questionButton.innerHTML =`<span><a class="no-color-change collapsed" data-toggle="collapse" aria-expanded="false" href="#${question.userID}-q${question.questionID}"><i
            class="fas fa-chevron-down"></i><i class="fas fa-chevron-up"></i></a></span>
            <span>${solvedStr}</span>`;
            //console.log(question.isSolved);
            questionContainer.append(questionButton);
            answerQuestionContainer.append(questionContainer);
            divCol12.append(answerQuestionContainer);
            commentQuestionContainer.append(divCol12);
            if(question.answers.length !== 0){
                
                let collapse = document.createElement("div")
                collapse.className="collapse";
                collapse.id = `${question.userID}-q${question.questionID}`;
                for(const answer of question.answers){
                    let star = "";
                    if(question.isSolved){
                        star = ""
                        if(answer.isBestAnswer){
                            star= `<i class="fas fa-star"></i>`;
                        }
                    }else{
                        star=`<a class="no-color-change tick-button" data-id="${answer.userID}" href="#"><i
                        class="fas fa-check-circle"></i></a>`
                    }
                    const username = await getUserNameByID(answer.userID);
                    let answerContainer = document.createElement('div');
                    answerContainer.className = "answer-container-a";
                    let innerHTML = `<div class="answer">${answer.content}<span class="show-time"> By ${username} [${new Date(answer.uploadDate).toLocaleString()}]<span></div>
                    <div class="answer-button">
                        <span>${star}</span>
                    </div>`
                    answerContainer.innerHTML = innerHTML;
                    collapse.append(answerContainer);
                }
                answerQuestionContainer.append(collapse);
                const tickButtons = answerQuestionContainer.querySelectorAll('.no-color-change.tick-button')
                for(tickButton of tickButtons){
                                   
                tickButton.addEventListener('click', async(event)=>{
                    console.log(event.target.dataset.id);
                    const res = await fetch(`${askedQuestionsRequest}/${question.questionID}/${event.target.dataset.id}`,{
                        method:"PUT"
                    })
                    const result = await res.json();
                    if(result.status){
                        displayAskedQuestions();
                    }else{
                        window.alert(result.message);
                    }
                })

                }
            }
            }
            }
            let listItems = commentQuestionContainer.querySelectorAll('.delete-button');
            if(listItems.length != 0){
                for(let listItem of listItems){
                    listItem.addEventListener('click',async function(event){
                            console.log(event.target.dataset.id);
                            await fetch(`${askedQuestionsRequest}/${event.target.dataset.id}`, {
                                method: 'DELETE'
                              });
                              displayAskedQuestions();            
                    })
                }
            }
     }


}
async function getAskedQuestions(){
    const res = await fetch(askedQuestionsRequest);
    const askedQuestionsList = await res.json();
   // console.log(askedQuestionsList);
    return askedQuestionsList;
}
async function getNameofUser(){
    const res = await fetch(getNameofUserRequest);
    const name = await res.json();
    //console.log(subscribedTopicList);
    return name;
}
async function getSubscribedTopic(){
    const res = await fetch(subscribedTopicRequest);
    const subscribedTopicList = await res.json();
    //console.log(subscribedTopicList);
    return subscribedTopicList;
}
async function getVideoByID(ID){
    const res = await fetch(`${getVideobyID}/${ID}`);
    //console.log(`${getVideobyID}/${ID}`);
    const video = await res.json();
    //console.log(subscribedTopicList);
    return video;
}
async function getUserNameByID(ID){
    const res = await fetch(`${getUserNamebyID}/${ID}`);
    //console.log(`${getVideobyID}/${ID}`);
    const username = await res.json();
    //console.log(subscribedTopicList);
    return username;
}
async function getContribution(){
    const res = await fetch(getContributionRequest);
    const contribution = await res.json();
    //console.log(subscribedTopicList);
    return contribution;
}

function getYoutubeImageLink(youtubeURL)
{
const youtubeID = youtubeURL.substring(youtubeURL.indexOf("=")+1);
//console.log(youtubeID);
return `https://img.youtube.com/vi/${youtubeID}/0.jpg`
}

//change navigation bar to active state.
navigationElement.addEventListener('click', function (event) {
    if (event.target && event.target.matches('.nav-item.nav-link')) {
        event.preventDefault();
        navigationElement.querySelector('.nav-item.nav-link.active').classList.remove("active");
        event.target.classList.add("active");

        if (event.target.innerHTML === navEnum.watchedVideo) {
        displayWatchedorSavedVideos(watchedVideoRequest);
        }
        else if (event.target.innerHTML === navEnum.savedVideo) {
            displayWatchedorSavedVideos(savedVideoRequest);
        }
        else if (event.target.innerHTML === navEnum.subscribedTopic) {
            displaySubscribedTopic();
        }
        else if (event.target.innerHTML === navEnum.questions) {
            displayAskedQuestions();
        }
        else if (event.target.innerHTML === navEnum.contributions) {
           displayContribution();
           //contentElement.innerHTML = HTML[3];
        }

    }
});
//notification
notificationElement.addEventListener('click',async (event)=>{
    if(!notificationElement.querySelector("#notification-text-container").classList.contains('display-none'))//displaying
    {
        notificationElement.querySelector("#notification-text-container").classList.add('display-none');
        let unreadRedCircle = notificationTextContainer.querySelectorAll('.is-read-span.not-read')
        const unReadCount = unreadRedCircle.length;
        if(unReadCount>0){
            for(let circle of unreadRedCircle){
                circle.className = "is-read-span is-read";
            }
        }
        notificationTextContainer.querySelector(".notification-header").innerHTML =`No unread messages`;

    }else
    {
        await fetch(`${notificationRequest}`,{
            method:"PUT"
        }) 
        if(!notificationElement.querySelector(".notification-active").classList.contains('none'))
        {
            notificationElement.querySelector(".notification-active").classList.add('none');
        }
        notificationElement.querySelector("#notification-text-container").classList.remove('display-none');
    }
})

async function fetchNotifications(){
    const isReadIcon = '<i class="fas fa-circle"></i>';
    const fetchRes = await fetch(`${lastRefreshTimeRequest}`,{
        method:"POST"
    });
    const lastRefreshTime = await fetchRes.json();

    notificationTextContainer.innerHTML="";
    const notificationTextOuterDiv = document.createElement('div');
    notificationTextOuterDiv.className = "notification-outer-div";
  console.log(new Date(lastRefreshTime).toLocaleString());
    console.log("now fetch notification");
   const notifications = await getNotification();
   if(notifications.length>0){
       for(notification of notifications){
           console.log("there is notification before")
           //if need to display Date, add it here, use new Date(notification.uploadDate).toLocaleString();
           const fulldate = new Date(notification.uploadDate);
         //  console.log(fulldate);
           const date = fulldate.getDate();
           const month = fulldate.getMonth()+1;
           const hour = fulldate.getHours();
           const minutes = fulldate.getMinutes();
           const displayDate = ` ${hour}:${minutes}, ${date}/${month}`
        const isReadSpan = document.createElement('span');
        const showTimeSpan = document.createElement('span');
        showTimeSpan.className = "show-time";
        showTimeSpan.innerHTML = displayDate;
        isReadSpan.innerHTML = isReadIcon;
        isReadSpan.className = notification.isRead?"is-read-span is-read":"is-read-span not-read"
        let notificationText = document.createElement('div');
        notificationText.className = "notification-text";
        const textContainer = document.createElement('span');
        textContainer.innerHTML = `${notification.content}`;
        textContainer.className = "notification-content";
        notificationText.append(isReadSpan);
        textContainer.append(showTimeSpan);
        notificationText.append(textContainer);
        //notificationText.append(showTimeSpan);
        notificationTextOuterDiv.append(notificationText);
       } 
   }
    
       const fulldate = new Date();
        const date = fulldate.getDate();
        const month = fulldate.getMonth()+1;
        const hour = fulldate.getHours();
        const minutes = fulldate.getMinutes();
        const displayDate = `${hour}:${minutes}, ${date}/${month}`

   let saveNotifications = [];


    const subscribedTopics = await getSubscribedTopic();
    for (const topic of subscribedTopics){
    const videos = await getSubscribedVideos(topic);
    videos.sort(function(a,b){
        return new Date(b.uploadDate) - new Date(a.uploadDate);
    })
       let updatesCount = videos.findIndex(elem=>elem.uploadDate<lastRefreshTime);
       //if(notifications.length == 0 || updatesCount == -1)
       if(updatesCount == -1){
        updatesCount = videos.length;
    }
       if(updatesCount>0){
           console.log("there are updates on subscribed videos")
            const isReadSpan = document.createElement('span');
            const showTimeSpan = document.createElement('span');
            showTimeSpan.className = "show-time";
            showTimeSpan.innerHTML = displayDate;
            isReadSpan.innerHTML = isReadIcon;
            isReadSpan.className = "is-read-span not-read"
           let notificationText = document.createElement('div');
           notificationText.className='notification-text';
           const str = `[${topic}] ${updatesCount} new Videos`
           const textContainer = document.createElement('span');
           textContainer.className = "notification-content";
            textContainer.innerHTML = str;
        notificationText.append(isReadSpan);
        textContainer.append(showTimeSpan);
        notificationText.append(textContainer);
        //notificationText.append(showTimeSpan);
        notificationTextOuterDiv.insertBefore(notificationText,notificationTextOuterDiv.childNodes[0]);
        saveNotifications.push(str);
        //subscribed video how many days ago
       }  
    }


    const askedQuestions= await getAskedQuestions();
    let answers = [];
       for (const askedQuestion of askedQuestions){
        //    console.log(askedQuestion);
        //    console.log(askedQuestion.isSolved);
        //    console.log(askedQuestion.isDelete);
        //    console.log(!(askedQuestion.isSolved&&askedQuestion));
           //(askedQuestion.isDelete == false) && (askedQuestions.isSolved == false)
        if(!(askedQuestion.isSolved&&askedQuestion)){
        console.log("there are updates on answers")
       const answers = askedQuestion.answers;
       answers.sort(function(a,b){
           return new Date(b.uploadDate) - new Date(a.uploadDate);
       })
          let updatesCount = answers.findIndex(elem=>elem.uploadDate<lastRefreshTime);
          //if(notifications.length == 0 || updatesCount == -1){
              console.log(updatesCount);
              console.log("show answer uploaddate: ")
          if(updatesCount == -1){
           updatesCount = answers.length;
       }
          if(updatesCount>0){
              const video = await getVideoByID(askedQuestion.videoID);
              const title = video.videoName;
             const showTimeSpan = document.createElement('span');
             showTimeSpan.className = "show-time";
             showTimeSpan.innerHTML = displayDate;
               const isReadSpan = document.createElement('span');
               isReadSpan.innerHTML = isReadIcon;
               isReadSpan.className = "is-read-span not-read"
              let notificationText = document.createElement('div');
              notificationText.className='notification-text';
              const str = `<a href="../video/watch?id=${video.videoID}">[${title}]</a> ${updatesCount} replies on your questions`
              const textContainer = document.createElement('span');
              textContainer.className = "notification-content";
               textContainer.innerHTML = str;
           notificationText.append(isReadSpan);
           textContainer.append(showTimeSpan);
           notificationText.append(textContainer);
           //notificationText.append(showTimeSpan);
           notificationTextOuterDiv.insertBefore(notificationText,notificationTextOuterDiv.childNodes[0]);
           saveNotifications.push(str);
           //subscribed video how many days ago
          }  
       }
    }
    const header = document.createElement('div');
    if(notifications.length + saveNotifications.length == 0 ){
        header.className = "notification-header no-messages"
        header.innerHTML = "No Any Messages"
    }else{
        const unreadCount = notificationTextOuterDiv.querySelectorAll('.is-read-span.not-read').length;
        header.className = "notification-header";
        let tempStr ="";
        if(unreadCount == 0 ){
            tempStr = `No unread messages`
        }else{
            tempStr = `You have ${unreadCount} unread messages`
        }
        header.innerHTML = tempStr;
    }
    //console.log(header);
    notificationTextOuterDiv.insertBefore(header,notificationTextOuterDiv.childNodes[0]);
    notificationTextContainer.append(notificationTextOuterDiv);
    await saveNotification(saveNotifications);
}

async function getNotification(){
    const res = await fetch ('/rs/user/notification');
    const notifications = res.json();
    return notifications;
}
async function saveNotification(saveNotifications){
    const res = await fetch ('/rs/user/notification',{
        method:"POST",
        headers:{
            "Content-Type": 'application/json'
        },
        body:JSON.stringify({notifications:saveNotifications})
    });
}
function addIcontoNotification(){
    let unreadRedCircle = notificationTextContainer.querySelectorAll('.is-read-span.not-read')
    console.log(unreadRedCircle);
        const unReadCount = unreadRedCircle.length;
        if(unReadCount>0){
            if(notificationElement.querySelector(".notification-active").classList.contains('none'))
            {
                notificationElement.querySelector(".notification-active").classList.remove('none');
            }
            }
        }
