import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import jsonfile from "jsonfile"
// import jsonfile from 'jsonfile';
// import { Server } from 'http';

const app = express();
app.use(express.static(path.join(__dirname, 'public/login')))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname,"public")))
app.use(express.static(path.join(__dirname,"pageImage")))
app.use(express.static(path.join(__dirname,"/public/VideoPage(template)")))



app.get('/allVideo',async (req,res)=> {
    try {
        const allVideo = await jsonfile.readFile('./database/videos.json')
        res.json(allVideo.videos)
    }catch (e) {
        console.error(e)
        res.status(500).json({success:false})
    }
})

app.get('/trendVideo',async (req,res)=> {
    try {
        const trendVideo = await jsonfile.readFile('./database/videos.json')
        res.json(trendVideo.videos)
    }catch(e) {
        console.error(e)
        res.status(500).json({success:false})
    }
})


app.delete('/allVideo/:id',async (req,res)=> {
    try{
        const deletePic = await jsonfile.readFile('./database/videos.json')

        deletePic.videos.splice(req.params.id,1)

        await jsonfile.writeFile('./database/videos.json',deletePic)

        res.json({success:true})
        
    }catch (e){
        console.error(e)
        res.status(500).json({success:false})
    }
})

// app.get('/video/:id', async (req, res)=> {
//     res.sendFile(path.join(__dirname,"public/VideoPage(template)/video.html"));
// });

// app.get('/comments/:id', async(req, res)=>{
//     // console.log(req.originalUrl);
//     const videoID = req.originalUrl.replace('/comments/','');
//     console.log(videoID);

//     const commentFilePath = path.join(__dirname, `./database/commentFile/comments${videoID}.json`);
    
//     console.log(commentFilePath);
//     try{
//         const comments = await jsonfile.readFile(commentFilePath);
//         res.json(comments);
//     }
//     catch(err){
//         console.error(err);
//         console.log(`comment${videoID} jsonfile no exists, creat json comments${videoID} now`);

        
//         res.status(500).json({success: false})
//     }
// })

// app.post('/comments/:id', async(req, res)=>{

//     const videoID = req.originalUrl.replace('/comments/','');
//     console.log(videoID);

//     const commentFilePath = path.join(__dirname, `./database/commentFile/comments${videoID}.json`);

//     let comments = await jsonfile.readFile(commentFilePath);
//     comments.push({
//         content: req.body.comment,
//         date: new Date().toLocaleString()
//     })
//     console.log(req.body.comment);
//     await jsonfile.writeFile(commentFilePath, comments);
//     res.redirect(req.originalUrl);
// })

import {UserService} from './services/UserService';
import {UserRouter} from './routers/UserRouter'
import {LoadVideoRouter} from './routers/LoadVideoRouter';
import {LoadVideoService} from './services/LoadVideoService'


const userService = new UserService();
const userRouter = new UserRouter(userService);

const loadVideoService = new LoadVideoService();
const loadVideoRouter = new LoadVideoRouter(loadVideoService);

app.use('/', loadVideoRouter.router())
app.use('/login', userRouter.router())

const PORT =8080;

app.listen(PORT, ()=>{
    console.log(`Listening at http://localhost:${PORT}/`);
})