import express from 'express';
import path from 'path';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
//@ts-ignore
import jsonfile from 'jsonfile';
//@ts-ignore
import * as selfTypes from './selfTypes';
import {TopicService, QuestionService} from './services/Rs';// 
//import {QuestionService} from './services/Rs/questionService';
import {UserServiceRS} from './services/Rs/userService';
import {VideoService} from './services/Rs/videoService';
import {QuestionRouter} from './routers/Rs/questionRouter';
import {TopicRouter} from './routers/Rs/topicRouter';
import {UserRouterRS} from './routers/Rs/userRouter';
import {VideoRouter} from './routers/Rs/videoRouter';
import { LoginRouter } from './routers/Rs/loginRouter';
// import fetch from 'node-fetch';
import dotenv from 'dotenv';
import grant from 'grant-express';
import http from 'http';
import socketIO from 'socket.io';

import {guard, guardAllVideo} from './routers/Rs/isLoggedIn'

//@ts-ignore
import {UserService} from './services/UserService';
//@ts-ignore
import {UserRouter} from './routers/UserRouter'
const questionRouterPath = `/rs/question`
const topicRouterPath = `/rs/topic`
const videoRouterPath = `/rs/video`
const userRouterPath = `/rs/user`
const faceBookLogin = `/rs/login`


const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const sessionMiddleware = expressSession({
    secret: "Tecky Academy teaches typescript",
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
  });
app.use(sessionMiddleware)
const server = new http.Server(app);
const io = socketIO(server);
io.use((socket,next)=>{
    sessionMiddleware(socket.request,socket.request.res,next);
});
const videoIO = io.of('/room');
videoIO.on('connection', function(socket){
    let roomNumber:string|number;
    let nameCB:string;
    if(!socket.request.session.userID){
        socket.disconnect()
    }
    if(socket.request.session.userID){
        socket.join(`${socket.request.session.userID}`);
        //socket.request.session.socketId = socket.id;
        videoIO.to(`${socket.request.session.userID}`).emit('whichRoom');
        socket.leave(`${socket.request.session.userID}`);
        // socket.request.session.save();
         socket.on('joinRoom',(obj)=>{
            nameCB = obj.name;
            roomNumber = obj.videoID;
            socket.join(obj.videoID);
            console.log(`you are now joining room:${obj.videoID}`);
            videoIO.to(obj.videoID).emit('someoneJoin', obj.name);
        })
        socket.on(`message`,(msg)=>{
         
            const obj = {name:nameCB,message:msg}
            socket.to(`${roomNumber}`).emit('message',obj);
        })
        socket.on(`typing`,(msg)=>{
            const obj = {
                        id: socket.request.session.userID,
                        name: nameCB    
                    }
                 //   console.log(obj);
            socket.to(`${roomNumber}`).emit('type',obj);
        })

        socket.on('stopTyping',()=>{
            socket.to(`${roomNumber}`).emit('stopType',`${socket.request.session.userID}`);
            
        })
            
        socket.on("disconnect",()=>{
                console.log(`you are now leaving room:${roomNumber}`)
                videoIO.in(`${roomNumber}`).emit('someoneLeft',nameCB);
        })
        }
});

io.on('connection', function (socket) {
    if(!socket.request.session.userID){
        socket.disconnect()
    }
    if(socket.request.session.userID){
        socket.join(`${socket.request.session.userID}`);
        socket.request.session.socketId = socket.id;
         socket.request.session.save();
         socket.on('ABC',(msg)=>{
            console.log(msg);
         })
         socket.on("logout",()=>{
            socket.request.session.socketId = null;
           socket.request.session.save();
        }) 
    }
    socket.on("disconnect",()=>{
        socket.request.session.socketId = null;
       //socket.request.session.save();
    })
});
const questionRouter = new QuestionRouter(new QuestionService(new UserServiceRS()),io);
const topicRouter = new TopicRouter(new TopicService(new UserServiceRS()));
const videoRouter = new VideoRouter(new VideoService(new UserServiceRS()));
const userRouterRS = new UserRouterRS(new UserServiceRS());
const loginRouter = new LoginRouter(new UserServiceRS());
dotenv.config();
//const sessionMiddleware =expressSession({secret: 'grant', saveUninitialized: true, resave: true})

app.use(grant({
    "defaults":{
        "protocol": "http",
        "host": process.env.REDIRECT_HOST || "",
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile","email"],
        "callback": "/login/google"
      },
  }));

// app.use(grant({
//     "defaults":{
//         "protocol": "http",
//         "host": "localhost:8080",
//         "transport": "session",
//         "state": true,
//     },
//     "facebook":{
//         "key": process.env.FACEBOOK_APP_ID || "",
//         "secret": process.env.FACEBOOK_CLIENT_SECRET || "",
//         "scope": ["email"],
//         "callback": "/rs/login/facebook"
//       },
//   }));
app.use(express.static(path.join(__dirname,"public")))
app.use(express.static(path.join(__dirname,"pageImage")))
//app.use(express.static(path.join(__dirname,'private')));

app.use(questionRouterPath, guard,questionRouter.router());
app.use(topicRouterPath, guard,topicRouter.router());
app.use(videoRouterPath, guard,videoRouter.router());
app.use(userRouterPath,userRouterRS.router());
app.use(faceBookLogin,loginRouter.router());

//kinko
app.use(express.static(path.join(__dirname,"/private/VideoPage(template)")))


  

import {LoadVideoRouter} from './routers/LoadVideoRouter';
import {LoadVideoService} from './services/LoadVideoService'


const userService = new UserService();

const userRouter = new UserRouter(userService);
const loadVideoService = new LoadVideoService();

const loadVideoRouter = new LoadVideoRouter(loadVideoService)


app.use('/login', userRouter.router())
app.use('/', loadVideoRouter.router())

const PORT = 8080;

server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});

// app.listen(PORT, () => {
    
//     console.log(`Listening at http://localhost:${PORT}/`);
//     //console.log((new Date()).toLocaleString());
// });

//topics should limited to 4? give a dropdown list based on our database?


//==========================================================================

/*Ronson*/

app.get("/user/currentUser", async (req, res) => {
    if (req.session) {
        req.session.user = {
            userID: 1,
            username: "testing",
            password: "1234",
            notification: [],
            videoRelated: {
                watchedVideos: [],
                savedVideos: [1, 2],
                likedVideos: [],
            },
            subscribedTopic: [selfTypes.Topic.javascript],
            questions: [],
            contributions: [],
            lastRefreshTime: new Date('December 17, 2019 03:24:00'),
            isAdmin: false
        }
        req.session.userID = 4;
        if (req.session.user) {
            //console.log(req.session.userID);
            return res.end('testing');

        } else {
            return res.status(400).end("You have to Login first");
        }
    }
}) //just for testing, should not be used in the future


//===========================================================================
/* Hayden */
app.get('/allVideo', guardAllVideo ,async (req,res)=> {
   
    try {
        const allVideo = await jsonfile.readFile('./database/videos.json')
        res.json(allVideo.videos)
    }catch (e) {
        console.error(e)
        res.status(500).json({success:false})
    }
})

app.get('/trendVideo',async (req,res)=> {
    try {
        const trendVideo = await jsonfile.readFile('./database/videos.json')
        res.json(trendVideo.videos)
    }catch(e) {
        console.error(e)
        res.status(500).json({success:false})
    }
})


app.delete('/allVideo/:id',async (req,res)=> {
    try{
        const deletePic = await jsonfile.readFile('./database/videos.json')

        // deletePic.videos.splice(req.params.id,1)

        const VideoArr = deletePic.videos;

        console.log(VideoArr);
        console.log(`reqId: ${req.params.id}`);
        const findIndex = VideoArr.find( (currentObj:any) => currentObj.videoID == req.params.id);

        // console.log(findIndex);
        const VideoIndex = VideoArr.indexOf(findIndex);
        // console.log(VideoIndex);

        VideoArr[VideoIndex].isDeleted = true;

        console.log( VideoArr);

        await jsonfile.writeFile('./database/videos.json',deletePic)

        res.json({success:true})
        
    }catch (e){
        console.error(e)
        res.status(500).json({success:false})
    }
})

app.post('/allVideo',async(req,res)=> {

    try{
        const addVideo = await jsonfile.readFile('./database/videos.json')
        const subTopic = await jsonfile.readFile('./database/topic.json')
        const obj = {
            videoID:addVideo.nextID++, 
            videoName:req.body.videoName,
            videoURL:req.body.videoURL,
            likes:parseInt(req.body.likes),
            topic:req.body.topic,
            uploadDate:new Date(),
            isDeleted:false
        }
        addVideo.videos.push(obj);
        subTopic[obj.topic].push(obj.videoID)
        
        res.redirect('/')

        await jsonfile.writeFile('./database/videos.json',addVideo)
        await jsonfile.writeFile('./database/topic.json', subTopic)

    }catch(e){
        console.error(e)
        res.status(500).json({success:false})
    }
})



app.use(guard, express.static(path.join(__dirname,'private')));