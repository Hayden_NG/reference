let currentPage = window.location.href;
let currentPath = window.location.pathname;
let homepage = currentPage.replace(currentPath, "");


console.log(homepage);

document.querySelector('#loginForm').addEventListener('submit', async (event)=> {
    event.preventDefault();
    const form = event.currentTarget;

    const username = document.querySelector('[name=username]').value;
    const password = document.querySelector('[name=password]').value;
    
    const res = await fetch('/login/login', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({username, password})
    });
    const result = await res.json();
    console.log(result);

    
    console.log(homepage);
    if(result.createUser == true){
        alert("successfully registered, Please login again")
        window.location.href = homepage;
    }else if(result.password == false){
        alert("Wrong Password, Please check your password");
    }else if(result.login == true){
        window.location.href = homepage;
    }

})