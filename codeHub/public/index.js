window.onload = function () {
    const searchParams = new URLSearchParams(window.location.search);
    const errMessage = searchParams.get("loggedIn");
    console.log("4");
    if (errMessage) {
        window.alert("You have to Log In first!")
    }
}
console.log("5");



async function allVideo() {
    const fetchRes = await fetch('/allVideo')
    const videos = await fetchRes.json()
    console.log(videos);

    if (videos.login == false) {
        alert("you have to login first!")
        return;
    }
    

    // kinko add selectbar & isAdmin*================================================
    const getUser = await fetch('/login/user');
    const CurrentUser = await getUser.json();
    console.log(CurrentUser);

    console.log(CurrentUser.isAdmin);

    document.querySelector('.allVideoTitle').innerHTML = '<p>More Video</p>'

    let selectBarHtml = "";
    selectBarHtml += '<input id="All" type="button" value="All">';
    selectBarHtml += '<input id="Javascript" type="button" value="Javascript">';
    selectBarHtml += '<input id="Python" type="button" value="Python"> ';
    selectBarHtml += '<input id="CSharp" type="button" value="CSharp">';
    selectBarHtml += '<input id="Others" type="button" value="Others">';
    document.querySelector('.selectBar').innerHTML = selectBarHtml;
    // =================================================================

    document.querySelector('.allVideoList').innerHTML = ''
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i]

        if (videos[i].isDeleted == false){
        let picHTML = '<div class="adminPage">'
        // if(1>0) {


        if (CurrentUser.isAdmin === true) {
            if (videos[i].isDeleted == false){
            picHTML += '<div class="adminBar">'
            picHTML += `<button class="delete" data-id="${videos[i].videoID}"><i class="far fa-trash-alt fa-2x"></i></button>`
            picHTML += '</div>'
            }
        }

        // }

        
        const videoId = videos[i].videoID;
        const sendPath = "watch?id=" + videoId;

        picHTML += `<a href="/video/${sendPath}"  data-id="${i}">`
        let videoHTML = video.videoURL
        let url = new URL(videoHTML)
        let id = new URLSearchParams(url.search)
        let result = id.get('v')
        picHTML += `<img class="imageSize" src="https://img.youtube.com/vi/${result}/0.jpg">`
        picHTML += '</a>'
        picHTML += '</div>'

        document.querySelector('.allVideoList').innerHTML += picHTML
        }

        
    }


    if (CurrentUser.isAdmin === true) {
        const trashs = document.querySelectorAll('.delete')
        console.log(trashs)
        for (let trash of trashs) {
            trash.addEventListener('click', async (event) => {
                const button = event.currentTarget;

                await fetch('/allVideo/' + button.dataset.id, {
                    method: "DELETE"
                })
                allVideo()
            })
        }
    }
    /*==============================KinKo===============================*/
    document.querySelector('#All').addEventListener('click', (event) => {
        console.log("HI");
        allVideo();
    })

    document.querySelector('#Javascript').addEventListener('click', (event) => {
        JavaScriptVideo();
    })

    document.querySelector('#Python').addEventListener('click', (event) => {
        PythonVideo();
    })

    document.querySelector('#CSharp').addEventListener('click', (event) => {
        CSharpVideo();
    })

    document.querySelector('#Others').addEventListener('click', (event) => {
        OthersVideo();
    })

    // ==============Hayden newAddVideo
    if (CurrentUser.isAdmin === true) {        //isAdmin
        document.querySelector('.allVideoTitle').innerHTML = '<p>More Video</p>'
        document.querySelector('.allVideoTitle').innerHTML += '<button class="addVideo"><i class="fas fa-plus"></i></button>'
        document.querySelector('.addVideo').addEventListener('click', () => {
            let addVideoForm = ""
            addVideoForm += '<form id="addVideo" action="/allVideo" method="POST">';
            addVideoForm += '<input type="text" name="videoName" required placeholder="videoName:"><input type="text" required name="videoURL" placeholder="videoURL:"><input type="text" required name="likes" placeholder="likes:">';
            addVideoForm += '<select required name="topic" aria-placeholder="topic">';
            addVideoForm += '<option value="Javascript">Javascript</option> <option value="Python">Python</option> <option value="CSharp">CSharp</option> <option value="Others">Others</option>';
            addVideoForm += '</select>'
            addVideoForm += '<input type="submit">';
            addVideoForm += '</form>';
            document.querySelector('.allVideoTitle').innerHTML += addVideoForm;

        })
        document.querySelector('#addVideo').addEventListener('submit', (event) => {
            event.preventDefault()
            const form = event.currentTarget
            const videoName = document.querySelector('[name=videoName]').value
            const videoURL = document.querySelector('[name=videoURL]').value
            const likes = document.querySelector('[name=likes]').value
            const topic = document.querySelector('[name=topic]').value

            fetch('/allVideo', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'videoName=' + videoName + '&videoURL=' + videoURL + '&likes=' + likes + '&topic=' + topic
            })
            allVideo()
            form.reset()
        })
    }


}

document.querySelector('.allVideo').addEventListener('click', async (event) => {
    const fetchRes = await fetch('/allVideo')
    const videos = await fetchRes.json()

    console.log(videos);
    if (videos.login == false) {
        alert("you have to login first!")
        return;
    }


    allVideo();

})
// ==================================


async function trendVideo() {
    const fetchRes = await fetch('/trendVideo')
    const trendVideo = await fetchRes.json()

    // const getUser = await fetch('/login/user');
    // const CurrentUser = await getUser.json();

    const videoFetchRes = await fetch('/allVideo')
    const videos = await videoFetchRes.json()
    

    // if (videos.login == false) {
    //     alert("you have to login first!");
    //     return;
    // }

    document.querySelector('.trendVideo').addEventListener('click', (event) => {
        console.log(videos);
        if (videos.login == false) {
            alert("you have to login first!");
            return;
        }
      

        document.querySelector('.trendVideoList').innerHTML = ''

        for (let i = 0; i < trendVideo.length; i++) {
            const like = trendVideo[i]
            const videoId = trendVideo[i].videoID;
            const sendPath = "watch?id=" + videoId;
    
            if (like.likes > 0) {
                let trendPic = '<div class="adminPage">'
    
                                  
                    if (trendVideo[i].isDeleted == false) {
                        document.querySelector('.trendVideoTitle').innerHTML = '<p>Trend Video</p>'
                        
                        trendPic += `<a href="/video/${sendPath}"  data-id="${i}">`
    
    
    
    
                        let likeHTML = like.videoURL
                        let url = new URL(likeHTML)
                        let id = new URLSearchParams(url.search)
                        let result = id.get('v')
                        trendPic += `<img class=imageSize src='https://img.youtube.com/vi/${result}/0.jpg'>`
                        trendPic += '</a>'
                        trendPic += '</div>'
                        document.querySelector('.trendVideoList').innerHTML += trendPic
                    
                } 
              
            } 
    
        }
    
        trendVideo() ;
       
    })

}





/*==============================Kinko===============================*/
async function JavaScriptVideo() {
    const fetchRes = await fetch('/allVideo')
    const videosArr = await fetchRes.json();

    const getUser = await fetch('/login/user');
    const CurrentUser = await getUser.json();

    const videos = videosArr.filter((videosObj) => videosObj.topic == "Javascript");
    console.log(videos);

    document.querySelector('.allVideoList').innerHTML = ''
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i]
        
        let picHTML = '<div class="adminPage">'
        if (videos[i].isDeleted == false ){
        if(CurrentUser.isAdmin === true) {
        picHTML += '<div class="adminBar">'
        picHTML += `<button class="delete" data-id="${i}"><i class="far fa-trash-alt fa-2x"></i></button>`
        picHTML += '</div>'
        }

        const videoId = videos[i].videoID;
        const sendPath = "watch?id=" + videoId;

        picHTML += `<a href="/video/${sendPath}"  data-id="${i}">`
        let videoHTML = video.videoURL
        let url = new URL(videoHTML)
        let id = new URLSearchParams(url.search)
        let result = id.get('v')
        picHTML += `<img class="imageSize" src="https://img.youtube.com/vi/${result}/0.jpg">`
        picHTML += '</a>'
        picHTML += '</div>'
        }

        document.querySelector('.allVideoList').innerHTML += picHTML
    }

    const trashs = document.querySelectorAll('.delete')
    console.log(trashs)
    for (let trash of trashs) {
        trash.addEventListener('click', async (event) => {
            const button = event.currentTarget;

            await fetch('/allVideo/' + button.dataset.id, {
                method: "DELETE"
            })
            allVideo()
        })
    }
}

async function PythonVideo() {
    const fetchRes = await fetch('/allVideo')
    const videosArr = await fetchRes.json();

    const getUser = await fetch('/login/user');
    const CurrentUser = await getUser.json();

    const videos = videosArr.filter((videosObj) => videosObj.topic == "Python");
    console.log(videos);

    document.querySelector('.allVideoList').innerHTML = ''
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i]
        
        let picHTML = '<div class="adminPage">'
        if (videos[i].isDeleted == false){
        if(CurrentUser.isAdmin == true) {
        picHTML += '<div class="adminBar">'
        picHTML += `<button class="delete" data-id="${videos[i].videoID}"><i class="far fa-trash-alt fa-2x"></i></button>`
        picHTML += '</div>'
        }

        const videoId = videos[i].videoID;
        const sendPath = "watch?id=" + videoId;

        picHTML += `<a href="/video/${sendPath}"  data-id="${i}">`
        let videoHTML = video.videoURL
        let url = new URL(videoHTML)
        let id = new URLSearchParams(url.search)
        let result = id.get('v')
        picHTML += `<img class="imageSize" src="https://img.youtube.com/vi/${result}/0.jpg">`
        picHTML += '</a>'
        picHTML += '</div>'
        }
        document.querySelector('.allVideoList').innerHTML += picHTML
    }

    
   
        const trashs = document.querySelectorAll('.delete')
        console.log(trashs)
        for (let trash of trashs) {
            trash.addEventListener('click', async (event) => {
                const button = event.currentTarget;

                await fetch('/allVideo/' + button.dataset.id, {
                    method: "DELETE"
                })
                allVideo()
            })
        }
    
}

async function CSharpVideo() {
    const fetchRes = await fetch('/allVideo')
    const videosArr = await fetchRes.json();

    const getUser = await fetch('/login/user');
    const CurrentUser = await getUser.json();

    const videos = videosArr.filter((videosObj) => videosObj.topic == "CSharp");
    console.log(videos);

    document.querySelector('.allVideoList').innerHTML = ''
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i]
        let picHTML = '<div class="adminPage">'
        if (videos[i].isDeleted == false){
        if(CurrentUser.isAdmin === true) {
        picHTML += '<div class="adminBar">'
        picHTML += `<button class="delete" data-id="${i}"><i class="far fa-trash-alt fa-2x"></i></button>`
        picHTML += '</div>'
        }

        const videoId = videos[i].videoID;
        const sendPath = "watch?id=" + videoId;

        picHTML += `<a href="/video/${sendPath}"  data-id="${i}">`
        let videoHTML = video.videoURL
        let url = new URL(videoHTML)
        let id = new URLSearchParams(url.search)
        let result = id.get('v')
        picHTML += `<img class="imageSize" src="https://img.youtube.com/vi/${result}/0.jpg">`
        picHTML += '</a>'
        picHTML += '</div>'
        }
        document.querySelector('.allVideoList').innerHTML += picHTML
    }

    const trashs = document.querySelectorAll('.delete')
    console.log(trashs)
    for (let trash of trashs) {
        trash.addEventListener('click', async (event) => {
            const button = event.currentTarget;

            await fetch('/allVideo/' + button.dataset.id, {
                method: "DELETE"
            })
            allVideo()
        })
    }
}

async function OthersVideo() {
    const fetchRes = await fetch('/allVideo')
    const videosArr = await fetchRes.json();

    const getUser = await fetch('/login/user');
    const CurrentUser = await getUser.json();

    const videos = videosArr.filter((videosObj) => videosObj.topic == "Others");
    console.log(videos);

    document.querySelector('.allVideoList').innerHTML = ''
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i]
        let picHTML = '<div class="adminPage">'
        if (videos[i].isDeleted == false){
        if(CurrentUser.isAdmin === true) {
        picHTML += '<div class="adminBar">'
        picHTML += `<button class="delete" data-id="${i}"><i class="far fa-trash-alt fa-2x"></i></button>`
        picHTML += '</div>'
        }

        const videoId = videos[i].videoID;
        const sendPath = "watch?id=" + videoId;

        picHTML += `<a href="/video/${sendPath}"  data-id="${i}">`
        let videoHTML = video.videoURL
        let url = new URL(videoHTML)
        let id = new URLSearchParams(url.search)
        let result = id.get('v')
        picHTML += `<img class="imageSize" src="https://img.youtube.com/vi/${result}/0.jpg">`
        picHTML += '</a>'
        picHTML += '</div>'
        }
        document.querySelector('.allVideoList').innerHTML += picHTML
    }

    const trashs = document.querySelectorAll('.delete')
    console.log(trashs)
    for (let trash of trashs) {
        trash.addEventListener('click', async (event) => {
            const button = event.currentTarget;

            await fetch('/allVideo/' + button.dataset.id, {
                method: "DELETE"
            })
            allVideo()
        })
    }
}

// document.querySelector('#Javascript').addEventListener('click',(event)=> {
//     JavaScriptVideo();
// })

trendVideo();


