const notificationRequest = `/rs/user/notification`
const subscribedTopicRequest = `/rs/topic/subscribed`;
const lastRefreshTimeRequest = `/rs/user/currentUser`;
//const subscribedVideoIDRequest =`/topic/subscribed/videoID`
const askedQuestionsRequest = `/rs/question/askedQuestion`;
const getVideobyID = `/rs/video`;

const getCurrentUserRequest = "/rs/user/currentUser";
const isThereAUserRequest = "/rs/user/isThereAUser";
const getNameRequest = "/rs/user/name";
const logoutRequest = "/rs/login/logout";
const topIcon = document.querySelector(".topIcon");
async function getCurrentUser() {
    const req = await fetch(isThereAUserRequest);
    const isUser = await req.json();
    console.log(isUser);
    if (isUser) {
        //getname

        topIcon.innerHTML = "";
        const req = await fetch(getNameRequest);
        const name = await req.json();
        const nameTextContainer = document.createElement('span');
        nameTextContainer.className = "name-of-user";
        const nameText = document.createTextNode(` ${name} `);
        const iconContainer = document.createElement('span');
        const notificationContainer = document.createElement('span');
        notificationContainer.id = "notification-icon";
        const nTextContainer = document.createElement('div');
        nTextContainer.id = "notification-text-container";
        nTextContainer.className = "display-none";
        const logoutContainer = document.createElement('span');
        nameTextContainer.append(nameText);
        iconContainer.innerHTML = `<a href ="/profile/profile.html"><i class="fas fa-user  login-icon icon"></i></a>`;
        notificationContainer.innerHTML = `<div class="notification-active none"></div><i class="fas fa-bell"></i>`;
        logoutContainer.innerHTML = `<a href ="${logoutRequest}"><i class="fas fa-sign-out-alt logout-icon icon"></i></a>`;
        topIcon.append(iconContainer);
        topIcon.append(nameTextContainer);
        notificationContainer.append(nTextContainer);
        topIcon.append(notificationContainer);
        topIcon.append(logoutContainer);
        group();
    } else { }
}
//window.addEventListener('onload',(event)=>{
    getCurrentUser();
//});

async function getSubscribedVideos(topicStr) {
    const res = await fetch(`${subscribedTopicRequest}/${topicStr}`
        // method:"POST",
        // headers:{
        //     'Content-Type':'application/json'
        // },
        // body: JSON.stringify(topics)
    );
    const videoList = await res.json();
    //console.log(videoList);
    return videoList;
}
async function getAskedQuestions() {
    const res = await fetch(askedQuestionsRequest);
    const askedQuestionsList = await res.json();
    // console.log(askedQuestionsList);
    return askedQuestionsList;
}

async function getSubscribedTopic() {
    const res = await fetch(subscribedTopicRequest);
    const subscribedTopicList = await res.json();
    //console.log(subscribedTopicList);
    return subscribedTopicList;
}


//notification

async function getVideoByID(ID){
    const res = await fetch(`${getVideobyID}/${ID}`);
    //console.log(`${getVideobyID}/${ID}`);
    const video = await res.json();
    //console.log(subscribedTopicList);
    return video;
}


async function getNotification() {
    const res = await fetch('/rs/user/notification');
    const notifications = res.json();
    return notifications;
}
async function saveNotification(saveNotifications) {
    const res = await fetch('/rs/user/notification', {
        method: "POST",
        headers: {
            "Content-Type": 'application/json'
        },
        body: JSON.stringify({ notifications: saveNotifications })
    });
}
async function group(){
    let notificationElement = document.querySelector('#notification-icon');
let notificationTextContainer = document.querySelector('#notification-text-container');
//console.log(notificationTextContainer);
 await fetchNotifications();
 addIcontoNotification();
 notificationElement.addEventListener('click', async (event) => {
    if (!notificationElement.querySelector("#notification-text-container").classList.contains('display-none'))//displaying
    {
        notificationElement.querySelector("#notification-text-container").classList.add('display-none');
        let unreadRedCircle = notificationTextContainer.querySelectorAll('.is-read-span.not-read')
        const unReadCount = unreadRedCircle.length;
        if (unReadCount > 0) {
            for (let circle of unreadRedCircle) {
                circle.className = "is-read-span is-read";
            }
        }
        notificationTextContainer.querySelector(".notification-header").innerHTML = `No unread messages`;

    } else {
        await fetch(`${notificationRequest}`, {
            method: "PUT"
        })
        if (!notificationElement.querySelector(".notification-active").classList.contains('none')) {
            notificationElement.querySelector(".notification-active").classList.add('none');
        }
        notificationElement.querySelector("#notification-text-container").classList.remove('display-none');
    }
})
async function fetchNotifications() {
    const isReadIcon = '<i class="fas fa-circle"></i>';
    const fetchRes = await fetch(`${lastRefreshTimeRequest}`, {
        method: "POST"
    });
    const lastRefreshTime = await fetchRes.json();
    console.log(new Date(lastRefreshTime).toLocaleString());
    notificationTextContainer.innerHTML = "";
    const notificationTextOuterDiv = document.createElement('div');
    notificationTextOuterDiv.className = "notification-outer-div";
    //  console.log(new Date(lastRefreshTime).toLocaleString());
    console.log("now fetch notification");
    const notifications = await getNotification();
    if (notifications.length > 0) {
        for (notification of notifications) {
            console.log("there is notification before")
            //if need to display Date, add it here, use new Date(notification.uploadDate).toLocaleString();
            const fulldate = new Date(notification.uploadDate);
            //  console.log(fulldate);
            const date = fulldate.getDate();
            const month = fulldate.getMonth() + 1;
            const hour = fulldate.getHours();
            const minutes = fulldate.getMinutes();
            const displayDate = ` ${hour}:${minutes}, ${date}/${month}`
            const isReadSpan = document.createElement('span');
            const showTimeSpan = document.createElement('span');
            showTimeSpan.className = "show-time";
            showTimeSpan.innerHTML = displayDate;
            isReadSpan.innerHTML = isReadIcon;
            isReadSpan.className = notification.isRead ? "is-read-span is-read" : "is-read-span not-read"
            let notificationText = document.createElement('div');
            notificationText.className = "notification-text";
            const textContainer = document.createElement('span');
            //console.log(`${notification.content}`);
            textContainer.innerHTML = `${notification.content}`;
            textContainer.className = "notification-content";
            notificationText.append(isReadSpan);
            textContainer.append(showTimeSpan);
            notificationText.append(textContainer);
            //notificationText.append(showTimeSpan);
            notificationTextOuterDiv.append(notificationText);
           // console.log(notificationTextOuterDiv);
        }
    }

    const fulldate = new Date();
    const date = fulldate.getDate();
    const month = fulldate.getMonth() + 1;
    const hour = fulldate.getHours();
    const minutes = fulldate.getMinutes();
    const displayDate = `${hour}:${minutes}, ${date}/${month}`

    let saveNotifications = [];


    const subscribedTopics = await getSubscribedTopic();
    for (const topic of subscribedTopics) {
        const videos = await getSubscribedVideos(topic);
        videos.sort(function (a, b) {
            return new Date(b.uploadDate) - new Date(a.uploadDate);
        })
        let updatesCount = videos.findIndex(elem => elem.uploadDate < lastRefreshTime);
        //if (notifications.length == 0 || updatesCount == -1) {
        if (updatesCount == -1) {
            updatesCount = videos.length;
        }
        if (updatesCount > 0) {
            console.log("there are updates on subscribed videos")
            const isReadSpan = document.createElement('span');
            const showTimeSpan = document.createElement('span');
            showTimeSpan.className = "show-time";
            showTimeSpan.innerHTML = displayDate;
            isReadSpan.innerHTML = isReadIcon;
            isReadSpan.className = "is-read-span not-read"
            let notificationText = document.createElement('div');
            notificationText.className = 'notification-text';
            const str = `[${topic}] ${updatesCount} new Videos`
            const textContainer = document.createElement('span');
            textContainer.className = "notification-content";
            textContainer.innerHTML = str;
            notificationText.append(isReadSpan);
            textContainer.append(showTimeSpan);
            notificationText.append(textContainer);
            //notificationText.append(showTimeSpan);
            notificationTextOuterDiv.insertBefore(notificationText, notificationTextOuterDiv.childNodes[0]);
            saveNotifications.push(str);
            //subscribed video how many days ago
        }
    }


    const askedQuestions = await getAskedQuestions();
    let answers = [];
    for (const askedQuestion of askedQuestions) {
        //    console.log(askedQuestion);
        //    console.log(askedQuestion.isSolved);
        //    console.log(askedQuestion.isDelete);
        //    console.log(!(askedQuestion.isSolved&&askedQuestion));
        //(askedQuestion.isDelete == false) && (askedQuestions.isSolved == false)
        if (!(askedQuestion.isSolved && askedQuestion)) {
            console.log("there are updates on answers")
            const answers = askedQuestion.answers;
            answers.sort(function (a, b) {
                return new Date(b.uploadDate) - new Date(a.uploadDate);
            })
            let updatesCount = answers.findIndex(elem => elem.uploadDate < lastRefreshTime);
            //if (notifications.length == 0 || updatesCount == -1) {
            if (updatesCount == -1) {
                updatesCount = answers.length;
            }
            if (updatesCount > 0) {
                const video = await getVideoByID(askedQuestion.videoID);
                const title = video.videoName;
                const showTimeSpan = document.createElement('span');
                showTimeSpan.className = "show-time";
                showTimeSpan.innerHTML = displayDate;
                const isReadSpan = document.createElement('span');
                isReadSpan.innerHTML = isReadIcon;
                isReadSpan.className = "is-read-span not-read"
                let notificationText = document.createElement('div');
                notificationText.className = 'notification-text';
                const str = `<a href="/video/watch?id=${video.videoID}">[${title}]</a> ${updatesCount} replies on your questions`
                const textContainer = document.createElement('span');
                textContainer.className = "notification-content";
                textContainer.innerHTML = str;
                notificationText.append(isReadSpan);
                textContainer.append(showTimeSpan);
                notificationText.append(textContainer);
                //notificationText.append(showTimeSpan);
                notificationTextOuterDiv.insertBefore(notificationText, notificationTextOuterDiv.childNodes[0]);
                saveNotifications.push(str);
                //subscribed video how many days ago
            }
        }
    }
    const header = document.createElement('div');
    if (notifications.length + saveNotifications.length == 0) {
        header.className = "notification-header no-messages"
        header.innerHTML = "No Any Messages"
    } else {
        const unreadCount = notificationTextOuterDiv.querySelectorAll('.is-read-span.not-read').length;
        header.className = "notification-header";
        let tempStr = "";
        if (unreadCount == 0) {
            tempStr = `No unread messages`
        } else {
            tempStr = `You have ${unreadCount} unread messages`
        }
        header.innerHTML = tempStr;
    }
    //console.log(header);
    notificationTextOuterDiv.insertBefore(header, notificationTextOuterDiv.childNodes[0]);
    notificationTextContainer.append(notificationTextOuterDiv);
    await saveNotification(saveNotifications);
}
function addIcontoNotification() {
    let unreadRedCircle = notificationTextContainer.querySelectorAll('.is-read-span.not-read')
    console.log(unreadRedCircle);
    const unReadCount = unreadRedCircle.length;
    if (unReadCount > 0) {
        if (notificationElement.querySelector(".notification-active").classList.contains('none')) {
            notificationElement.querySelector(".notification-active").classList.remove('none');
        }
    }
}
}
